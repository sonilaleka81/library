import {
	createBrowserRouter,
	RouterProvider,
	Navigate,
} from "react-router-dom";
import { Provider } from "react-redux";

import { store } from "./store";

import List from "./pages/List";
import ListDetails from "./pages/ListDetails";
import Book from "./pages/Book";

const router = createBrowserRouter([
	{
		path: "/",
		element: <List />,
	},
	{
		path: "/list/:listName/:publishDate",
		element: <ListDetails />,
	},
	{
		path: "/book/:isbn",
		element: <Book />,
	},
	{
		path: "*",
		element: <Navigate to={`/`} replace />,
	},
]);

function App() {
	return (
		<Provider store={store}>
			<div className="App my-5">
				<RouterProvider router={router} />
			</div>
		</Provider>
	);
}

export default App;

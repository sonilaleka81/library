const ResultDetails = ({ details = {} }) => {
	return (
		<div className="flex flex-col">
			{Object.entries(details).map(([key, value], index) => (
				<div key={`item-${index}`} className="flex flex-row">
					<div className="font-bold">{key}:</div>
					<div className="ml-2">{value || "--"}</div>
				</div>
			))}
		</div>
	);
};

export default ResultDetails;

import api from "./_api";
import config from "./config";

import ListDetails from "./mock/list-details";
import List from "./mock/full-overview";

export const getBestsellerOverview = async () => {
	return config.USE_MOCK ? List : await api.get(`lists/full-overview.json`);
};

export const getBestSellerListDetails = async (listName, date = "current") => {
	return config.USE_MOCK
		? ListDetails
		: await api.get(`lists/${date}/${listName}.json`);
};

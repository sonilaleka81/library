import { Link } from "react-router-dom";

const PurchaseLinks = ({ links }) => {
	return (
		<div className="mt-10">
			<h1 className="text-lg font-bold">Available for purchase in:</h1>
			<div className="mt-3 grid grid-cols-2 sm:grid-cols-4 md:grid-cols-5 gap-2">
				{links.map((link, index) => (
					<Link key={`buy-link-${index}`} to={link.url}>
						<div className="text-white text-center bg-slate-400 hover:bg-slate-600 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 mb-2 ">
							{link.name}
						</div>
					</Link>
				))}
			</div>
		</div>
	);
};

export default PurchaseLinks;

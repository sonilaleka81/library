const mock = {
	data: {
		status: "OK",
		copyright:
			"Copyright (c) 2023 The New York Times Company.  All Rights Reserved.",
		num_results: 90,
		results: {
			bestsellers_date: "2023-07-15",
			published_date: "2023-07-30",
			published_date_description: "latest",
			previous_published_date: "2023-07-23",
			next_published_date: "",
			lists: [
				{
					list_id: 704,
					list_name: "Combined Print and E-Book Fiction",
					list_name_encoded: "combined-print-and-e-book-fiction",
					display_name: "Combined Print & E-Book Fiction",
					updated: "WEEKLY",
					list_image: null,
					list_image_width: null,
					list_image_height: null,
					books: [
						{
							age_group: "",
							amazon_product_url:
								"https://www.amazon.com/dp/1649374046?tag=NYTBSREV-20",
							article_chapter_link: "",
							author: "Rebecca Yarros",
							book_image:
								"https://storage.googleapis.com/du-prd/books/images/9781649374042.jpg",
							book_image_width: 309,
							book_image_height: 500,
							book_review_link: "",
							contributor: "by Rebecca Yarros",
							contributor_note: "",
							created_date: "2023-07-19 22:10:24",
							description:
								"Violet Sorrengail is urged by the commanding general, who also is her mother, to become a candidate for the elite dragon riders.",
							first_chapter_link: "",
							price: "0.00",
							primary_isbn10: "1649374046",
							primary_isbn13: "9781649374042",
							book_uri:
								"nyt://book/106c2cf0-7d20-51b1-bad4-91c3ebcd131a",
							publisher: "Red Tower",
							rank: 1,
							rank_last_week: 2,
							sunday_review_link: "",
							title: "FOURTH WING",
							updated_date: "2023-07-19 22:14:39",
							weeks_on_list: 11,
							buy_links: [
								{
									name: "Amazon",
									url: "https://www.amazon.com/dp/1649374046?tag=NYTBSREV-20",
								},
								{
									name: "Apple Books",
									url: "https://goto.applebooks.apple/9781649374042?at=10lIEQ",
								},
								{
									name: "Barnes and Noble",
									url: "https://www.anrdoezrs.net/click-7990613-11819508?url=https%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9781649374042",
								},
								{
									name: "Books-A-Million",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fp%252FFOURTH%252BWING%252FRebecca%252BYarros%252F9781649374042&url2=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fsearch%253Fquery%253DFOURTH%252BWING%252BRebecca%252BYarros",
								},
								{
									name: "Bookshop",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fbookshop.org%2Fa%2F3546%2F9781649374042&url2=https%3A%2F%2Fbookshop.org%2Fbooks%3Faffiliate%3D3546%26keywords%3DFOURTH%2BWING",
								},
								{
									name: "IndieBound",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.indiebound.org%2Fbook%2F9781649374042%3Faff%3DNYT&url2=https%3A%2F%2Fwww.indiebound.org%2Fsearch%2Fbook%3Fkeys%3DFOURTH%2BWING%2BRebecca%2BYarros%26aff%3DNYT",
								},
							],
						},
						{
							age_group: "",
							amazon_product_url:
								"https://www.amazon.com/dp/1538756595?tag=NYTBSREV-20",
							article_chapter_link: "",
							author: "Colleen Hoover",
							book_image:
								"https://storage.googleapis.com/du-prd/books/images/9781538756591.jpg",
							book_image_width: 333,
							book_image_height: 500,
							book_review_link: "",
							contributor: "by Colleen Hoover",
							contributor_note: "",
							created_date: "2023-07-19 22:10:24",
							description:
								"Dangers develop when a drug trafficker becomes obsessed with a woman who has a mutual attraction to a D.E.A. agent.",
							first_chapter_link: "",
							price: "0.00",
							primary_isbn10: "1538756595",
							primary_isbn13: "9781538756591",
							book_uri:
								"nyt://book/0962ac94-2f91-5500-bcf8-24c90607bc4a",
							publisher: "Grand Central",
							rank: 2,
							rank_last_week: 1,
							sunday_review_link: "",
							title: "TOO LATE",
							updated_date: "2023-07-19 22:14:39",
							weeks_on_list: 3,
							buy_links: [
								{
									name: "Amazon",
									url: "https://www.amazon.com/dp/1538756595?tag=NYTBSREV-20",
								},
								{
									name: "Apple Books",
									url: "https://goto.applebooks.apple/9781538756591?at=10lIEQ",
								},
								{
									name: "Barnes and Noble",
									url: "https://www.anrdoezrs.net/click-7990613-11819508?url=https%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9781538756591",
								},
								{
									name: "Books-A-Million",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fp%252FTOO%252BLATE%252FColleen%252BHoover%252F9781538756591&url2=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fsearch%253Fquery%253DTOO%252BLATE%252BColleen%252BHoover",
								},
								{
									name: "Bookshop",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fbookshop.org%2Fa%2F3546%2F9781538756591&url2=https%3A%2F%2Fbookshop.org%2Fbooks%3Faffiliate%3D3546%26keywords%3DTOO%2BLATE",
								},
								{
									name: "IndieBound",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.indiebound.org%2Fbook%2F9781538756591%3Faff%3DNYT&url2=https%3A%2F%2Fwww.indiebound.org%2Fsearch%2Fbook%3Fkeys%3DTOO%2BLATE%2BColleen%2BHoover%26aff%3DNYT",
								},
							],
						},
						{
							age_group: "",
							amazon_product_url:
								"https://www.amazon.com/dp/0316499579?tag=NYTBSREV-20",
							article_chapter_link: "",
							author: "James Patterson and James O. Born",
							book_image:
								"https://storage.googleapis.com/du-prd/books/images/9780316499576.jpg",
							book_image_width: 322,
							book_image_height: 500,
							book_review_link: "",
							contributor: "by James Patterson and James O. Born",
							contributor_note: "",
							created_date: "2023-07-19 22:10:24",
							description:
								"A killer, who targets women in New York City, becomes obsessed with Michael Bennett’s daughter.",
							first_chapter_link: "",
							price: "0.00",
							primary_isbn10: "0316499587",
							primary_isbn13: "9780316499583",
							book_uri:
								"nyt://book/a63798ad-ba3b-5763-9cf1-066a06a453cf",
							publisher: "Little, Brown",
							rank: 3,
							rank_last_week: 0,
							sunday_review_link: "",
							title: "OBSESSED",
							updated_date: "2023-07-19 22:14:40",
							weeks_on_list: 1,
							buy_links: [
								{
									name: "Amazon",
									url: "https://www.amazon.com/dp/0316499579?tag=NYTBSREV-20",
								},
								{
									name: "Apple Books",
									url: "https://goto.applebooks.apple/9780316499583?at=10lIEQ",
								},
								{
									name: "Barnes and Noble",
									url: "https://www.anrdoezrs.net/click-7990613-11819508?url=https%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9780316499583",
								},
								{
									name: "Books-A-Million",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fp%252FOBSESSED%252FJames%252BPatterson%252Band%252BJames%252BO.%252BBorn%252F9780316499583&url2=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fsearch%253Fquery%253DOBSESSED%252BJames%252BPatterson%252Band%252BJames%252BO.%252BBorn",
								},
								{
									name: "Bookshop",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fbookshop.org%2Fa%2F3546%2F9780316499583&url2=https%3A%2F%2Fbookshop.org%2Fbooks%3Faffiliate%3D3546%26keywords%3DOBSESSED",
								},
								{
									name: "IndieBound",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.indiebound.org%2Fbook%2F9780316499583%3Faff%3DNYT&url2=https%3A%2F%2Fwww.indiebound.org%2Fsearch%2Fbook%3Fkeys%3DOBSESSED%2BJames%2BPatterson%2Band%2BJames%2BO.%2BBorn%26aff%3DNYT",
								},
							],
						},
						{
							age_group: "",
							amazon_product_url:
								"http://www.amazon.com/Ends-Us-Novel-Colleen-Hoover-ebook/dp/B0176M3U10?tag=NYTBSREV-20",
							article_chapter_link: "",
							author: "Colleen Hoover",
							book_image:
								"https://storage.googleapis.com/du-prd/books/images/9781501110375.jpg",
							book_image_width: 319,
							book_image_height: 495,
							book_review_link: "",
							contributor: "by Colleen Hoover",
							contributor_note: "",
							created_date: "2023-07-19 22:10:24",
							description:
								"A battered wife raised in a violent home attempts to halt the cycle of abuse.",
							first_chapter_link: "",
							price: "0.00",
							primary_isbn10: "1501110365",
							primary_isbn13: "9781501110368",
							book_uri:
								"nyt://book/e2a3545e-e9cb-5828-9d97-50a798a0e4f6",
							publisher: "Atria",
							rank: 4,
							rank_last_week: 3,
							sunday_review_link: "",
							title: "IT ENDS WITH US",
							updated_date: "2023-07-19 22:14:40",
							weeks_on_list: 109,
							buy_links: [
								{
									name: "Amazon",
									url: "http://www.amazon.com/Ends-Us-Novel-Colleen-Hoover-ebook/dp/B0176M3U10?tag=NYTBSREV-20",
								},
								{
									name: "Apple Books",
									url: "https://goto.applebooks.apple/9781501110368?at=10lIEQ",
								},
								{
									name: "Barnes and Noble",
									url: "https://www.anrdoezrs.net/click-7990613-11819508?url=https%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9781501110368",
								},
								{
									name: "Books-A-Million",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fp%252FIT%252BENDS%252BWITH%252BUS%252FColleen%252BHoover%252F9781501110368&url2=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fsearch%253Fquery%253DIT%252BENDS%252BWITH%252BUS%252BColleen%252BHoover",
								},
								{
									name: "Bookshop",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fbookshop.org%2Fa%2F3546%2F9781501110368&url2=https%3A%2F%2Fbookshop.org%2Fbooks%3Faffiliate%3D3546%26keywords%3DIT%2BENDS%2BWITH%2BUS",
								},
								{
									name: "IndieBound",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.indiebound.org%2Fbook%2F9781501110368%3Faff%3DNYT&url2=https%3A%2F%2Fwww.indiebound.org%2Fsearch%2Fbook%3Fkeys%3DIT%2BENDS%2BWITH%2BUS%2BColleen%2BHoover%26aff%3DNYT",
								},
							],
						},
						{
							age_group: "",
							amazon_product_url:
								"https://www.amazon.com/dp/1668001225?tag=NYTBSREV-20",
							article_chapter_link: "",
							author: "Colleen Hoover",
							book_image:
								"https://storage.googleapis.com/du-prd/books/images/9781668001226.jpg",
							book_image_width: 322,
							book_image_height: 500,
							book_review_link: "",
							contributor: "by Colleen Hoover",
							contributor_note: "",
							created_date: "2023-07-19 22:10:24",
							description:
								"In the sequel to “It Ends With Us,” Lily deals with her jealous ex-husband as she reconnects with her first boyfriend.",
							first_chapter_link: "",
							price: "0.00",
							primary_isbn10: "1668001225",
							primary_isbn13: "9781668001226",
							book_uri:
								"nyt://book/3aa85e47-4df9-53ef-9957-a77753d3502c",
							publisher: "Atria",
							rank: 5,
							rank_last_week: 7,
							sunday_review_link: "",
							title: "IT STARTS WITH US",
							updated_date: "2023-07-19 22:14:40",
							weeks_on_list: 39,
							buy_links: [
								{
									name: "Amazon",
									url: "https://www.amazon.com/dp/1668001225?tag=NYTBSREV-20",
								},
								{
									name: "Apple Books",
									url: "https://goto.applebooks.apple/9781668001226?at=10lIEQ",
								},
								{
									name: "Barnes and Noble",
									url: "https://www.anrdoezrs.net/click-7990613-11819508?url=https%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9781668001226",
								},
								{
									name: "Books-A-Million",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fp%252FIT%252BSTARTS%252BWITH%252BUS%252FColleen%252BHoover%252F9781668001226&url2=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fsearch%253Fquery%253DIT%252BSTARTS%252BWITH%252BUS%252BColleen%252BHoover",
								},
								{
									name: "Bookshop",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fbookshop.org%2Fa%2F3546%2F9781668001226&url2=https%3A%2F%2Fbookshop.org%2Fbooks%3Faffiliate%3D3546%26keywords%3DIT%2BSTARTS%2BWITH%2BUS",
								},
								{
									name: "IndieBound",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.indiebound.org%2Fbook%2F9781668001226%3Faff%3DNYT&url2=https%3A%2F%2Fwww.indiebound.org%2Fsearch%2Fbook%3Fkeys%3DIT%2BSTARTS%2BWITH%2BUS%2BColleen%2BHoover%26aff%3DNYT",
								},
							],
						},
					],
				},
				{
					list_id: 708,
					list_name: "Combined Print and E-Book Nonfiction",
					list_name_encoded: "combined-print-and-e-book-nonfiction",
					display_name: "Combined Print & E-Book Nonfiction",
					updated: "WEEKLY",
					list_image: null,
					list_image_width: null,
					list_image_height: null,
					books: [
						{
							age_group: "",
							amazon_product_url:
								"https://www.amazon.com/dp/1250326753?tag=NYTBSREV-20",
							article_chapter_link: "",
							author: "BTS and Myeongseok Kang",
							book_image:
								"https://storage.googleapis.com/du-prd/books/images/9781250326751.jpg",
							book_image_width: 336,
							book_image_height: 500,
							book_review_link: "",
							contributor: "by BTS and Myeongseok Kang",
							contributor_note: "",
							created_date: "2023-07-19 22:10:25",
							description:
								"An oral history of the K-pop group that is celebrating its 10th anniversary.",
							first_chapter_link: "",
							price: "0.00",
							primary_isbn10: "1250326753",
							primary_isbn13: "9781250326751",
							book_uri:
								"nyt://book/4fb7ac9f-69d4-5758-b614-3ca0f5faf11d",
							publisher: "Flatiron",
							rank: 1,
							rank_last_week: 0,
							sunday_review_link: "",
							title: "BEYOND THE STORY",
							updated_date: "2023-07-19 22:14:40",
							weeks_on_list: 1,
							buy_links: [
								{
									name: "Amazon",
									url: "https://www.amazon.com/dp/1250326753?tag=NYTBSREV-20",
								},
								{
									name: "Apple Books",
									url: "https://goto.applebooks.apple/9781250326751?at=10lIEQ",
								},
								{
									name: "Barnes and Noble",
									url: "https://www.anrdoezrs.net/click-7990613-11819508?url=https%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9781250326751",
								},
								{
									name: "Books-A-Million",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fp%252FBEYOND%252BTHE%252BSTORY%252FBTS%252Band%252BMyeongseok%252BKang%252F9781250326751&url2=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fsearch%253Fquery%253DBEYOND%252BTHE%252BSTORY%252BBTS%252Band%252BMyeongseok%252BKang",
								},
								{
									name: "Bookshop",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fbookshop.org%2Fa%2F3546%2F9781250326751&url2=https%3A%2F%2Fbookshop.org%2Fbooks%3Faffiliate%3D3546%26keywords%3DBEYOND%2BTHE%2BSTORY",
								},
								{
									name: "IndieBound",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.indiebound.org%2Fbook%2F9781250326751%3Faff%3DNYT&url2=https%3A%2F%2Fwww.indiebound.org%2Fsearch%2Fbook%3Fkeys%3DBEYOND%2BTHE%2BSTORY%2BBTS%2Band%2BMyeongseok%2BKang%26aff%3DNYT",
								},
							],
						},
						{
							age_group: "",
							amazon_product_url:
								"https://www.amazon.com/dp/0375726268?tag=NYTBSREV-20",
							article_chapter_link: "",
							author: "Kai Bird and Martin J. Sherwin",
							book_image:
								"https://storage.googleapis.com/du-prd/books/images/9780375726262.jpg",
							book_image_width: 323,
							book_image_height: 500,
							book_review_link: "",
							contributor: "by Kai Bird and Martin J. Sherwin",
							contributor_note: "",
							created_date: "2023-07-19 22:10:25",
							description:
								"A biography of J. Robert Oppenheimer. Winner of the Pulitzer Prize in 2006 and an inspiration for the film “Oppenheimer.”",
							first_chapter_link: "",
							price: "0.00",
							primary_isbn10: "0375726268",
							primary_isbn13: "9780375726262",
							book_uri:
								"nyt://book/5a66cfd7-9fd6-5795-8a97-d06ad76ec3f5",
							publisher: "Vintage",
							rank: 2,
							rank_last_week: 11,
							sunday_review_link: "",
							title: "AMERICAN PROMETHEUS",
							updated_date: "2023-07-19 22:14:40",
							weeks_on_list: 2,
							buy_links: [
								{
									name: "Amazon",
									url: "https://www.amazon.com/dp/0375726268?tag=NYTBSREV-20",
								},
								{
									name: "Apple Books",
									url: "https://goto.applebooks.apple/9780375726262?at=10lIEQ",
								},
								{
									name: "Barnes and Noble",
									url: "https://www.anrdoezrs.net/click-7990613-11819508?url=https%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9780375726262",
								},
								{
									name: "Books-A-Million",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fp%252FAMERICAN%252BPROMETHEUS%252FKai%252BBird%252Band%252BMartin%252BJ.%252BSherwin%252F9780375726262&url2=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fsearch%253Fquery%253DAMERICAN%252BPROMETHEUS%252BKai%252BBird%252Band%252BMartin%252BJ.%252BSherwin",
								},
								{
									name: "Bookshop",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fbookshop.org%2Fa%2F3546%2F9780375726262&url2=https%3A%2F%2Fbookshop.org%2Fbooks%3Faffiliate%3D3546%26keywords%3DAMERICAN%2BPROMETHEUS",
								},
								{
									name: "IndieBound",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.indiebound.org%2Fbook%2F9780375726262%3Faff%3DNYT&url2=https%3A%2F%2Fwww.indiebound.org%2Fsearch%2Fbook%3Fkeys%3DAMERICAN%2BPROMETHEUS%2BKai%2BBird%2Band%2BMartin%2BJ.%2BSherwin%26aff%3DNYT",
								},
							],
						},
						{
							age_group: "",
							amazon_product_url:
								"https://www.amazon.com/Killers-Flower-Moon-Osage-Murders/dp/0385534248?tag=NYTBSREV-20",
							article_chapter_link: "",
							author: "David Grann",
							book_image:
								"https://storage.googleapis.com/du-prd/books/images/9780385534246.jpg",
							book_image_width: 326,
							book_image_height: 495,
							book_review_link:
								"https://www.nytimes.com/2017/04/28/books/review/killers-of-the-flower-moon-david-grann.html",
							contributor: "by David Grann",
							contributor_note: "",
							created_date: "2023-07-19 22:10:25",
							description:
								"The story of a murder spree in 1920s Oklahoma that targeted Osage Indians, whose lands contained oil.",
							first_chapter_link: "",
							price: "0.00",
							primary_isbn10: "0307742482",
							primary_isbn13: "9780307742483",
							book_uri:
								"nyt://book/c5c1cd05-cdbc-5e7a-8255-1923ab4b1ceb",
							publisher: "Doubleday",
							rank: 3,
							rank_last_week: 1,
							sunday_review_link: "",
							title: "KILLERS OF THE FLOWER MOON",
							updated_date: "2023-07-19 22:14:40",
							weeks_on_list: 85,
							buy_links: [
								{
									name: "Amazon",
									url: "https://www.amazon.com/Killers-Flower-Moon-Osage-Murders/dp/0385534248?tag=NYTBSREV-20",
								},
								{
									name: "Apple Books",
									url: "https://goto.applebooks.apple/9780307742483?at=10lIEQ",
								},
								{
									name: "Barnes and Noble",
									url: "https://www.anrdoezrs.net/click-7990613-11819508?url=https%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9780307742483",
								},
								{
									name: "Books-A-Million",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fp%252FKILLERS%252BOF%252BTHE%252BFLOWER%252BMOON%252FDavid%252BGrann%252F9780307742483&url2=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fsearch%253Fquery%253DKILLERS%252BOF%252BTHE%252BFLOWER%252BMOON%252BDavid%252BGrann",
								},
								{
									name: "Bookshop",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fbookshop.org%2Fa%2F3546%2F9780307742483&url2=https%3A%2F%2Fbookshop.org%2Fbooks%3Faffiliate%3D3546%26keywords%3DKILLERS%2BOF%2BTHE%2BFLOWER%2BMOON",
								},
								{
									name: "IndieBound",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.indiebound.org%2Fbook%2F9780307742483%3Faff%3DNYT&url2=https%3A%2F%2Fwww.indiebound.org%2Fsearch%2Fbook%3Fkeys%3DKILLERS%2BOF%2BTHE%2BFLOWER%2BMOON%2BDavid%2BGrann%26aff%3DNYT",
								},
							],
						},
						{
							age_group: "",
							amazon_product_url:
								"https://www.amazon.com/dp/0593236599?tag=NYTBSREV-20",
							article_chapter_link: "",
							author: "Peter Attia with Bill Gifford",
							book_image:
								"https://storage.googleapis.com/du-prd/books/images/9780593236598.jpg",
							book_image_width: 385,
							book_image_height: 500,
							book_review_link: "",
							contributor: "by Peter Attia with Bill Gifford",
							contributor_note: "",
							created_date: "2023-07-19 22:10:25",
							description:
								"A look at recent scientific research on aging and longevity.",
							first_chapter_link: "",
							price: "0.00",
							primary_isbn10: "0593236599",
							primary_isbn13: "9780593236598",
							book_uri:
								"nyt://book/69f6e624-4f30-59d6-8103-816d17ae5ec1",
							publisher: "Harmony",
							rank: 4,
							rank_last_week: 3,
							sunday_review_link: "",
							title: "OUTLIVE",
							updated_date: "2023-07-19 22:14:40",
							weeks_on_list: 16,
							buy_links: [
								{
									name: "Amazon",
									url: "https://www.amazon.com/dp/0593236599?tag=NYTBSREV-20",
								},
								{
									name: "Apple Books",
									url: "https://goto.applebooks.apple/9780593236598?at=10lIEQ",
								},
								{
									name: "Barnes and Noble",
									url: "https://www.anrdoezrs.net/click-7990613-11819508?url=https%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9780593236598",
								},
								{
									name: "Books-A-Million",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fp%252FOUTLIVE%252FPeter%252BAttia%252Bwith%252BBill%252BGifford%252F9780593236598&url2=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fsearch%253Fquery%253DOUTLIVE%252BPeter%252BAttia%252Bwith%252BBill%252BGifford",
								},
								{
									name: "Bookshop",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fbookshop.org%2Fa%2F3546%2F9780593236598&url2=https%3A%2F%2Fbookshop.org%2Fbooks%3Faffiliate%3D3546%26keywords%3DOUTLIVE",
								},
								{
									name: "IndieBound",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.indiebound.org%2Fbook%2F9780593236598%3Faff%3DNYT&url2=https%3A%2F%2Fwww.indiebound.org%2Fsearch%2Fbook%3Fkeys%3DOUTLIVE%2BPeter%2BAttia%2Bwith%2BBill%2BGifford%26aff%3DNYT",
								},
							],
						},
						{
							age_group: "",
							amazon_product_url:
								"https://www.amazon.com/dp/0385534264?tag=NYTBSREV-20",
							article_chapter_link: "",
							author: "David Grann",
							book_image:
								"https://storage.googleapis.com/du-prd/books/images/9780385534260.jpg",
							book_image_width: 329,
							book_image_height: 500,
							book_review_link: "",
							contributor: "by David Grann",
							contributor_note: "",
							created_date: "2023-07-19 22:10:25",
							description:
								"The survivors of a shipwrecked British vessel on a secret mission during an imperial war with Spain have different accounts of events.",
							first_chapter_link: "",
							price: "0.00",
							primary_isbn10: "0385534264",
							primary_isbn13: "9780385534260",
							book_uri:
								"nyt://book/92ec85ca-4cff-59b6-94a3-b81002bfc8e6",
							publisher: "Doubleday",
							rank: 5,
							rank_last_week: 2,
							sunday_review_link: "",
							title: "THE WAGER",
							updated_date: "2023-07-19 22:14:40",
							weeks_on_list: 13,
							buy_links: [
								{
									name: "Amazon",
									url: "https://www.amazon.com/dp/0385534264?tag=NYTBSREV-20",
								},
								{
									name: "Apple Books",
									url: "https://goto.applebooks.apple/9780385534260?at=10lIEQ",
								},
								{
									name: "Barnes and Noble",
									url: "https://www.anrdoezrs.net/click-7990613-11819508?url=https%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9780385534260",
								},
								{
									name: "Books-A-Million",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fp%252FTHE%252BWAGER%252FDavid%252BGrann%252F9780385534260&url2=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fsearch%253Fquery%253DTHE%252BWAGER%252BDavid%252BGrann",
								},
								{
									name: "Bookshop",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fbookshop.org%2Fa%2F3546%2F9780385534260&url2=https%3A%2F%2Fbookshop.org%2Fbooks%3Faffiliate%3D3546%26keywords%3DTHE%2BWAGER",
								},
								{
									name: "IndieBound",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.indiebound.org%2Fbook%2F9780385534260%3Faff%3DNYT&url2=https%3A%2F%2Fwww.indiebound.org%2Fsearch%2Fbook%3Fkeys%3DTHE%2BWAGER%2BDavid%2BGrann%26aff%3DNYT",
								},
							],
						},
					],
				},
				{
					list_id: 1,
					list_name: "Hardcover Fiction",
					list_name_encoded: "hardcover-fiction",
					display_name: "Hardcover Fiction",
					updated: "WEEKLY",
					list_image: null,
					list_image_width: null,
					list_image_height: null,
					books: [
						{
							age_group: "",
							amazon_product_url:
								"https://www.amazon.com/dp/1649374046?tag=NYTBSREV-20",
							article_chapter_link: "",
							author: "Rebecca Yarros",
							book_image:
								"https://storage.googleapis.com/du-prd/books/images/9781649374042.jpg",
							book_image_width: 309,
							book_image_height: 500,
							book_review_link: "",
							contributor: "by Rebecca Yarros",
							contributor_note: "",
							created_date: "2023-07-19 22:10:18",
							description:
								"Violet Sorrengail is urged by the commanding general, who also is her mother, to become a candidate for the elite dragon riders.",
							first_chapter_link: "",
							price: "0.00",
							primary_isbn10: "1649374046",
							primary_isbn13: "9781649374042",
							book_uri:
								"nyt://book/106c2cf0-7d20-51b1-bad4-91c3ebcd131a",
							publisher: "Red Tower",
							rank: 1,
							rank_last_week: 1,
							sunday_review_link: "",
							title: "FOURTH WING",
							updated_date: "2023-07-19 22:14:34",
							weeks_on_list: 10,
							buy_links: [
								{
									name: "Amazon",
									url: "https://www.amazon.com/dp/1649374046?tag=NYTBSREV-20",
								},
								{
									name: "Apple Books",
									url: "https://goto.applebooks.apple/9781649374042?at=10lIEQ",
								},
								{
									name: "Barnes and Noble",
									url: "https://www.anrdoezrs.net/click-7990613-11819508?url=https%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9781649374042",
								},
								{
									name: "Books-A-Million",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fp%252FFOURTH%252BWING%252FRebecca%252BYarros%252F9781649374042&url2=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fsearch%253Fquery%253DFOURTH%252BWING%252BRebecca%252BYarros",
								},
								{
									name: "Bookshop",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fbookshop.org%2Fa%2F3546%2F9781649374042&url2=https%3A%2F%2Fbookshop.org%2Fbooks%3Faffiliate%3D3546%26keywords%3DFOURTH%2BWING",
								},
								{
									name: "IndieBound",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.indiebound.org%2Fbook%2F9781649374042%3Faff%3DNYT&url2=https%3A%2F%2Fwww.indiebound.org%2Fsearch%2Fbook%3Fkeys%3DFOURTH%2BWING%2BRebecca%2BYarros%26aff%3DNYT",
								},
							],
						},
						{
							age_group: "",
							amazon_product_url:
								"https://www.amazon.com/dp/0593441273?tag=NYTBSREV-20",
							article_chapter_link: "",
							author: "Emily Henry",
							book_image:
								"https://storage.googleapis.com/du-prd/books/images/9780593441275.jpg",
							book_image_width: 331,
							book_image_height: 500,
							book_review_link: "",
							contributor: "by Emily Henry",
							contributor_note: "",
							created_date: "2023-07-19 22:10:18",
							description:
								"A former couple pretend to be together for the sake of their friends during their annual getaway in Maine.",
							first_chapter_link: "",
							price: "0.00",
							primary_isbn10: "0593441273",
							primary_isbn13: "9780593441275",
							book_uri:
								"nyt://book/9ab3a59a-e572-5452-ae0c-eedeadb05831",
							publisher: "Berkley",
							rank: 2,
							rank_last_week: 2,
							sunday_review_link: "",
							title: "HAPPY PLACE",
							updated_date: "2023-07-19 22:14:34",
							weeks_on_list: 12,
							buy_links: [
								{
									name: "Amazon",
									url: "https://www.amazon.com/dp/0593441273?tag=NYTBSREV-20",
								},
								{
									name: "Apple Books",
									url: "https://goto.applebooks.apple/9780593441275?at=10lIEQ",
								},
								{
									name: "Barnes and Noble",
									url: "https://www.anrdoezrs.net/click-7990613-11819508?url=https%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9780593441275",
								},
								{
									name: "Books-A-Million",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fp%252FHAPPY%252BPLACE%252FEmily%252BHenry%252F9780593441275&url2=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fsearch%253Fquery%253DHAPPY%252BPLACE%252BEmily%252BHenry",
								},
								{
									name: "Bookshop",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fbookshop.org%2Fa%2F3546%2F9780593441275&url2=https%3A%2F%2Fbookshop.org%2Fbooks%3Faffiliate%3D3546%26keywords%3DHAPPY%2BPLACE",
								},
								{
									name: "IndieBound",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.indiebound.org%2Fbook%2F9780593441275%3Faff%3DNYT&url2=https%3A%2F%2Fwww.indiebound.org%2Fsearch%2Fbook%3Fkeys%3DHAPPY%2BPLACE%2BEmily%2BHenry%26aff%3DNYT",
								},
							],
						},
						{
							age_group: "",
							amazon_product_url:
								"https://www.amazon.com/dp/038554734X?tag=NYTBSREV-20",
							article_chapter_link: "",
							author: "Bonnie Garmus",
							book_image:
								"https://storage.googleapis.com/du-prd/books/images/9780385547345.jpg",
							book_image_width: 328,
							book_image_height: 500,
							book_review_link: "",
							contributor: "by Bonnie Garmus",
							contributor_note: "",
							created_date: "2023-07-19 22:10:18",
							description:
								"A scientist and single mother living in California in the 1960s becomes a star on a TV cooking show.",
							first_chapter_link: "",
							price: "0.00",
							primary_isbn10: "038554734X",
							primary_isbn13: "9780385547345",
							book_uri:
								"nyt://book/717e0ead-9782-567d-af27-d9a6cfb1856f",
							publisher: "Doubleday",
							rank: 3,
							rank_last_week: 3,
							sunday_review_link: "",
							title: "LESSONS IN CHEMISTRY",
							updated_date: "2023-07-19 22:14:34",
							weeks_on_list: 62,
							buy_links: [
								{
									name: "Amazon",
									url: "https://www.amazon.com/dp/038554734X?tag=NYTBSREV-20",
								},
								{
									name: "Apple Books",
									url: "https://goto.applebooks.apple/9780385547345?at=10lIEQ",
								},
								{
									name: "Barnes and Noble",
									url: "https://www.anrdoezrs.net/click-7990613-11819508?url=https%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9780385547345",
								},
								{
									name: "Books-A-Million",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fp%252FLESSONS%252BIN%252BCHEMISTRY%252FBonnie%252BGarmus%252F9780385547345&url2=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fsearch%253Fquery%253DLESSONS%252BIN%252BCHEMISTRY%252BBonnie%252BGarmus",
								},
								{
									name: "Bookshop",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fbookshop.org%2Fa%2F3546%2F9780385547345&url2=https%3A%2F%2Fbookshop.org%2Fbooks%3Faffiliate%3D3546%26keywords%3DLESSONS%2BIN%2BCHEMISTRY",
								},
								{
									name: "IndieBound",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.indiebound.org%2Fbook%2F9780385547345%3Faff%3DNYT&url2=https%3A%2F%2Fwww.indiebound.org%2Fsearch%2Fbook%3Fkeys%3DLESSONS%2BIN%2BCHEMISTRY%2BBonnie%2BGarmus%26aff%3DNYT",
								},
							],
						},
						{
							age_group: "",
							amazon_product_url:
								"https://www.amazon.com/dp/0316499579?tag=NYTBSREV-20",
							article_chapter_link: "",
							author: "James Patterson and James O. Born",
							book_image:
								"https://storage.googleapis.com/du-prd/books/images/9780316499576.jpg",
							book_image_width: 322,
							book_image_height: 500,
							book_review_link: "",
							contributor: "by James Patterson and James O. Born",
							contributor_note: "",
							created_date: "2023-07-19 22:10:18",
							description:
								"A killer, who targets women in New York City, becomes obsessed with Michael Bennett’s daughter.",
							first_chapter_link: "",
							price: "0.00",
							primary_isbn10: "0316499579",
							primary_isbn13: "9780316499576",
							book_uri:
								"nyt://book/f1421518-0d0e-5dc4-b96e-a71560ef0aff",
							publisher: "Little, Brown",
							rank: 4,
							rank_last_week: 0,
							sunday_review_link: "",
							title: "OBSESSED",
							updated_date: "2023-07-19 22:14:34",
							weeks_on_list: 1,
							buy_links: [
								{
									name: "Amazon",
									url: "https://www.amazon.com/dp/0316499579?tag=NYTBSREV-20",
								},
								{
									name: "Apple Books",
									url: "https://goto.applebooks.apple/9780316499576?at=10lIEQ",
								},
								{
									name: "Barnes and Noble",
									url: "https://www.anrdoezrs.net/click-7990613-11819508?url=https%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9780316499576",
								},
								{
									name: "Books-A-Million",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fp%252FOBSESSED%252FJames%252BPatterson%252Band%252BJames%252BO.%252BBorn%252F9780316499576&url2=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fsearch%253Fquery%253DOBSESSED%252BJames%252BPatterson%252Band%252BJames%252BO.%252BBorn",
								},
								{
									name: "Bookshop",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fbookshop.org%2Fa%2F3546%2F9780316499576&url2=https%3A%2F%2Fbookshop.org%2Fbooks%3Faffiliate%3D3546%26keywords%3DOBSESSED",
								},
								{
									name: "IndieBound",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.indiebound.org%2Fbook%2F9780316499576%3Faff%3DNYT&url2=https%3A%2F%2Fwww.indiebound.org%2Fsearch%2Fbook%3Fkeys%3DOBSESSED%2BJames%2BPatterson%2Band%2BJames%2BO.%2BBorn%26aff%3DNYT",
								},
							],
						},
						{
							age_group: "",
							amazon_product_url:
								"https://www.amazon.com/dp/0316258776?tag=NYTBSREV-20",
							article_chapter_link: "",
							author: "Elin Hilderbrand",
							book_image:
								"https://storage.googleapis.com/du-prd/books/images/9780316258777.jpg",
							book_image_width: 323,
							book_image_height: 500,
							book_review_link: "",
							contributor: "by Elin Hilderbrand",
							contributor_note: "",
							created_date: "2023-07-19 22:10:18",
							description:
								"After a tragedy, a popular food blogger brings friends from distinct times in her life to spend a weekend in Nantucket.",
							first_chapter_link: "",
							price: "0.00",
							primary_isbn10: "0316258776",
							primary_isbn13: "9780316258777",
							book_uri:
								"nyt://book/8c779b4e-ab10-5946-a522-e8dd1167b1db",
							publisher: "Little, Brown",
							rank: 5,
							rank_last_week: 4,
							sunday_review_link: "",
							title: "THE FIVE-STAR WEEKEND",
							updated_date: "2023-07-19 22:14:34",
							weeks_on_list: 5,
							buy_links: [
								{
									name: "Amazon",
									url: "https://www.amazon.com/dp/0316258776?tag=NYTBSREV-20",
								},
								{
									name: "Apple Books",
									url: "https://goto.applebooks.apple/9780316258777?at=10lIEQ",
								},
								{
									name: "Barnes and Noble",
									url: "https://www.anrdoezrs.net/click-7990613-11819508?url=https%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9780316258777",
								},
								{
									name: "Books-A-Million",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fp%252FTHE%252BFIVE-STAR%252BWEEKEND%252FElin%252BHilderbrand%252F9780316258777&url2=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fsearch%253Fquery%253DTHE%252BFIVE-STAR%252BWEEKEND%252BElin%252BHilderbrand",
								},
								{
									name: "Bookshop",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fbookshop.org%2Fa%2F3546%2F9780316258777&url2=https%3A%2F%2Fbookshop.org%2Fbooks%3Faffiliate%3D3546%26keywords%3DTHE%2BFIVE-STAR%2BWEEKEND",
								},
								{
									name: "IndieBound",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.indiebound.org%2Fbook%2F9780316258777%3Faff%3DNYT&url2=https%3A%2F%2Fwww.indiebound.org%2Fsearch%2Fbook%3Fkeys%3DTHE%2BFIVE-STAR%2BWEEKEND%2BElin%2BHilderbrand%26aff%3DNYT",
								},
							],
						},
					],
				},
				{
					list_id: 2,
					list_name: "Hardcover Nonfiction",
					list_name_encoded: "hardcover-nonfiction",
					display_name: "Hardcover Nonfiction",
					updated: "WEEKLY",
					list_image: null,
					list_image_width: null,
					list_image_height: null,
					books: [
						{
							age_group: "",
							amazon_product_url:
								"https://www.amazon.com/dp/1250326753?tag=NYTBSREV-20",
							article_chapter_link: "",
							author: "BTS and Myeongseok Kang",
							book_image:
								"https://storage.googleapis.com/du-prd/books/images/9781250326751.jpg",
							book_image_width: 336,
							book_image_height: 500,
							book_review_link: "",
							contributor: "by BTS and Myeongseok Kang",
							contributor_note: "",
							created_date: "2023-07-19 22:10:21",
							description:
								"An oral history of the K-pop group that is celebrating its 10th anniversary.",
							first_chapter_link: "",
							price: "0.00",
							primary_isbn10: "1250326753",
							primary_isbn13: "9781250326751",
							book_uri:
								"nyt://book/4fb7ac9f-69d4-5758-b614-3ca0f5faf11d",
							publisher: "Flatiron",
							rank: 1,
							rank_last_week: 0,
							sunday_review_link: "",
							title: "BEYOND THE STORY",
							updated_date: "2023-07-19 22:14:37",
							weeks_on_list: 1,
							buy_links: [
								{
									name: "Amazon",
									url: "https://www.amazon.com/dp/1250326753?tag=NYTBSREV-20",
								},
								{
									name: "Apple Books",
									url: "https://goto.applebooks.apple/9781250326751?at=10lIEQ",
								},
								{
									name: "Barnes and Noble",
									url: "https://www.anrdoezrs.net/click-7990613-11819508?url=https%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9781250326751",
								},
								{
									name: "Books-A-Million",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fp%252FBEYOND%252BTHE%252BSTORY%252FBTS%252Band%252BMyeongseok%252BKang%252F9781250326751&url2=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fsearch%253Fquery%253DBEYOND%252BTHE%252BSTORY%252BBTS%252Band%252BMyeongseok%252BKang",
								},
								{
									name: "Bookshop",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fbookshop.org%2Fa%2F3546%2F9781250326751&url2=https%3A%2F%2Fbookshop.org%2Fbooks%3Faffiliate%3D3546%26keywords%3DBEYOND%2BTHE%2BSTORY",
								},
								{
									name: "IndieBound",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.indiebound.org%2Fbook%2F9781250326751%3Faff%3DNYT&url2=https%3A%2F%2Fwww.indiebound.org%2Fsearch%2Fbook%3Fkeys%3DBEYOND%2BTHE%2BSTORY%2BBTS%2Band%2BMyeongseok%2BKang%26aff%3DNYT",
								},
							],
						},
						{
							age_group: "",
							amazon_product_url:
								"https://www.amazon.com/dp/0593236599?tag=NYTBSREV-20",
							article_chapter_link: "",
							author: "Peter Attia with Bill Gifford",
							book_image:
								"https://storage.googleapis.com/du-prd/books/images/9780593236598.jpg",
							book_image_width: 385,
							book_image_height: 500,
							book_review_link: "",
							contributor: "by Peter Attia with Bill Gifford",
							contributor_note: "",
							created_date: "2023-07-19 22:10:21",
							description:
								"A look at recent scientific research on aging and longevity.",
							first_chapter_link: "",
							price: "0.00",
							primary_isbn10: "0593236599",
							primary_isbn13: "9780593236598",
							book_uri:
								"nyt://book/69f6e624-4f30-59d6-8103-816d17ae5ec1",
							publisher: "Harmony",
							rank: 2,
							rank_last_week: 1,
							sunday_review_link: "",
							title: "OUTLIVE",
							updated_date: "2023-07-19 22:14:37",
							weeks_on_list: 16,
							buy_links: [
								{
									name: "Amazon",
									url: "https://www.amazon.com/dp/0593236599?tag=NYTBSREV-20",
								},
								{
									name: "Apple Books",
									url: "https://goto.applebooks.apple/9780593236598?at=10lIEQ",
								},
								{
									name: "Barnes and Noble",
									url: "https://www.anrdoezrs.net/click-7990613-11819508?url=https%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9780593236598",
								},
								{
									name: "Books-A-Million",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fp%252FOUTLIVE%252FPeter%252BAttia%252Bwith%252BBill%252BGifford%252F9780593236598&url2=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fsearch%253Fquery%253DOUTLIVE%252BPeter%252BAttia%252Bwith%252BBill%252BGifford",
								},
								{
									name: "Bookshop",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fbookshop.org%2Fa%2F3546%2F9780593236598&url2=https%3A%2F%2Fbookshop.org%2Fbooks%3Faffiliate%3D3546%26keywords%3DOUTLIVE",
								},
								{
									name: "IndieBound",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.indiebound.org%2Fbook%2F9780593236598%3Faff%3DNYT&url2=https%3A%2F%2Fwww.indiebound.org%2Fsearch%2Fbook%3Fkeys%3DOUTLIVE%2BPeter%2BAttia%2Bwith%2BBill%2BGifford%26aff%3DNYT",
								},
							],
						},
						{
							age_group: "",
							amazon_product_url:
								"https://www.amazon.com/dp/0385534264?tag=NYTBSREV-20",
							article_chapter_link: "",
							author: "David Grann",
							book_image:
								"https://storage.googleapis.com/du-prd/books/images/9780385534260.jpg",
							book_image_width: 329,
							book_image_height: 500,
							book_review_link: "",
							contributor: "by David Grann",
							contributor_note: "",
							created_date: "2023-07-19 22:10:21",
							description:
								"The survivors of a shipwrecked British vessel on a secret mission during an imperial war with Spain have different accounts of events.",
							first_chapter_link: "",
							price: "0.00",
							primary_isbn10: "0385534264",
							primary_isbn13: "9780385534260",
							book_uri:
								"nyt://book/92ec85ca-4cff-59b6-94a3-b81002bfc8e6",
							publisher: "Doubleday",
							rank: 3,
							rank_last_week: 2,
							sunday_review_link: "",
							title: "THE WAGER",
							updated_date: "2023-07-19 22:14:37",
							weeks_on_list: 13,
							buy_links: [
								{
									name: "Amazon",
									url: "https://www.amazon.com/dp/0385534264?tag=NYTBSREV-20",
								},
								{
									name: "Apple Books",
									url: "https://goto.applebooks.apple/9780385534260?at=10lIEQ",
								},
								{
									name: "Barnes and Noble",
									url: "https://www.anrdoezrs.net/click-7990613-11819508?url=https%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9780385534260",
								},
								{
									name: "Books-A-Million",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fp%252FTHE%252BWAGER%252FDavid%252BGrann%252F9780385534260&url2=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fsearch%253Fquery%253DTHE%252BWAGER%252BDavid%252BGrann",
								},
								{
									name: "Bookshop",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fbookshop.org%2Fa%2F3546%2F9780385534260&url2=https%3A%2F%2Fbookshop.org%2Fbooks%3Faffiliate%3D3546%26keywords%3DTHE%2BWAGER",
								},
								{
									name: "IndieBound",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.indiebound.org%2Fbook%2F9780385534260%3Faff%3DNYT&url2=https%3A%2F%2Fwww.indiebound.org%2Fsearch%2Fbook%3Fkeys%3DTHE%2BWAGER%2BDavid%2BGrann%26aff%3DNYT",
								},
							],
						},
						{
							age_group: "",
							amazon_product_url:
								"https://www.amazon.com/dp/1982185821?tag=NYTBSREV-20",
							article_chapter_link: "",
							author: "Jennette McCurdy",
							book_image:
								"https://storage.googleapis.com/du-prd/books/images/9781982185824.jpg",
							book_image_width: 329,
							book_image_height: 500,
							book_review_link: "",
							contributor: "by Jennette McCurdy",
							contributor_note: "",
							created_date: "2023-07-19 22:10:22",
							description:
								"The actress and filmmaker describes her eating disorders and difficult relationship with her mother.",
							first_chapter_link: "",
							price: "0.00",
							primary_isbn10: "1982185821",
							primary_isbn13: "9781982185824",
							book_uri:
								"nyt://book/be73b1c3-c238-5232-af55-bf70c59cb907",
							publisher: "Simon & Schuster",
							rank: 4,
							rank_last_week: 3,
							sunday_review_link: "",
							title: "I'M GLAD MY MOM DIED",
							updated_date: "2023-07-19 22:14:37",
							weeks_on_list: 49,
							buy_links: [
								{
									name: "Amazon",
									url: "https://www.amazon.com/dp/1982185821?tag=NYTBSREV-20",
								},
								{
									name: "Apple Books",
									url: "https://goto.applebooks.apple/9781982185824?at=10lIEQ",
								},
								{
									name: "Barnes and Noble",
									url: "https://www.anrdoezrs.net/click-7990613-11819508?url=https%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9781982185824",
								},
								{
									name: "Books-A-Million",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fp%252FI%252527M%252BGLAD%252BMY%252BMOM%252BDIED%252FJennette%252BMcCurdy%252F9781982185824&url2=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fsearch%253Fquery%253DI%252527M%252BGLAD%252BMY%252BMOM%252BDIED%252BJennette%252BMcCurdy",
								},
								{
									name: "Bookshop",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fbookshop.org%2Fa%2F3546%2F9781982185824&url2=https%3A%2F%2Fbookshop.org%2Fbooks%3Faffiliate%3D3546%26keywords%3DI%2527M%2BGLAD%2BMY%2BMOM%2BDIED",
								},
								{
									name: "IndieBound",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.indiebound.org%2Fbook%2F9781982185824%3Faff%3DNYT&url2=https%3A%2F%2Fwww.indiebound.org%2Fsearch%2Fbook%3Fkeys%3DI%2527M%2BGLAD%2BMY%2BMOM%2BDIED%2BJennette%2BMcCurdy%26aff%3DNYT",
								},
							],
						},
						{
							age_group: "",
							amazon_product_url:
								"https://www.amazon.com/dp/0063226081?tag=NYTBSREV-20",
							article_chapter_link: "",
							author: "Johnny Joey Jones",
							book_image:
								"https://storage.googleapis.com/du-prd/books/images/9780063226081.jpg",
							book_image_width: 331,
							book_image_height: 500,
							book_review_link: "",
							contributor: "by Johnny Joey Jones",
							contributor_note: "",
							created_date: "2023-07-19 22:10:22",
							description:
								"The Fox News military analyst shares stories from working with veterans for over a decade.",
							first_chapter_link: "",
							price: "0.00",
							primary_isbn10: "0063226081",
							primary_isbn13: "9780063226081",
							book_uri:
								"nyt://book/eb3987a4-4026-5b92-a086-b1dc4dbad328",
							publisher: "Broadside",
							rank: 5,
							rank_last_week: 4,
							sunday_review_link: "",
							title: "UNBROKEN BONDS OF BATTLE",
							updated_date: "2023-07-19 22:14:37",
							weeks_on_list: 3,
							buy_links: [
								{
									name: "Amazon",
									url: "https://www.amazon.com/dp/0063226081?tag=NYTBSREV-20",
								},
								{
									name: "Apple Books",
									url: "https://goto.applebooks.apple/9780063226081?at=10lIEQ",
								},
								{
									name: "Barnes and Noble",
									url: "https://www.anrdoezrs.net/click-7990613-11819508?url=https%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9780063226081",
								},
								{
									name: "Books-A-Million",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fp%252FUNBROKEN%252BBONDS%252BOF%252BBATTLE%252FJohnny%252BJoey%252BJones%252F9780063226081&url2=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fsearch%253Fquery%253DUNBROKEN%252BBONDS%252BOF%252BBATTLE%252BJohnny%252BJoey%252BJones",
								},
								{
									name: "Bookshop",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fbookshop.org%2Fa%2F3546%2F9780063226081&url2=https%3A%2F%2Fbookshop.org%2Fbooks%3Faffiliate%3D3546%26keywords%3DUNBROKEN%2BBONDS%2BOF%2BBATTLE",
								},
								{
									name: "IndieBound",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.indiebound.org%2Fbook%2F9780063226081%3Faff%3DNYT&url2=https%3A%2F%2Fwww.indiebound.org%2Fsearch%2Fbook%3Fkeys%3DUNBROKEN%2BBONDS%2BOF%2BBATTLE%2BJohnny%2BJoey%2BJones%26aff%3DNYT",
								},
							],
						},
					],
				},
				{
					list_id: 17,
					list_name: "Trade Fiction Paperback",
					list_name_encoded: "trade-fiction-paperback",
					display_name: "Paperback Trade Fiction",
					updated: "WEEKLY",
					list_image: null,
					list_image_width: null,
					list_image_height: null,
					books: [
						{
							age_group: "",
							amazon_product_url:
								"https://www.amazon.com/dp/1538756595?tag=NYTBSREV-20",
							article_chapter_link: "",
							author: "Colleen Hoover",
							book_image:
								"https://storage.googleapis.com/du-prd/books/images/9781538756591.jpg",
							book_image_width: 333,
							book_image_height: 500,
							book_review_link: "",
							contributor: "by Colleen Hoover",
							contributor_note: "",
							created_date: "2023-07-19 22:10:21",
							description:
								"Dangers develop when a drug trafficker becomes obsessed with a woman who has a mutual attraction to a D.E.A. agent.",
							first_chapter_link: "",
							price: "0.00",
							primary_isbn10: "1538756595",
							primary_isbn13: "9781538756591",
							book_uri:
								"nyt://book/0962ac94-2f91-5500-bcf8-24c90607bc4a",
							publisher: "Grand Central",
							rank: 1,
							rank_last_week: 1,
							sunday_review_link: "",
							title: "TOO LATE",
							updated_date: "2023-07-19 22:14:36",
							weeks_on_list: 3,
							buy_links: [
								{
									name: "Amazon",
									url: "https://www.amazon.com/dp/1538756595?tag=NYTBSREV-20",
								},
								{
									name: "Apple Books",
									url: "https://goto.applebooks.apple/9781538756591?at=10lIEQ",
								},
								{
									name: "Barnes and Noble",
									url: "https://www.anrdoezrs.net/click-7990613-11819508?url=https%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9781538756591",
								},
								{
									name: "Books-A-Million",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fp%252FTOO%252BLATE%252FColleen%252BHoover%252F9781538756591&url2=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fsearch%253Fquery%253DTOO%252BLATE%252BColleen%252BHoover",
								},
								{
									name: "Bookshop",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fbookshop.org%2Fa%2F3546%2F9781538756591&url2=https%3A%2F%2Fbookshop.org%2Fbooks%3Faffiliate%3D3546%26keywords%3DTOO%2BLATE",
								},
								{
									name: "IndieBound",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.indiebound.org%2Fbook%2F9781538756591%3Faff%3DNYT&url2=https%3A%2F%2Fwww.indiebound.org%2Fsearch%2Fbook%3Fkeys%3DTOO%2BLATE%2BColleen%2BHoover%26aff%3DNYT",
								},
							],
						},
						{
							age_group: "",
							amazon_product_url:
								"http://www.amazon.com/Ends-Us-Novel-Colleen-Hoover-ebook/dp/B0176M3U10?tag=NYTBSREV-20",
							article_chapter_link: "",
							author: "Colleen Hoover",
							book_image:
								"https://storage.googleapis.com/du-prd/books/images/9781501110375.jpg",
							book_image_width: 319,
							book_image_height: 495,
							book_review_link: "",
							contributor: "by Colleen Hoover",
							contributor_note: "",
							created_date: "2023-07-19 22:10:21",
							description:
								"A battered wife raised in a violent home attempts to halt the cycle of abuse.",
							first_chapter_link: "",
							price: "0.00",
							primary_isbn10: "1501110365",
							primary_isbn13: "9781501110368",
							book_uri:
								"nyt://book/e2a3545e-e9cb-5828-9d97-50a798a0e4f6",
							publisher: "Atria",
							rank: 2,
							rank_last_week: 2,
							sunday_review_link: "",
							title: "IT ENDS WITH US",
							updated_date: "2023-07-19 22:14:36",
							weeks_on_list: 115,
							buy_links: [
								{
									name: "Amazon",
									url: "http://www.amazon.com/Ends-Us-Novel-Colleen-Hoover-ebook/dp/B0176M3U10?tag=NYTBSREV-20",
								},
								{
									name: "Apple Books",
									url: "https://goto.applebooks.apple/9781501110368?at=10lIEQ",
								},
								{
									name: "Barnes and Noble",
									url: "https://www.anrdoezrs.net/click-7990613-11819508?url=https%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9781501110368",
								},
								{
									name: "Books-A-Million",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fp%252FIT%252BENDS%252BWITH%252BUS%252FColleen%252BHoover%252F9781501110368&url2=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fsearch%253Fquery%253DIT%252BENDS%252BWITH%252BUS%252BColleen%252BHoover",
								},
								{
									name: "Bookshop",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fbookshop.org%2Fa%2F3546%2F9781501110368&url2=https%3A%2F%2Fbookshop.org%2Fbooks%3Faffiliate%3D3546%26keywords%3DIT%2BENDS%2BWITH%2BUS",
								},
								{
									name: "IndieBound",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.indiebound.org%2Fbook%2F9781501110368%3Faff%3DNYT&url2=https%3A%2F%2Fwww.indiebound.org%2Fsearch%2Fbook%3Fkeys%3DIT%2BENDS%2BWITH%2BUS%2BColleen%2BHoover%26aff%3DNYT",
								},
							],
						},
						{
							age_group: "",
							amazon_product_url:
								"https://www.amazon.com/dp/1668001225?tag=NYTBSREV-20",
							article_chapter_link: "",
							author: "Colleen Hoover",
							book_image:
								"https://storage.googleapis.com/du-prd/books/images/9781668001226.jpg",
							book_image_width: 322,
							book_image_height: 500,
							book_review_link: "",
							contributor: "by Colleen Hoover",
							contributor_note: "",
							created_date: "2023-07-19 22:10:21",
							description:
								"In the sequel to “It Ends With Us,” Lily deals with her jealous ex-husband as she reconnects with her first boyfriend.",
							first_chapter_link: "",
							price: "0.00",
							primary_isbn10: "1668001225",
							primary_isbn13: "9781668001226",
							book_uri:
								"nyt://book/3aa85e47-4df9-53ef-9957-a77753d3502c",
							publisher: "Atria",
							rank: 3,
							rank_last_week: 3,
							sunday_review_link: "",
							title: "IT STARTS WITH US",
							updated_date: "2023-07-19 22:14:36",
							weeks_on_list: 39,
							buy_links: [
								{
									name: "Amazon",
									url: "https://www.amazon.com/dp/1668001225?tag=NYTBSREV-20",
								},
								{
									name: "Apple Books",
									url: "https://goto.applebooks.apple/9781668001226?at=10lIEQ",
								},
								{
									name: "Barnes and Noble",
									url: "https://www.anrdoezrs.net/click-7990613-11819508?url=https%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9781668001226",
								},
								{
									name: "Books-A-Million",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fp%252FIT%252BSTARTS%252BWITH%252BUS%252FColleen%252BHoover%252F9781668001226&url2=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fsearch%253Fquery%253DIT%252BSTARTS%252BWITH%252BUS%252BColleen%252BHoover",
								},
								{
									name: "Bookshop",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fbookshop.org%2Fa%2F3546%2F9781668001226&url2=https%3A%2F%2Fbookshop.org%2Fbooks%3Faffiliate%3D3546%26keywords%3DIT%2BSTARTS%2BWITH%2BUS",
								},
								{
									name: "IndieBound",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.indiebound.org%2Fbook%2F9781668001226%3Faff%3DNYT&url2=https%3A%2F%2Fwww.indiebound.org%2Fsearch%2Fbook%3Fkeys%3DIT%2BSTARTS%2BWITH%2BUS%2BColleen%2BHoover%26aff%3DNYT",
								},
							],
						},
						{
							age_group: "",
							amazon_product_url:
								"https://www.amazon.com/dp/1668026031?tag=NYTBSREV-20",
							article_chapter_link: "",
							author: "Hannah Grace",
							book_image:
								"https://storage.googleapis.com/du-prd/books/images/9781668026038.jpg",
							book_image_width: 322,
							book_image_height: 500,
							book_review_link: "",
							contributor: "by Hannah Grace",
							contributor_note: "",
							created_date: "2023-07-19 22:10:21",
							description:
								"Anastasia might need the help of the captain of a college hockey team to get on the Olympic figure skating team.",
							first_chapter_link: "",
							price: "0.00",
							primary_isbn10: "1668026031",
							primary_isbn13: "9781668026038",
							book_uri:
								"nyt://book/a89bf395-aba9-520b-bf2b-7e6245e89ef9",
							publisher: "Atria",
							rank: 4,
							rank_last_week: 4,
							sunday_review_link: "",
							title: "ICEBREAKER",
							updated_date: "2023-07-19 22:14:36",
							weeks_on_list: 22,
							buy_links: [
								{
									name: "Amazon",
									url: "https://www.amazon.com/dp/1668026031?tag=NYTBSREV-20",
								},
								{
									name: "Apple Books",
									url: "https://goto.applebooks.apple/9781668026038?at=10lIEQ",
								},
								{
									name: "Barnes and Noble",
									url: "https://www.anrdoezrs.net/click-7990613-11819508?url=https%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9781668026038",
								},
								{
									name: "Books-A-Million",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fp%252FICEBREAKER%252FHannah%252BGrace%252F9781668026038&url2=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fsearch%253Fquery%253DICEBREAKER%252BHannah%252BGrace",
								},
								{
									name: "Bookshop",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fbookshop.org%2Fa%2F3546%2F9781668026038&url2=https%3A%2F%2Fbookshop.org%2Fbooks%3Faffiliate%3D3546%26keywords%3DICEBREAKER",
								},
								{
									name: "IndieBound",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.indiebound.org%2Fbook%2F9781668026038%3Faff%3DNYT&url2=https%3A%2F%2Fwww.indiebound.org%2Fsearch%2Fbook%3Fkeys%3DICEBREAKER%2BHannah%2BGrace%26aff%3DNYT",
								},
							],
						},
						{
							age_group: "",
							amazon_product_url:
								"https://www.amazon.com/dp/1501161938?tag=NYTBSREV-20",
							article_chapter_link: "",
							author: "Taylor Jenkins Reid",
							book_image:
								"https://storage.googleapis.com/du-prd/books/images/9781501161933.jpg",
							book_image_width: 315,
							book_image_height: 500,
							book_review_link: "",
							contributor: "by Taylor Jenkins Reid",
							contributor_note: "",
							created_date: "2023-07-19 22:10:21",
							description:
								"A movie icon recounts stories of her loves and career to a struggling magazine writer.",
							first_chapter_link: "",
							price: "0.00",
							primary_isbn10: "1501161938",
							primary_isbn13: "9781501161933",
							book_uri:
								"nyt://book/d9511fac-ee44-5a87-9af7-2cd6a6f8f984",
							publisher: "Washington Square/Atria",
							rank: 5,
							rank_last_week: 6,
							sunday_review_link: "",
							title: "THE SEVEN HUSBANDS OF EVELYN HUGO",
							updated_date: "2023-07-19 22:14:36",
							weeks_on_list: 123,
							buy_links: [
								{
									name: "Amazon",
									url: "https://www.amazon.com/dp/1501161938?tag=NYTBSREV-20",
								},
								{
									name: "Apple Books",
									url: "https://goto.applebooks.apple/9781501161933?at=10lIEQ",
								},
								{
									name: "Barnes and Noble",
									url: "https://www.anrdoezrs.net/click-7990613-11819508?url=https%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9781501161933",
								},
								{
									name: "Books-A-Million",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fp%252FTHE%252BSEVEN%252BHUSBANDS%252BOF%252BEVELYN%252BHUGO%252FTaylor%252BJenkins%252BReid%252F9781501161933&url2=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fsearch%253Fquery%253DTHE%252BSEVEN%252BHUSBANDS%252BOF%252BEVELYN%252BHUGO%252BTaylor%252BJenkins%252BReid",
								},
								{
									name: "Bookshop",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fbookshop.org%2Fa%2F3546%2F9781501161933&url2=https%3A%2F%2Fbookshop.org%2Fbooks%3Faffiliate%3D3546%26keywords%3DTHE%2BSEVEN%2BHUSBANDS%2BOF%2BEVELYN%2BHUGO",
								},
								{
									name: "IndieBound",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.indiebound.org%2Fbook%2F9781501161933%3Faff%3DNYT&url2=https%3A%2F%2Fwww.indiebound.org%2Fsearch%2Fbook%3Fkeys%3DTHE%2BSEVEN%2BHUSBANDS%2BOF%2BEVELYN%2BHUGO%2BTaylor%2BJenkins%2BReid%26aff%3DNYT",
								},
							],
						},
					],
				},
				{
					list_id: 4,
					list_name: "Paperback Nonfiction",
					list_name_encoded: "paperback-nonfiction",
					display_name: "Paperback Nonfiction",
					updated: "WEEKLY",
					list_image: null,
					list_image_width: null,
					list_image_height: null,
					books: [
						{
							age_group: "",
							amazon_product_url:
								"https://www.amazon.com/Killers-Flower-Moon-Osage-Murders/dp/0385534248?tag=NYTBSREV-20",
							article_chapter_link: "",
							author: "David Grann",
							book_image:
								"https://storage.googleapis.com/du-prd/books/images/9780385534246.jpg",
							book_image_width: 326,
							book_image_height: 495,
							book_review_link:
								"https://www.nytimes.com/2017/04/28/books/review/killers-of-the-flower-moon-david-grann.html",
							contributor: "by David Grann",
							contributor_note: "",
							created_date: "2023-07-19 22:10:23",
							description:
								"The story of a murder spree in 1920s Oklahoma that targeted Osage Indians, whose lands contained oil. The fledgling F.B.I. intervened, ineffectively.",
							first_chapter_link: "",
							price: "0.00",
							primary_isbn10: "0307742482",
							primary_isbn13: "9780307742483",
							book_uri:
								"nyt://book/c5c1cd05-cdbc-5e7a-8255-1923ab4b1ceb",
							publisher: "Vintage",
							rank: 1,
							rank_last_week: 1,
							sunday_review_link: "",
							title: "KILLERS OF THE FLOWER MOON",
							updated_date: "2023-07-19 22:14:38",
							weeks_on_list: 124,
							buy_links: [
								{
									name: "Amazon",
									url: "https://www.amazon.com/Killers-Flower-Moon-Osage-Murders/dp/0385534248?tag=NYTBSREV-20",
								},
								{
									name: "Apple Books",
									url: "https://goto.applebooks.apple/9780307742483?at=10lIEQ",
								},
								{
									name: "Barnes and Noble",
									url: "https://www.anrdoezrs.net/click-7990613-11819508?url=https%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9780307742483",
								},
								{
									name: "Books-A-Million",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fp%252FKILLERS%252BOF%252BTHE%252BFLOWER%252BMOON%252FDavid%252BGrann%252F9780307742483&url2=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fsearch%253Fquery%253DKILLERS%252BOF%252BTHE%252BFLOWER%252BMOON%252BDavid%252BGrann",
								},
								{
									name: "Bookshop",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fbookshop.org%2Fa%2F3546%2F9780307742483&url2=https%3A%2F%2Fbookshop.org%2Fbooks%3Faffiliate%3D3546%26keywords%3DKILLERS%2BOF%2BTHE%2BFLOWER%2BMOON",
								},
								{
									name: "IndieBound",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.indiebound.org%2Fbook%2F9780307742483%3Faff%3DNYT&url2=https%3A%2F%2Fwww.indiebound.org%2Fsearch%2Fbook%3Fkeys%3DKILLERS%2BOF%2BTHE%2BFLOWER%2BMOON%2BDavid%2BGrann%26aff%3DNYT",
								},
							],
						},
						{
							age_group: "",
							amazon_product_url:
								"http://www.amazon.com/The-Body-Keeps-Score-Healing/dp/0670785938?tag=NYTBSREV-20",
							article_chapter_link: "",
							author: "Bessel van der Kolk",
							book_image:
								"https://storage.googleapis.com/du-prd/books/images/9780670785933.jpg",
							book_image_width: 128,
							book_image_height: 193,
							book_review_link:
								"https://www.nytimes.com/2018/10/18/books/review/how-to-rewire-your-traumatized-brain.html",
							contributor: "by Bessel van der Kolk",
							contributor_note: "",
							created_date: "2023-07-19 22:10:23",
							description:
								"How trauma affects the body and mind, and innovative treatments for recovery.",
							first_chapter_link: "",
							price: "0.00",
							primary_isbn10: "0143127748",
							primary_isbn13: "9780143127741",
							book_uri:
								"nyt://book/e3e33e9d-0e67-5fec-b0d2-2ecddc38ce0e",
							publisher: "Penguin",
							rank: 2,
							rank_last_week: 2,
							sunday_review_link: "",
							title: "THE BODY KEEPS THE SCORE",
							updated_date: "2023-07-19 22:14:38",
							weeks_on_list: 247,
							buy_links: [
								{
									name: "Amazon",
									url: "http://www.amazon.com/The-Body-Keeps-Score-Healing/dp/0670785938?tag=NYTBSREV-20",
								},
								{
									name: "Apple Books",
									url: "https://goto.applebooks.apple/9780143127741?at=10lIEQ",
								},
								{
									name: "Barnes and Noble",
									url: "https://www.anrdoezrs.net/click-7990613-11819508?url=https%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9780143127741",
								},
								{
									name: "Books-A-Million",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fp%252FTHE%252BBODY%252BKEEPS%252BTHE%252BSCORE%252FBessel%252Bvan%252Bder%252BKolk%252F9780143127741&url2=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fsearch%253Fquery%253DTHE%252BBODY%252BKEEPS%252BTHE%252BSCORE%252BBessel%252Bvan%252Bder%252BKolk",
								},
								{
									name: "Bookshop",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fbookshop.org%2Fa%2F3546%2F9780143127741&url2=https%3A%2F%2Fbookshop.org%2Fbooks%3Faffiliate%3D3546%26keywords%3DTHE%2BBODY%2BKEEPS%2BTHE%2BSCORE",
								},
								{
									name: "IndieBound",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.indiebound.org%2Fbook%2F9780143127741%3Faff%3DNYT&url2=https%3A%2F%2Fwww.indiebound.org%2Fsearch%2Fbook%3Fkeys%3DTHE%2BBODY%2BKEEPS%2BTHE%2BSCORE%2BBessel%2Bvan%2Bder%2BKolk%26aff%3DNYT",
								},
							],
						},
						{
							age_group: "",
							amazon_product_url:
								"https://www.amazon.com/dp/0375726268?tag=NYTBSREV-20",
							article_chapter_link: "",
							author: "Kai Bird and Martin J. Sherwin",
							book_image:
								"https://storage.googleapis.com/du-prd/books/images/9780375726262.jpg",
							book_image_width: 323,
							book_image_height: 500,
							book_review_link: "",
							contributor: "by Kai Bird and Martin J. Sherwin",
							contributor_note: "",
							created_date: "2023-07-19 22:10:23",
							description:
								"A biography of J. Robert Oppenheimer. Winner of the Pulitzer Prize in 2006 and an inspiration for the film “Oppenheimer.”",
							first_chapter_link: "",
							price: "0.00",
							primary_isbn10: "0375726268",
							primary_isbn13: "9780375726262",
							book_uri:
								"nyt://book/5a66cfd7-9fd6-5795-8a97-d06ad76ec3f5",
							publisher: "Vintage",
							rank: 3,
							rank_last_week: 7,
							sunday_review_link: "",
							title: "AMERICAN PROMETHEUS",
							updated_date: "2023-07-19 22:14:38",
							weeks_on_list: 3,
							buy_links: [
								{
									name: "Amazon",
									url: "https://www.amazon.com/dp/0375726268?tag=NYTBSREV-20",
								},
								{
									name: "Apple Books",
									url: "https://goto.applebooks.apple/9780375726262?at=10lIEQ",
								},
								{
									name: "Barnes and Noble",
									url: "https://www.anrdoezrs.net/click-7990613-11819508?url=https%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9780375726262",
								},
								{
									name: "Books-A-Million",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fp%252FAMERICAN%252BPROMETHEUS%252FKai%252BBird%252Band%252BMartin%252BJ.%252BSherwin%252F9780375726262&url2=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fsearch%253Fquery%253DAMERICAN%252BPROMETHEUS%252BKai%252BBird%252Band%252BMartin%252BJ.%252BSherwin",
								},
								{
									name: "Bookshop",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fbookshop.org%2Fa%2F3546%2F9780375726262&url2=https%3A%2F%2Fbookshop.org%2Fbooks%3Faffiliate%3D3546%26keywords%3DAMERICAN%2BPROMETHEUS",
								},
								{
									name: "IndieBound",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.indiebound.org%2Fbook%2F9780375726262%3Faff%3DNYT&url2=https%3A%2F%2Fwww.indiebound.org%2Fsearch%2Fbook%3Fkeys%3DAMERICAN%2BPROMETHEUS%2BKai%2BBird%2Band%2BMartin%2BJ.%2BSherwin%26aff%3DNYT",
								},
							],
						},
						{
							age_group: "",
							amazon_product_url:
								"https://www.amazon.com/dp/0060959479?tag=NYTBSREV-20",
							article_chapter_link: "",
							author: "bell hooks",
							book_image:
								"https://storage.googleapis.com/du-prd/books/images/9780060959470.jpg",
							book_image_width: 319,
							book_image_height: 500,
							book_review_link: "",
							contributor: "by bell hooks",
							contributor_note: "",
							created_date: "2023-07-19 22:10:23",
							description:
								"The late feminist icon explores the causes of a polarized society and the meaning of love.",
							first_chapter_link: "",
							price: "0.00",
							primary_isbn10: "0060959479",
							primary_isbn13: "9780060959470",
							book_uri:
								"nyt://book/84ce374b-f6b1-5f05-8556-b0e09e97679c",
							publisher: "Morrow",
							rank: 4,
							rank_last_week: 4,
							sunday_review_link:
								"https://www.nytimes.com/2000/01/30/books/that-4-letter-word.html",
							title: "ALL ABOUT LOVE",
							updated_date: "2023-07-19 22:14:38",
							weeks_on_list: 84,
							buy_links: [
								{
									name: "Amazon",
									url: "https://www.amazon.com/dp/0060959479?tag=NYTBSREV-20",
								},
								{
									name: "Apple Books",
									url: "https://goto.applebooks.apple/9780060959470?at=10lIEQ",
								},
								{
									name: "Barnes and Noble",
									url: "https://www.anrdoezrs.net/click-7990613-11819508?url=https%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9780060959470",
								},
								{
									name: "Books-A-Million",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fp%252FALL%252BABOUT%252BLOVE%252Fbell%252Bhooks%252F9780060959470&url2=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fsearch%253Fquery%253DALL%252BABOUT%252BLOVE%252Bbell%252Bhooks",
								},
								{
									name: "Bookshop",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fbookshop.org%2Fa%2F3546%2F9780060959470&url2=https%3A%2F%2Fbookshop.org%2Fbooks%3Faffiliate%3D3546%26keywords%3DALL%2BABOUT%2BLOVE",
								},
								{
									name: "IndieBound",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.indiebound.org%2Fbook%2F9780060959470%3Faff%3DNYT&url2=https%3A%2F%2Fwww.indiebound.org%2Fsearch%2Fbook%3Fkeys%3DALL%2BABOUT%2BLOVE%2Bbell%2Bhooks%26aff%3DNYT",
								},
							],
						},
						{
							age_group: "",
							amazon_product_url:
								"https://www.amazon.com/dp/0525657746?tag=NYTBSREV-20",
							article_chapter_link: "",
							author: "Michelle Zauner",
							book_image:
								"https://storage.googleapis.com/du-prd/books/images/9780525657743.jpg",
							book_image_width: 338,
							book_image_height: 500,
							book_review_link: "",
							contributor: "by Michelle Zauner",
							contributor_note: "",
							created_date: "2023-07-19 22:10:23",
							description:
								"The daughter of a Korean mother and Jewish American father, and leader of the indie rock project Japanese Breakfast, describes creating her own identity after losing her mother to cancer.",
							first_chapter_link: "",
							price: "0.00",
							primary_isbn10: "1984898957",
							primary_isbn13: "9781984898951",
							book_uri:
								"nyt://book/520cb070-10ce-5d43-9b0c-e77f57d84a8d",
							publisher: "Vintage",
							rank: 5,
							rank_last_week: 3,
							sunday_review_link: "",
							title: "CRYING IN H MART",
							updated_date: "2023-07-19 22:14:38",
							weeks_on_list: 16,
							buy_links: [
								{
									name: "Amazon",
									url: "https://www.amazon.com/dp/0525657746?tag=NYTBSREV-20",
								},
								{
									name: "Apple Books",
									url: "https://goto.applebooks.apple/9781984898951?at=10lIEQ",
								},
								{
									name: "Barnes and Noble",
									url: "https://www.anrdoezrs.net/click-7990613-11819508?url=https%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9781984898951",
								},
								{
									name: "Books-A-Million",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fp%252FCRYING%252BIN%252BH%252BMART%252FMichelle%252BZauner%252F9781984898951&url2=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fsearch%253Fquery%253DCRYING%252BIN%252BH%252BMART%252BMichelle%252BZauner",
								},
								{
									name: "Bookshop",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fbookshop.org%2Fa%2F3546%2F9781984898951&url2=https%3A%2F%2Fbookshop.org%2Fbooks%3Faffiliate%3D3546%26keywords%3DCRYING%2BIN%2BH%2BMART",
								},
								{
									name: "IndieBound",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.indiebound.org%2Fbook%2F9781984898951%3Faff%3DNYT&url2=https%3A%2F%2Fwww.indiebound.org%2Fsearch%2Fbook%3Fkeys%3DCRYING%2BIN%2BH%2BMART%2BMichelle%2BZauner%26aff%3DNYT",
								},
							],
						},
					],
				},
				{
					list_id: 24,
					list_name: "Advice How-To and Miscellaneous",
					list_name_encoded: "advice-how-to-and-miscellaneous",
					display_name: "Advice, How-To & Miscellaneous",
					updated: "WEEKLY",
					list_image: null,
					list_image_width: null,
					list_image_height: null,
					books: [
						{
							age_group: "",
							amazon_product_url:
								"https://www.amazon.com/Atomic-Habits-Proven-Build-Break/dp/0735211299?tag=NYTBSREV-20",
							article_chapter_link: "",
							author: "James Clear",
							book_image:
								"https://storage.googleapis.com/du-prd/books/images/9780735211292.jpg",
							book_image_width: 328,
							book_image_height: 495,
							book_review_link: "",
							contributor: "by James Clear",
							contributor_note: "",
							created_date: "2023-07-19 22:10:22",
							description: "",
							first_chapter_link: "",
							price: "0.00",
							primary_isbn10: "0735211299",
							primary_isbn13: "9780735211292",
							book_uri:
								"nyt://book/0398a355-c032-534e-a0af-647b06f0840d",
							publisher: "Avery",
							rank: 1,
							rank_last_week: 1,
							sunday_review_link: "",
							title: "ATOMIC HABITS",
							updated_date: "2023-07-19 22:14:38",
							weeks_on_list: 190,
							buy_links: [
								{
									name: "Amazon",
									url: "https://www.amazon.com/Atomic-Habits-Proven-Build-Break/dp/0735211299?tag=NYTBSREV-20",
								},
								{
									name: "Apple Books",
									url: "https://goto.applebooks.apple/9780735211292?at=10lIEQ",
								},
								{
									name: "Barnes and Noble",
									url: "https://www.anrdoezrs.net/click-7990613-11819508?url=https%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9780735211292",
								},
								{
									name: "Books-A-Million",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fp%252FATOMIC%252BHABITS%252FJames%252BClear%252F9780735211292&url2=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fsearch%253Fquery%253DATOMIC%252BHABITS%252BJames%252BClear",
								},
								{
									name: "Bookshop",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fbookshop.org%2Fa%2F3546%2F9780735211292&url2=https%3A%2F%2Fbookshop.org%2Fbooks%3Faffiliate%3D3546%26keywords%3DATOMIC%2BHABITS",
								},
								{
									name: "IndieBound",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.indiebound.org%2Fbook%2F9780735211292%3Faff%3DNYT&url2=https%3A%2F%2Fwww.indiebound.org%2Fsearch%2Fbook%3Fkeys%3DATOMIC%2BHABITS%2BJames%2BClear%26aff%3DNYT",
								},
							],
						},
						{
							age_group: "",
							amazon_product_url:
								"https://www.amazon.com/dp/0593652886?tag=NYTBSREV-20",
							article_chapter_link: "",
							author: "Rick Rubin with Neil Strauss",
							book_image:
								"https://storage.googleapis.com/du-prd/books/images/9780593652886.jpg",
							book_image_width: 341,
							book_image_height: 500,
							book_review_link: "",
							contributor: "by Rick Rubin with Neil Strauss",
							contributor_note: "",
							created_date: "2023-07-19 22:10:22",
							description: "",
							first_chapter_link: "",
							price: "0.00",
							primary_isbn10: "0593652886",
							primary_isbn13: "9780593652886",
							book_uri:
								"nyt://book/da28ca16-1ebd-55aa-aba3-fde278604a82",
							publisher: "Penguin Press",
							rank: 2,
							rank_last_week: 2,
							sunday_review_link: "",
							title: "THE CREATIVE ACT",
							updated_date: "2023-07-19 22:14:38",
							weeks_on_list: 26,
							buy_links: [
								{
									name: "Amazon",
									url: "https://www.amazon.com/dp/0593652886?tag=NYTBSREV-20",
								},
								{
									name: "Apple Books",
									url: "https://goto.applebooks.apple/9780593652886?at=10lIEQ",
								},
								{
									name: "Barnes and Noble",
									url: "https://www.anrdoezrs.net/click-7990613-11819508?url=https%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9780593652886",
								},
								{
									name: "Books-A-Million",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fp%252FTHE%252BCREATIVE%252BACT%252FRick%252BRubin%252Bwith%252BNeil%252BStrauss%252F9780593652886&url2=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fsearch%253Fquery%253DTHE%252BCREATIVE%252BACT%252BRick%252BRubin%252Bwith%252BNeil%252BStrauss",
								},
								{
									name: "Bookshop",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fbookshop.org%2Fa%2F3546%2F9780593652886&url2=https%3A%2F%2Fbookshop.org%2Fbooks%3Faffiliate%3D3546%26keywords%3DTHE%2BCREATIVE%2BACT",
								},
								{
									name: "IndieBound",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.indiebound.org%2Fbook%2F9780593652886%3Faff%3DNYT&url2=https%3A%2F%2Fwww.indiebound.org%2Fsearch%2Fbook%3Fkeys%3DTHE%2BCREATIVE%2BACT%2BRick%2BRubin%2Bwith%2BNeil%2BStrauss%26aff%3DNYT",
								},
							],
						},
						{
							age_group: "",
							amazon_product_url:
								"http://www.amazon.com/Subtle-Art-Not-Giving-Counterintuitive/dp/0062457713?tag=NYTBSREV-20",
							article_chapter_link: "",
							author: "Mark Manson",
							book_image:
								"https://storage.googleapis.com/du-prd/books/images/9780062457714.jpg",
							book_image_width: 330,
							book_image_height: 495,
							book_review_link: "",
							contributor: "by Mark Manson",
							contributor_note: "",
							created_date: "2023-07-19 22:10:22",
							description: "",
							first_chapter_link: "",
							price: "0.00",
							primary_isbn10: "0062457713",
							primary_isbn13: "9780062457714",
							book_uri:
								"nyt://book/61980bbe-79bd-5824-9412-31c5f07762fd",
							publisher: "Harper",
							rank: 3,
							rank_last_week: 3,
							sunday_review_link: "",
							title: "THE SUBTLE ART OF NOT GIVING A F*CK",
							updated_date: "2023-07-19 22:14:38",
							weeks_on_list: 302,
							buy_links: [
								{
									name: "Amazon",
									url: "http://www.amazon.com/Subtle-Art-Not-Giving-Counterintuitive/dp/0062457713?tag=NYTBSREV-20",
								},
								{
									name: "Apple Books",
									url: "https://goto.applebooks.apple/9780062457714?at=10lIEQ",
								},
								{
									name: "Barnes and Noble",
									url: "https://www.anrdoezrs.net/click-7990613-11819508?url=https%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9780062457714",
								},
								{
									name: "Books-A-Million",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fp%252FTHE%252BSUBTLE%252BART%252BOF%252BNOT%252BGIVING%252BA%252BF%25252ACK%252FMark%252BManson%252F9780062457714&url2=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fsearch%253Fquery%253DTHE%252BSUBTLE%252BART%252BOF%252BNOT%252BGIVING%252BA%252BF%25252ACK%252BMark%252BManson",
								},
								{
									name: "Bookshop",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fbookshop.org%2Fa%2F3546%2F9780062457714&url2=https%3A%2F%2Fbookshop.org%2Fbooks%3Faffiliate%3D3546%26keywords%3DTHE%2BSUBTLE%2BART%2BOF%2BNOT%2BGIVING%2BA%2BF%252ACK",
								},
								{
									name: "IndieBound",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.indiebound.org%2Fbook%2F9780062457714%3Faff%3DNYT&url2=https%3A%2F%2Fwww.indiebound.org%2Fsearch%2Fbook%3Fkeys%3DTHE%2BSUBTLE%2BART%2BOF%2BNOT%2BGIVING%2BA%2BF%252ACK%2BMark%2BManson%26aff%3DNYT",
								},
							],
						},
						{
							age_group: "",
							amazon_product_url:
								"https://www.amazon.com/Cant-Hurt-Me-Master-Your/dp/1544512287?tag=NYTBSREV-20",
							article_chapter_link: "",
							author: "David Goggins",
							book_image:
								"https://storage.googleapis.com/du-prd/books/images/9781544512280.jpg",
							book_image_width: 321,
							book_image_height: 495,
							book_review_link: "",
							contributor: "by David Goggins",
							contributor_note: "",
							created_date: "2023-07-19 22:10:22",
							description: "",
							first_chapter_link: "",
							price: "0.00",
							primary_isbn10: "1544512279",
							primary_isbn13: "9781544512273",
							book_uri:
								"nyt://book/cad7f15b-73d9-5097-89f3-bc2ea602617e",
							publisher: "Lioncrest",
							rank: 4,
							rank_last_week: 4,
							sunday_review_link: "",
							title: "CAN'T HURT ME",
							updated_date: "2023-07-19 22:14:38",
							weeks_on_list: 42,
							buy_links: [
								{
									name: "Amazon",
									url: "https://www.amazon.com/Cant-Hurt-Me-Master-Your/dp/1544512287?tag=NYTBSREV-20",
								},
								{
									name: "Apple Books",
									url: "https://goto.applebooks.apple/9781544512273?at=10lIEQ",
								},
								{
									name: "Barnes and Noble",
									url: "https://www.anrdoezrs.net/click-7990613-11819508?url=https%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9781544512273",
								},
								{
									name: "Books-A-Million",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fp%252FCAN%252527T%252BHURT%252BME%252FDavid%252BGoggins%252F9781544512273&url2=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fsearch%253Fquery%253DCAN%252527T%252BHURT%252BME%252BDavid%252BGoggins",
								},
								{
									name: "Bookshop",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fbookshop.org%2Fa%2F3546%2F9781544512273&url2=https%3A%2F%2Fbookshop.org%2Fbooks%3Faffiliate%3D3546%26keywords%3DCAN%2527T%2BHURT%2BME",
								},
								{
									name: "IndieBound",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.indiebound.org%2Fbook%2F9781544512273%3Faff%3DNYT&url2=https%3A%2F%2Fwww.indiebound.org%2Fsearch%2Fbook%3Fkeys%3DCAN%2527T%2BHURT%2BME%2BDavid%2BGoggins%26aff%3DNYT",
								},
							],
						},
						{
							age_group: "",
							amazon_product_url:
								"https://www.amazon.com/Will-Teach-You-Rich-Second-ebook/dp/B07GNXPP4P?tag=NYTBSREV-20",
							article_chapter_link: "",
							author: "Ramit Sethi",
							book_image:
								"https://storage.googleapis.com/du-prd/books/images/9781523507870.jpg",
							book_image_width: 329,
							book_image_height: 495,
							book_review_link: "",
							contributor: "by Ramit Sethi",
							contributor_note: "",
							created_date: "2023-07-19 22:10:22",
							description: "",
							first_chapter_link: "",
							price: "0.00",
							primary_isbn10: "1523505745",
							primary_isbn13: "9781523505746",
							book_uri:
								"nyt://book/b0fe54f6-2468-55d3-bedf-0338f98478e6",
							publisher: "Workman",
							rank: 5,
							rank_last_week: 8,
							sunday_review_link: "",
							title: "I WILL TEACH YOU TO BE RICH, SECOND EDITION",
							updated_date: "2023-07-19 22:14:38",
							weeks_on_list: 11,
							buy_links: [
								{
									name: "Amazon",
									url: "https://www.amazon.com/Will-Teach-You-Rich-Second-ebook/dp/B07GNXPP4P?tag=NYTBSREV-20",
								},
								{
									name: "Apple Books",
									url: "https://goto.applebooks.apple/9781523505746?at=10lIEQ",
								},
								{
									name: "Barnes and Noble",
									url: "https://www.anrdoezrs.net/click-7990613-11819508?url=https%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9781523505746",
								},
								{
									name: "Books-A-Million",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fp%252FI%252BWILL%252BTEACH%252BYOU%252BTO%252BBE%252BRICH%25252C%252BSECOND%252BEDITION%252FRamit%252BSethi%252F9781523505746&url2=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fsearch%253Fquery%253DI%252BWILL%252BTEACH%252BYOU%252BTO%252BBE%252BRICH%25252C%252BSECOND%252BEDITION%252BRamit%252BSethi",
								},
								{
									name: "Bookshop",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fbookshop.org%2Fa%2F3546%2F9781523505746&url2=https%3A%2F%2Fbookshop.org%2Fbooks%3Faffiliate%3D3546%26keywords%3DI%2BWILL%2BTEACH%2BYOU%2BTO%2BBE%2BRICH%252C%2BSECOND%2BEDITION",
								},
								{
									name: "IndieBound",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.indiebound.org%2Fbook%2F9781523505746%3Faff%3DNYT&url2=https%3A%2F%2Fwww.indiebound.org%2Fsearch%2Fbook%3Fkeys%3DI%2BWILL%2BTEACH%2BYOU%2BTO%2BBE%2BRICH%252C%2BSECOND%2BEDITION%2BRamit%2BSethi%26aff%3DNYT",
								},
							],
						},
					],
				},
				{
					list_id: 13,
					list_name: "Childrens Middle Grade Hardcover",
					list_name_encoded: "childrens-middle-grade-hardcover",
					display_name: "Children’s Middle Grade Hardcover",
					updated: "WEEKLY",
					list_image: null,
					list_image_width: null,
					list_image_height: null,
					books: [
						{
							age_group: "",
							amazon_product_url:
								"https://www.amazon.com/dp/1368081150?tag=NYTBSREV-20",
							article_chapter_link: "",
							author: "Rick Riordan and Mark Oshiro",
							book_image:
								"https://storage.googleapis.com/du-prd/books/images/9781368081153.jpg",
							book_image_width: 331,
							book_image_height: 500,
							book_review_link: "",
							contributor: "by Rick Riordan and Mark Oshiro",
							contributor_note: "",
							created_date: "2023-07-19 22:10:20",
							description:
								"The demigods Will and Nico embark on a dangerous journey to the Underworld to rescue an old friend.",
							first_chapter_link: "",
							price: "0.00",
							primary_isbn10: "1368081150",
							primary_isbn13: "9781368081153",
							book_uri:
								"nyt://book/8384c64a-d0de-5285-9132-8f16cc7b085f",
							publisher: "Disney Hyperion",
							rank: 1,
							rank_last_week: 1,
							sunday_review_link: "",
							title: "THE SUN AND THE STAR",
							updated_date: "2023-07-19 22:14:35",
							weeks_on_list: 11,
							buy_links: [
								{
									name: "Amazon",
									url: "https://www.amazon.com/dp/1368081150?tag=NYTBSREV-20",
								},
								{
									name: "Apple Books",
									url: "https://goto.applebooks.apple/9781368081153?at=10lIEQ",
								},
								{
									name: "Barnes and Noble",
									url: "https://www.anrdoezrs.net/click-7990613-11819508?url=https%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9781368081153",
								},
								{
									name: "Books-A-Million",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fp%252FTHE%252BSUN%252BAND%252BTHE%252BSTAR%252FRick%252BRiordan%252Band%252BMark%252BOshiro%252F9781368081153&url2=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fsearch%253Fquery%253DTHE%252BSUN%252BAND%252BTHE%252BSTAR%252BRick%252BRiordan%252Band%252BMark%252BOshiro",
								},
								{
									name: "Bookshop",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fbookshop.org%2Fa%2F3546%2F9781368081153&url2=https%3A%2F%2Fbookshop.org%2Fbooks%3Faffiliate%3D3546%26keywords%3DTHE%2BSUN%2BAND%2BTHE%2BSTAR",
								},
								{
									name: "IndieBound",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.indiebound.org%2Fbook%2F9781368081153%3Faff%3DNYT&url2=https%3A%2F%2Fwww.indiebound.org%2Fsearch%2Fbook%3Fkeys%3DTHE%2BSUN%2BAND%2BTHE%2BSTAR%2BRick%2BRiordan%2Band%2BMark%2BOshiro%26aff%3DNYT",
								},
							],
						},
						{
							age_group: "",
							amazon_product_url:
								"https://www.amazon.com/Refugee-Alan-Gratz/dp/0545880831?tag=NYTBSREV-20",
							article_chapter_link: "",
							author: "Alan Gratz",
							book_image:
								"https://storage.googleapis.com/du-prd/books/images/9780545880831.jpg",
							book_image_width: 327,
							book_image_height: 495,
							book_review_link: "",
							contributor: "by Alan Gratz",
							contributor_note: "",
							created_date: "2023-07-19 22:10:20",
							description:
								"Three children in three different conflicts look for safe haven.",
							first_chapter_link: "",
							price: "0.00",
							primary_isbn10: "0545880831",
							primary_isbn13: "9780545880831",
							book_uri:
								"nyt://book/32519cc3-8ee6-5bc8-9a1a-563502a5d2ad",
							publisher: "Scholastic",
							rank: 2,
							rank_last_week: 2,
							sunday_review_link: "",
							title: "REFUGEE",
							updated_date: "2023-07-19 22:14:35",
							weeks_on_list: 229,
							buy_links: [
								{
									name: "Amazon",
									url: "https://www.amazon.com/Refugee-Alan-Gratz/dp/0545880831?tag=NYTBSREV-20",
								},
								{
									name: "Apple Books",
									url: "https://goto.applebooks.apple/9780545880831?at=10lIEQ",
								},
								{
									name: "Barnes and Noble",
									url: "https://www.anrdoezrs.net/click-7990613-11819508?url=https%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9780545880831",
								},
								{
									name: "Books-A-Million",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fp%252FREFUGEE%252FAlan%252BGratz%252F9780545880831&url2=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fsearch%253Fquery%253DREFUGEE%252BAlan%252BGratz",
								},
								{
									name: "Bookshop",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fbookshop.org%2Fa%2F3546%2F9780545880831&url2=https%3A%2F%2Fbookshop.org%2Fbooks%3Faffiliate%3D3546%26keywords%3DREFUGEE",
								},
								{
									name: "IndieBound",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.indiebound.org%2Fbook%2F9780545880831%3Faff%3DNYT&url2=https%3A%2F%2Fwww.indiebound.org%2Fsearch%2Fbook%3Fkeys%3DREFUGEE%2BAlan%2BGratz%26aff%3DNYT",
								},
							],
						},
						{
							age_group: "",
							amazon_product_url:
								"http://www.amazon.com/Wonder-R-J-Palacio-ebook/dp/B0051ANPZQ?tag=NYTBSREV-20",
							article_chapter_link: "",
							author: "R.J. Palacio",
							book_image:
								"https://storage.googleapis.com/du-prd/books/images/9780375899881.jpg",
							book_image_width: 329,
							book_image_height: 495,
							book_review_link: "",
							contributor: "by R.J. Palacio",
							contributor_note: "",
							created_date: "2023-07-19 22:10:20",
							description:
								"A boy with a facial deformity starts school.",
							first_chapter_link: "",
							price: "0.00",
							primary_isbn10: "0375869026",
							primary_isbn13: "9780375869020",
							book_uri:
								"nyt://book/ae6bd2cf-a5d7-535a-99dd-ca8e283c2b01",
							publisher: "Knopf",
							rank: 3,
							rank_last_week: 3,
							sunday_review_link:
								"https://www.nytimes.com/2012/04/08/books/review/wonder-by-r-j-palacio.html",
							title: "WONDER",
							updated_date: "2023-07-19 22:14:35",
							weeks_on_list: 405,
							buy_links: [
								{
									name: "Amazon",
									url: "http://www.amazon.com/Wonder-R-J-Palacio-ebook/dp/B0051ANPZQ?tag=NYTBSREV-20",
								},
								{
									name: "Apple Books",
									url: "https://goto.applebooks.apple/9780375869020?at=10lIEQ",
								},
								{
									name: "Barnes and Noble",
									url: "https://www.anrdoezrs.net/click-7990613-11819508?url=https%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9780375869020",
								},
								{
									name: "Books-A-Million",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fp%252FWONDER%252FR.J.%252BPalacio%252F9780375869020&url2=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fsearch%253Fquery%253DWONDER%252BR.J.%252BPalacio",
								},
								{
									name: "Bookshop",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fbookshop.org%2Fa%2F3546%2F9780375869020&url2=https%3A%2F%2Fbookshop.org%2Fbooks%3Faffiliate%3D3546%26keywords%3DWONDER",
								},
								{
									name: "IndieBound",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.indiebound.org%2Fbook%2F9780375869020%3Faff%3DNYT&url2=https%3A%2F%2Fwww.indiebound.org%2Fsearch%2Fbook%3Fkeys%3DWONDER%2BR.J.%2BPalacio%26aff%3DNYT",
								},
							],
						},
						{
							age_group: "",
							amazon_product_url:
								"https://www.amazon.com/dp/1665925280?tag=NYTBSREV-20",
							article_chapter_link: "",
							author: "Wanda Coven.",
							book_image:
								"https://storage.googleapis.com/du-prd/books/images/9781665925280.jpg",
							book_image_width: 346,
							book_image_height: 500,
							book_review_link: "",
							contributor:
								"by Wanda Coven. Illustrated by Anna Abramskaya",
							contributor_note: "Illustrated by Anna Abramskaya",
							created_date: "2023-07-19 22:10:20",
							description:
								"Old rivals, Heidi and Melanie find out they're roommates at the Broomsfield Academy.",
							first_chapter_link: "",
							price: "0.00",
							primary_isbn10: "1665925280",
							primary_isbn13: "9781665925280",
							book_uri:
								"nyt://book/aa5397d3-ccd0-5f9c-b6d3-032823aaabb6",
							publisher: "Simon Spotlight",
							rank: 4,
							rank_last_week: 6,
							sunday_review_link: "",
							title: "WORST BROOMMATE EVER!",
							updated_date: "2023-07-19 22:14:35",
							weeks_on_list: 6,
							buy_links: [
								{
									name: "Amazon",
									url: "https://www.amazon.com/dp/1665925280?tag=NYTBSREV-20",
								},
								{
									name: "Apple Books",
									url: "https://goto.applebooks.apple/9781665925280?at=10lIEQ",
								},
								{
									name: "Barnes and Noble",
									url: "https://www.anrdoezrs.net/click-7990613-11819508?url=https%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9781665925280",
								},
								{
									name: "Books-A-Million",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fp%252FWORST%252BBROOMMATE%252BEVER%252521%252FWanda%252BCoven.%252F9781665925280&url2=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fsearch%253Fquery%253DWORST%252BBROOMMATE%252BEVER%252521%252BWanda%252BCoven.",
								},
								{
									name: "Bookshop",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fbookshop.org%2Fa%2F3546%2F9781665925280&url2=https%3A%2F%2Fbookshop.org%2Fbooks%3Faffiliate%3D3546%26keywords%3DWORST%2BBROOMMATE%2BEVER%2521",
								},
								{
									name: "IndieBound",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.indiebound.org%2Fbook%2F9781665925280%3Faff%3DNYT&url2=https%3A%2F%2Fwww.indiebound.org%2Fsearch%2Fbook%3Fkeys%3DWORST%2BBROOMMATE%2BEVER%2521%2BWanda%2BCoven.%26aff%3DNYT",
								},
							],
						},
						{
							age_group: "",
							amazon_product_url:
								"https://www.amazon.com/dp/0593533232?tag=NYTBSREV-20",
							article_chapter_link: "",
							author: "Beth Lincoln.",
							book_image:
								"https://storage.googleapis.com/du-prd/books/images/9780593533239.jpg",
							book_image_width: 331,
							book_image_height: 500,
							book_review_link: "",
							contributor:
								"by Beth Lincoln. Illustrated by Claire Powell",
							contributor_note: "Illustrated by Claire Powell",
							created_date: "2023-07-19 22:10:20",
							description:
								"Shenanigan Swift investigates murders during a family reunion treasure hunt.",
							first_chapter_link: "",
							price: "0.00",
							primary_isbn10: "0593533232",
							primary_isbn13: "9780593533239",
							book_uri:
								"nyt://book/43f173db-0104-5673-aa7c-23dc6a71472d",
							publisher: "Dutton",
							rank: 5,
							rank_last_week: 7,
							sunday_review_link: "",
							title: "THE SWIFTS",
							updated_date: "2023-07-19 22:14:35",
							weeks_on_list: 17,
							buy_links: [
								{
									name: "Amazon",
									url: "https://www.amazon.com/dp/0593533232?tag=NYTBSREV-20",
								},
								{
									name: "Apple Books",
									url: "https://goto.applebooks.apple/9780593533239?at=10lIEQ",
								},
								{
									name: "Barnes and Noble",
									url: "https://www.anrdoezrs.net/click-7990613-11819508?url=https%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9780593533239",
								},
								{
									name: "Books-A-Million",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fp%252FTHE%252BSWIFTS%252FBeth%252BLincoln.%252F9780593533239&url2=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fsearch%253Fquery%253DTHE%252BSWIFTS%252BBeth%252BLincoln.",
								},
								{
									name: "Bookshop",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fbookshop.org%2Fa%2F3546%2F9780593533239&url2=https%3A%2F%2Fbookshop.org%2Fbooks%3Faffiliate%3D3546%26keywords%3DTHE%2BSWIFTS",
								},
								{
									name: "IndieBound",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.indiebound.org%2Fbook%2F9780593533239%3Faff%3DNYT&url2=https%3A%2F%2Fwww.indiebound.org%2Fsearch%2Fbook%3Fkeys%3DTHE%2BSWIFTS%2BBeth%2BLincoln.%26aff%3DNYT",
								},
							],
						},
					],
				},
				{
					list_id: 7,
					list_name: "Picture Books",
					list_name_encoded: "picture-books",
					display_name: "Children’s Picture Books",
					updated: "WEEKLY",
					list_image: null,
					list_image_width: null,
					list_image_height: null,
					books: [
						{
							age_group: "",
							amazon_product_url:
								"https://www.amazon.com/dp/1536223360?tag=NYTBSREV-20",
							article_chapter_link: "",
							author: "Jon Klassen",
							book_image:
								"https://storage.googleapis.com/du-prd/books/images/9781536223361.jpg",
							book_image_width: 378,
							book_image_height: 500,
							book_review_link: "",
							contributor: "by Jon Klassen",
							contributor_note: "",
							created_date: "2023-07-19 22:10:23",
							description:
								"After running away from home, Otilla discovers an old mansion whose only occupant is a talking skull.",
							first_chapter_link: "",
							price: "0.00",
							primary_isbn10: "1536223360",
							primary_isbn13: "9781536223361",
							book_uri:
								"nyt://book/7d47f9aa-5073-5ef8-bd6a-55d453f07d17",
							publisher: "Candlewick",
							rank: 1,
							rank_last_week: 0,
							sunday_review_link: "",
							title: "THE SKULL",
							updated_date: "2023-07-19 22:14:39",
							weeks_on_list: 1,
							buy_links: [
								{
									name: "Amazon",
									url: "https://www.amazon.com/dp/1536223360?tag=NYTBSREV-20",
								},
								{
									name: "Apple Books",
									url: "https://goto.applebooks.apple/9781536223361?at=10lIEQ",
								},
								{
									name: "Barnes and Noble",
									url: "https://www.anrdoezrs.net/click-7990613-11819508?url=https%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9781536223361",
								},
								{
									name: "Books-A-Million",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fp%252FTHE%252BSKULL%252FJon%252BKlassen%252F9781536223361&url2=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fsearch%253Fquery%253DTHE%252BSKULL%252BJon%252BKlassen",
								},
								{
									name: "Bookshop",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fbookshop.org%2Fa%2F3546%2F9781536223361&url2=https%3A%2F%2Fbookshop.org%2Fbooks%3Faffiliate%3D3546%26keywords%3DTHE%2BSKULL",
								},
								{
									name: "IndieBound",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.indiebound.org%2Fbook%2F9781536223361%3Faff%3DNYT&url2=https%3A%2F%2Fwww.indiebound.org%2Fsearch%2Fbook%3Fkeys%3DTHE%2BSKULL%2BJon%2BKlassen%26aff%3DNYT",
								},
							],
						},
						{
							age_group: "",
							amazon_product_url:
								"http://www.amazon.com/The-Crayons-Quit-Drew-Daywalt/dp/0399255370?tag=NYTBSREV-20",
							article_chapter_link: "",
							author: "Drew Daywalt.",
							book_image:
								"https://storage.googleapis.com/du-prd/books/images/9780399255373.jpg",
							book_image_width: 330,
							book_image_height: 332,
							book_review_link: "",
							contributor:
								"by Drew Daywalt. Illustrated by Oliver Jeffers",
							contributor_note: "Illustrated by Oliver Jeffers",
							created_date: "2023-07-19 22:10:23",
							description:
								"Problems arise when Duncan’s crayons revolt.",
							first_chapter_link: "",
							price: "0.00",
							primary_isbn10: "0399255370",
							primary_isbn13: "9780399255373",
							book_uri:
								"nyt://book/e42bd6ff-8143-53b3-b574-c80553973559",
							publisher: "Philomel",
							rank: 2,
							rank_last_week: 5,
							sunday_review_link:
								"https://www.nytimes.com/2013/08/25/books/review/henris-scissors-by-jeanette-winter-and-more.html",
							title: "THE DAY THE CRAYONS QUIT",
							updated_date: "2023-07-19 22:14:39",
							weeks_on_list: 369,
							buy_links: [
								{
									name: "Amazon",
									url: "http://www.amazon.com/The-Crayons-Quit-Drew-Daywalt/dp/0399255370?tag=NYTBSREV-20",
								},
								{
									name: "Apple Books",
									url: "https://goto.applebooks.apple/9780399255373?at=10lIEQ",
								},
								{
									name: "Barnes and Noble",
									url: "https://www.anrdoezrs.net/click-7990613-11819508?url=https%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9780399255373",
								},
								{
									name: "Books-A-Million",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fp%252FTHE%252BDAY%252BTHE%252BCRAYONS%252BQUIT%252FDrew%252BDaywalt.%252F9780399255373&url2=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fsearch%253Fquery%253DTHE%252BDAY%252BTHE%252BCRAYONS%252BQUIT%252BDrew%252BDaywalt.",
								},
								{
									name: "Bookshop",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fbookshop.org%2Fa%2F3546%2F9780399255373&url2=https%3A%2F%2Fbookshop.org%2Fbooks%3Faffiliate%3D3546%26keywords%3DTHE%2BDAY%2BTHE%2BCRAYONS%2BQUIT",
								},
								{
									name: "IndieBound",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.indiebound.org%2Fbook%2F9780399255373%3Faff%3DNYT&url2=https%3A%2F%2Fwww.indiebound.org%2Fsearch%2Fbook%3Fkeys%3DTHE%2BDAY%2BTHE%2BCRAYONS%2BQUIT%2BDrew%2BDaywalt.%26aff%3DNYT",
								},
							],
						},
						{
							age_group: "",
							amazon_product_url:
								"http://www.amazon.com/Dragons-Love-Tacos-Adam-Rubin/dp/0803736800?tag=NYTBSREV-20",
							article_chapter_link: "",
							author: "Adam Rubin.",
							book_image:
								"https://storage.googleapis.com/du-prd/books/images/9780803736801.jpg",
							book_image_width: 330,
							book_image_height: 330,
							book_review_link: "",
							contributor:
								"by Adam Rubin. Illustrated by Daniel Salmieri",
							contributor_note: "Illustrated by Daniel Salmieri",
							created_date: "2023-07-19 22:10:24",
							description: "What to serve your dragon-guests.",
							first_chapter_link: "",
							price: "0.00",
							primary_isbn10: "0803736800",
							primary_isbn13: "9780803736801",
							book_uri:
								"nyt://book/25d4f970-1f30-515b-a88c-691b4854bc63",
							publisher: "Dial",
							rank: 3,
							rank_last_week: 4,
							sunday_review_link: "",
							title: "DRAGONS LOVE TACOS",
							updated_date: "2023-07-19 22:14:39",
							weeks_on_list: 407,
							buy_links: [
								{
									name: "Amazon",
									url: "http://www.amazon.com/Dragons-Love-Tacos-Adam-Rubin/dp/0803736800?tag=NYTBSREV-20",
								},
								{
									name: "Apple Books",
									url: "https://goto.applebooks.apple/9780803736801?at=10lIEQ",
								},
								{
									name: "Barnes and Noble",
									url: "https://www.anrdoezrs.net/click-7990613-11819508?url=https%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9780803736801",
								},
								{
									name: "Books-A-Million",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fp%252FDRAGONS%252BLOVE%252BTACOS%252FAdam%252BRubin.%252F9780803736801&url2=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fsearch%253Fquery%253DDRAGONS%252BLOVE%252BTACOS%252BAdam%252BRubin.",
								},
								{
									name: "Bookshop",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fbookshop.org%2Fa%2F3546%2F9780803736801&url2=https%3A%2F%2Fbookshop.org%2Fbooks%3Faffiliate%3D3546%26keywords%3DDRAGONS%2BLOVE%2BTACOS",
								},
								{
									name: "IndieBound",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.indiebound.org%2Fbook%2F9780803736801%3Faff%3DNYT&url2=https%3A%2F%2Fwww.indiebound.org%2Fsearch%2Fbook%3Fkeys%3DDRAGONS%2BLOVE%2BTACOS%2BAdam%2BRubin.%26aff%3DNYT",
								},
							],
						},
						{
							age_group: "",
							amazon_product_url:
								"https://www.amazon.com/dp/0593621115?tag=NYTBSREV-20",
							article_chapter_link: "",
							author: "Drew Daywalt.",
							book_image:
								"https://storage.googleapis.com/du-prd/books/images/9780593621110.jpg",
							book_image_width: 411,
							book_image_height: 500,
							book_review_link: "",
							contributor:
								"by Drew Daywalt. Illustrated by Oliver Jeffers",
							contributor_note: "Illustrated by Oliver Jeffers",
							created_date: "2023-07-19 22:10:24",
							description:
								"The crayons go back to school and can't wait for art class.",
							first_chapter_link: "",
							price: "0.00",
							primary_isbn10: "0593621115",
							primary_isbn13: "9780593621110",
							book_uri:
								"nyt://book/835b0f5f-fb74-5a99-9129-05012948ea7d",
							publisher: "Philomel",
							rank: 4,
							rank_last_week: 6,
							sunday_review_link: "",
							title: "THE CRAYONS GO BACK TO SCHOOL",
							updated_date: "2023-07-19 22:14:39",
							weeks_on_list: 5,
							buy_links: [
								{
									name: "Amazon",
									url: "https://www.amazon.com/dp/0593621115?tag=NYTBSREV-20",
								},
								{
									name: "Apple Books",
									url: "https://goto.applebooks.apple/9780593621110?at=10lIEQ",
								},
								{
									name: "Barnes and Noble",
									url: "https://www.anrdoezrs.net/click-7990613-11819508?url=https%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9780593621110",
								},
								{
									name: "Books-A-Million",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fp%252FTHE%252BCRAYONS%252BGO%252BBACK%252BTO%252BSCHOOL%252FDrew%252BDaywalt.%252F9780593621110&url2=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fsearch%253Fquery%253DTHE%252BCRAYONS%252BGO%252BBACK%252BTO%252BSCHOOL%252BDrew%252BDaywalt.",
								},
								{
									name: "Bookshop",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fbookshop.org%2Fa%2F3546%2F9780593621110&url2=https%3A%2F%2Fbookshop.org%2Fbooks%3Faffiliate%3D3546%26keywords%3DTHE%2BCRAYONS%2BGO%2BBACK%2BTO%2BSCHOOL",
								},
								{
									name: "IndieBound",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.indiebound.org%2Fbook%2F9780593621110%3Faff%3DNYT&url2=https%3A%2F%2Fwww.indiebound.org%2Fsearch%2Fbook%3Fkeys%3DTHE%2BCRAYONS%2BGO%2BBACK%2BTO%2BSCHOOL%2BDrew%2BDaywalt.%26aff%3DNYT",
								},
							],
						},
						{
							age_group: "",
							amazon_product_url:
								"https://www.amazon.com/dp/1368059589?tag=NYTBSREV-20",
							article_chapter_link: "",
							author: "Ryan T. Higgins",
							book_image:
								"https://storage.googleapis.com/du-prd/books/images/9781368059589.jpg",
							book_image_width: 500,
							book_image_height: 365,
							book_review_link: "",
							contributor: "by Ryan T. Higgins",
							contributor_note: "",
							created_date: "2023-07-19 22:10:24",
							description:
								"Bruce's family of mice and geese are determined to get him into the Halloween spirit.",
							first_chapter_link: "",
							price: "0.00",
							primary_isbn10: "1368059589",
							primary_isbn13: "9781368059589",
							book_uri:
								"nyt://book/4dff2dcd-2f13-525c-9288-0fdc1f6b46b6",
							publisher: "Disney Hyperion",
							rank: 5,
							rank_last_week: 0,
							sunday_review_link: "",
							title: "BRUCE AND THE LEGEND OF SOGGY HOLLOW",
							updated_date: "2023-07-19 22:14:39",
							weeks_on_list: 1,
							buy_links: [
								{
									name: "Amazon",
									url: "https://www.amazon.com/dp/1368059589?tag=NYTBSREV-20",
								},
								{
									name: "Apple Books",
									url: "https://goto.applebooks.apple/9781368059589?at=10lIEQ",
								},
								{
									name: "Barnes and Noble",
									url: "https://www.anrdoezrs.net/click-7990613-11819508?url=https%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9781368059589",
								},
								{
									name: "Books-A-Million",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fp%252FBRUCE%252BAND%252BTHE%252BLEGEND%252BOF%252BSOGGY%252BHOLLOW%252FRyan%252BT.%252BHiggins%252F9781368059589&url2=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fsearch%253Fquery%253DBRUCE%252BAND%252BTHE%252BLEGEND%252BOF%252BSOGGY%252BHOLLOW%252BRyan%252BT.%252BHiggins",
								},
								{
									name: "Bookshop",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fbookshop.org%2Fa%2F3546%2F9781368059589&url2=https%3A%2F%2Fbookshop.org%2Fbooks%3Faffiliate%3D3546%26keywords%3DBRUCE%2BAND%2BTHE%2BLEGEND%2BOF%2BSOGGY%2BHOLLOW",
								},
								{
									name: "IndieBound",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.indiebound.org%2Fbook%2F9781368059589%3Faff%3DNYT&url2=https%3A%2F%2Fwww.indiebound.org%2Fsearch%2Fbook%3Fkeys%3DBRUCE%2BAND%2BTHE%2BLEGEND%2BOF%2BSOGGY%2BHOLLOW%2BRyan%2BT.%2BHiggins%26aff%3DNYT",
								},
							],
						},
					],
				},
				{
					list_id: 10,
					list_name: "Series Books",
					list_name_encoded: "series-books",
					display_name: "Children’s Series",
					updated: "WEEKLY",
					list_image: null,
					list_image_width: null,
					list_image_height: null,
					books: [
						{
							age_group: "",
							amazon_product_url:
								"http://www.amazon.com/Well-Always-Summer-Turned-Pretty/dp/1416995587?tag=NYTBSREV-20",
							article_chapter_link: "",
							author: "Jenny Han",
							book_image:
								"https://storage.googleapis.com/du-prd/books/images/9781416995586.jpg",
							book_image_width: 266,
							book_image_height: 400,
							book_review_link: "",
							contributor: "by Jenny Han",
							contributor_note: "",
							created_date: "2023-07-19 22:10:19",
							description:
								"A beach house, summer love and enduring friendships.",
							first_chapter_link: "",
							price: "0.00",
							primary_isbn10: "1442498323",
							primary_isbn13: "9781442498327",
							book_uri:
								"nyt://book/0eda57dd-50e7-5233-8580-dc36e4b4b312",
							publisher: "Simon & Schuster",
							rank: 1,
							rank_last_week: 1,
							sunday_review_link: "",
							title: "THE SUMMER I TURNED PRETTY TRILOGY",
							updated_date: "2023-07-19 22:14:34",
							weeks_on_list: 65,
							buy_links: [
								{
									name: "Amazon",
									url: "http://www.amazon.com/Well-Always-Summer-Turned-Pretty/dp/1416995587?tag=NYTBSREV-20",
								},
								{
									name: "Apple Books",
									url: "https://goto.applebooks.apple/9781442498327?at=10lIEQ",
								},
								{
									name: "Barnes and Noble",
									url: "https://www.anrdoezrs.net/click-7990613-11819508?url=https%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9781442498327",
								},
								{
									name: "Books-A-Million",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fp%252FTHE%252BSUMMER%252BI%252BTURNED%252BPRETTY%252BTRILOGY%252FJenny%252BHan%252F9781442498327&url2=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fsearch%253Fquery%253DTHE%252BSUMMER%252BI%252BTURNED%252BPRETTY%252BTRILOGY%252BJenny%252BHan",
								},
								{
									name: "Bookshop",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fbookshop.org%2Fa%2F3546%2F9781442498327&url2=https%3A%2F%2Fbookshop.org%2Fbooks%3Faffiliate%3D3546%26keywords%3DTHE%2BSUMMER%2BI%2BTURNED%2BPRETTY%2BTRILOGY",
								},
								{
									name: "IndieBound",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.indiebound.org%2Fbook%2F9781442498327%3Faff%3DNYT&url2=https%3A%2F%2Fwww.indiebound.org%2Fsearch%2Fbook%3Fkeys%3DTHE%2BSUMMER%2BI%2BTURNED%2BPRETTY%2BTRILOGY%2BJenny%2BHan%26aff%3DNYT",
								},
							],
						},
						{
							age_group: "",
							amazon_product_url:
								"https://www.amazon.com/dp/0593379853?tag=NYTBSREV-20",
							article_chapter_link: "",
							author: "Holly Jackson",
							book_image:
								"https://storage.googleapis.com/du-prd/books/images/9780593379851.jpg",
							book_image_width: 331,
							book_image_height: 500,
							book_review_link: "",
							contributor: "by Holly Jackson",
							contributor_note: "",
							created_date: "2023-07-19 22:10:19",
							description:
								"Pippa Fitz-Amobi solves murderous crimes.",
							first_chapter_link: "",
							price: "0.00",
							primary_isbn10: "1984896393",
							primary_isbn13: "9781984896391",
							book_uri:
								"nyt://book/494423b3-84b1-5f41-ae97-b525e4a5245c",
							publisher: "Delacorte",
							rank: 2,
							rank_last_week: 2,
							sunday_review_link: "",
							title: "A GOOD GIRL'S GUIDE TO MURDER",
							updated_date: "2023-07-19 22:14:35",
							weeks_on_list: 94,
							buy_links: [
								{
									name: "Amazon",
									url: "https://www.amazon.com/dp/0593379853?tag=NYTBSREV-20",
								},
								{
									name: "Apple Books",
									url: "https://goto.applebooks.apple/9781984896391?at=10lIEQ",
								},
								{
									name: "Barnes and Noble",
									url: "https://www.anrdoezrs.net/click-7990613-11819508?url=https%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9781984896391",
								},
								{
									name: "Books-A-Million",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fp%252FA%252BGOOD%252BGIRL%252527S%252BGUIDE%252BTO%252BMURDER%252FHolly%252BJackson%252F9781984896391&url2=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fsearch%253Fquery%253DA%252BGOOD%252BGIRL%252527S%252BGUIDE%252BTO%252BMURDER%252BHolly%252BJackson",
								},
								{
									name: "Bookshop",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fbookshop.org%2Fa%2F3546%2F9781984896391&url2=https%3A%2F%2Fbookshop.org%2Fbooks%3Faffiliate%3D3546%26keywords%3DA%2BGOOD%2BGIRL%2527S%2BGUIDE%2BTO%2BMURDER",
								},
								{
									name: "IndieBound",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.indiebound.org%2Fbook%2F9781984896391%3Faff%3DNYT&url2=https%3A%2F%2Fwww.indiebound.org%2Fsearch%2Fbook%3Fkeys%3DA%2BGOOD%2BGIRL%2527S%2BGUIDE%2BTO%2BMURDER%2BHolly%2BJackson%26aff%3DNYT",
								},
							],
						},
						{
							age_group: "",
							amazon_product_url:
								"http://www.amazon.com/Diary-Wimpy-Kid-Hard-Luck/dp/1419711326?tag=NYTBSREV-20",
							article_chapter_link: "",
							author: "and   Jeff Kinney",
							book_image:
								"https://storage.googleapis.com/du-prd/books/images/9781419711329.jpg",
							book_image_width: 330,
							book_image_height: 484,
							book_review_link: "",
							contributor:
								"written and illustrated by Jeff Kinney",
							contributor_note:
								"written and illustrated by Jeff Kinney",
							created_date: "2023-07-19 22:10:19",
							description:
								"The travails and challenges of adolescence.",
							first_chapter_link: "",
							price: "0.00",
							primary_isbn10: "141976294X",
							primary_isbn13: "9781419762949",
							book_uri:
								"nyt://book/cb1d1f95-72fb-51ba-a5d1-fb54457d7ad9",
							publisher: "Amulet",
							rank: 3,
							rank_last_week: 3,
							sunday_review_link: "",
							title: "DIARY OF A WIMPY KID",
							updated_date: "2023-07-19 22:14:35",
							weeks_on_list: 748,
							buy_links: [
								{
									name: "Amazon",
									url: "http://www.amazon.com/Diary-Wimpy-Kid-Hard-Luck/dp/1419711326?tag=NYTBSREV-20",
								},
								{
									name: "Apple Books",
									url: "https://goto.applebooks.apple/9781419762949?at=10lIEQ",
								},
								{
									name: "Barnes and Noble",
									url: "https://www.anrdoezrs.net/click-7990613-11819508?url=https%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9781419762949",
								},
								{
									name: "Books-A-Million",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fp%252FDIARY%252BOF%252BA%252BWIMPY%252BKID%252Fand%252B%252B%252BJeff%252BKinney%252F9781419762949&url2=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fsearch%253Fquery%253DDIARY%252BOF%252BA%252BWIMPY%252BKID%252Band%252B%252B%252BJeff%252BKinney",
								},
								{
									name: "Bookshop",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fbookshop.org%2Fa%2F3546%2F9781419762949&url2=https%3A%2F%2Fbookshop.org%2Fbooks%3Faffiliate%3D3546%26keywords%3DDIARY%2BOF%2BA%2BWIMPY%2BKID",
								},
								{
									name: "IndieBound",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.indiebound.org%2Fbook%2F9781419762949%3Faff%3DNYT&url2=https%3A%2F%2Fwww.indiebound.org%2Fsearch%2Fbook%3Fkeys%3DDIARY%2BOF%2BA%2BWIMPY%2BKID%2Band%2B%2B%2BJeff%2BKinney%26aff%3DNYT",
								},
							],
						},
						{
							age_group: "",
							amazon_product_url:
								"http://www.amazon.com/Harry-Potter-And-Order-Phoenix/dp/0439358078?tag=NYTBSREV-20",
							article_chapter_link: "",
							author: "J.K. Rowling",
							book_image:
								"https://storage.googleapis.com/du-prd/books/images/9780590353427.jpg",
							book_image_width: 328,
							book_image_height: 495,
							book_review_link: "",
							contributor: "by J.K. Rowling",
							contributor_note: "",
							created_date: "2023-07-19 22:10:19",
							description:
								"A wizard hones his conjuring skills in the service of fighting evil.",
							first_chapter_link: "",
							price: "0.00",
							primary_isbn10: "1338878921",
							primary_isbn13: "9781338878929",
							book_uri:
								"nyt://book/9df40295-9d1e-5ee8-ba89-ba22d2992500",
							publisher: "Scholastic",
							rank: 4,
							rank_last_week: 4,
							sunday_review_link: "",
							title: "HARRY POTTER",
							updated_date: "2023-07-19 22:14:35",
							weeks_on_list: 747,
							buy_links: [
								{
									name: "Amazon",
									url: "http://www.amazon.com/Harry-Potter-And-Order-Phoenix/dp/0439358078?tag=NYTBSREV-20",
								},
								{
									name: "Apple Books",
									url: "https://goto.applebooks.apple/9781338878929?at=10lIEQ",
								},
								{
									name: "Barnes and Noble",
									url: "https://www.anrdoezrs.net/click-7990613-11819508?url=https%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9781338878929",
								},
								{
									name: "Books-A-Million",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fp%252FHARRY%252BPOTTER%252FJ.K.%252BRowling%252F9781338878929&url2=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fsearch%253Fquery%253DHARRY%252BPOTTER%252BJ.K.%252BRowling",
								},
								{
									name: "Bookshop",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fbookshop.org%2Fa%2F3546%2F9781338878929&url2=https%3A%2F%2Fbookshop.org%2Fbooks%3Faffiliate%3D3546%26keywords%3DHARRY%2BPOTTER",
								},
								{
									name: "IndieBound",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.indiebound.org%2Fbook%2F9781338878929%3Faff%3DNYT&url2=https%3A%2F%2Fwww.indiebound.org%2Fsearch%2Fbook%3Fkeys%3DHARRY%2BPOTTER%2BJ.K.%2BRowling%26aff%3DNYT",
								},
							],
						},
						{
							age_group: "",
							amazon_product_url:
								"http://www.amazon.com/Mockingjay-Final-Book-Hunger-Games/dp/0545663261?tag=NYTBSREV-20",
							article_chapter_link: "",
							author: "Suzanne Collins",
							book_image:
								"https://storage.googleapis.com/du-prd/books/images/9780545663267.jpg",
							book_image_width: 322,
							book_image_height: 495,
							book_review_link: "",
							contributor: "by Suzanne Collins",
							contributor_note: "",
							created_date: "2023-07-19 22:10:19",
							description:
								"In a dystopia, a girl fights for survival on live TV.",
							first_chapter_link: "",
							price: "0.00",
							primary_isbn10: "1338635174",
							primary_isbn13: "9781338635171",
							book_uri:
								"nyt://book/6023bfea-e267-5aca-8a56-2af3eba0439e",
							publisher: "Scholastic",
							rank: 5,
							rank_last_week: 5,
							sunday_review_link: "",
							title: "THE HUNGER GAMES",
							updated_date: "2023-07-19 22:14:35",
							weeks_on_list: 284,
							buy_links: [
								{
									name: "Amazon",
									url: "http://www.amazon.com/Mockingjay-Final-Book-Hunger-Games/dp/0545663261?tag=NYTBSREV-20",
								},
								{
									name: "Apple Books",
									url: "https://goto.applebooks.apple/9781338635171?at=10lIEQ",
								},
								{
									name: "Barnes and Noble",
									url: "https://www.anrdoezrs.net/click-7990613-11819508?url=https%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9781338635171",
								},
								{
									name: "Books-A-Million",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fp%252FTHE%252BHUNGER%252BGAMES%252FSuzanne%252BCollins%252F9781338635171&url2=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fsearch%253Fquery%253DTHE%252BHUNGER%252BGAMES%252BSuzanne%252BCollins",
								},
								{
									name: "Bookshop",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fbookshop.org%2Fa%2F3546%2F9781338635171&url2=https%3A%2F%2Fbookshop.org%2Fbooks%3Faffiliate%3D3546%26keywords%3DTHE%2BHUNGER%2BGAMES",
								},
								{
									name: "IndieBound",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.indiebound.org%2Fbook%2F9781338635171%3Faff%3DNYT&url2=https%3A%2F%2Fwww.indiebound.org%2Fsearch%2Fbook%3Fkeys%3DTHE%2BHUNGER%2BGAMES%2BSuzanne%2BCollins%26aff%3DNYT",
								},
							],
						},
					],
				},
				{
					list_id: 14,
					list_name: "Young Adult Hardcover",
					list_name_encoded: "young-adult-hardcover",
					display_name: "Young Adult Hardcover",
					updated: "WEEKLY",
					list_image: null,
					list_image_width: null,
					list_image_height: null,
					books: [
						{
							age_group: "",
							amazon_product_url:
								"https://www.amazon.com/dp/0593374169?tag=NYTBSREV-20",
							article_chapter_link: "",
							author: "Holly Jackson",
							book_image:
								"https://storage.googleapis.com/du-prd/books/images/9780593374160.jpg",
							book_image_width: 331,
							book_image_height: 500,
							book_review_link: "",
							contributor: "by Holly Jackson",
							contributor_note: "",
							created_date: "2023-07-19 22:10:20",
							description:
								"Six friends on a spring break road trip in an R.V. are the target of a sniper.",
							first_chapter_link: "",
							price: "0.00",
							primary_isbn10: "0593374169",
							primary_isbn13: "9780593374160",
							book_uri:
								"nyt://book/19555aab-32c6-52d2-9781-310810d0dd6f",
							publisher: "Delacorte",
							rank: 1,
							rank_last_week: 2,
							sunday_review_link: "",
							title: "FIVE SURVIVE",
							updated_date: "2023-07-19 22:14:36",
							weeks_on_list: 33,
							buy_links: [
								{
									name: "Amazon",
									url: "https://www.amazon.com/dp/0593374169?tag=NYTBSREV-20",
								},
								{
									name: "Apple Books",
									url: "https://goto.applebooks.apple/9780593374160?at=10lIEQ",
								},
								{
									name: "Barnes and Noble",
									url: "https://www.anrdoezrs.net/click-7990613-11819508?url=https%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9780593374160",
								},
								{
									name: "Books-A-Million",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fp%252FFIVE%252BSURVIVE%252FHolly%252BJackson%252F9780593374160&url2=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fsearch%253Fquery%253DFIVE%252BSURVIVE%252BHolly%252BJackson",
								},
								{
									name: "Bookshop",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fbookshop.org%2Fa%2F3546%2F9780593374160&url2=https%3A%2F%2Fbookshop.org%2Fbooks%3Faffiliate%3D3546%26keywords%3DFIVE%2BSURVIVE",
								},
								{
									name: "IndieBound",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.indiebound.org%2Fbook%2F9780593374160%3Faff%3DNYT&url2=https%3A%2F%2Fwww.indiebound.org%2Fsearch%2Fbook%3Fkeys%3DFIVE%2BSURVIVE%2BHolly%2BJackson%26aff%3DNYT",
								},
							],
						},
						{
							age_group: "",
							amazon_product_url:
								"https://www.amazon.com/dp/1338863428?tag=NYTBSREV-20",
							article_chapter_link: "",
							author: "Alice Oseman",
							book_image:
								"https://storage.googleapis.com/du-prd/books/images/9781339016238.jpg",
							book_image_width: 333,
							book_image_height: 500,
							book_review_link: "",
							contributor: "by Alice Oseman",
							contributor_note: "",
							created_date: "2023-07-19 22:10:20",
							description:
								"Tori Spring is determined to find out who's behind the blog called Solitaire, which has caused serious pranks at her school.",
							first_chapter_link: "",
							price: "0.00",
							primary_isbn10: "1338863428",
							primary_isbn13: "9781338863420",
							book_uri:
								"nyt://book/5c1b7a21-c152-51d2-af9e-fb641e4f90e5",
							publisher: "Scholastic",
							rank: 2,
							rank_last_week: 1,
							sunday_review_link: "",
							title: "SOLITAIRE",
							updated_date: "2023-07-19 22:14:36",
							weeks_on_list: 11,
							buy_links: [
								{
									name: "Amazon",
									url: "https://www.amazon.com/dp/1338863428?tag=NYTBSREV-20",
								},
								{
									name: "Apple Books",
									url: "https://goto.applebooks.apple/9781338863420?at=10lIEQ",
								},
								{
									name: "Barnes and Noble",
									url: "https://www.anrdoezrs.net/click-7990613-11819508?url=https%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9781338863420",
								},
								{
									name: "Books-A-Million",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fp%252FSOLITAIRE%252FAlice%252BOseman%252F9781338863420&url2=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fsearch%253Fquery%253DSOLITAIRE%252BAlice%252BOseman",
								},
								{
									name: "Bookshop",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fbookshop.org%2Fa%2F3546%2F9781338863420&url2=https%3A%2F%2Fbookshop.org%2Fbooks%3Faffiliate%3D3546%26keywords%3DSOLITAIRE",
								},
								{
									name: "IndieBound",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.indiebound.org%2Fbook%2F9781338863420%3Faff%3DNYT&url2=https%3A%2F%2Fwww.indiebound.org%2Fsearch%2Fbook%3Fkeys%3DSOLITAIRE%2BAlice%2BOseman%26aff%3DNYT",
								},
							],
						},
						{
							age_group: "",
							amazon_product_url:
								"https://www.amazon.com/dp/1338885103?tag=NYTBSREV-20",
							article_chapter_link: "",
							author: "Alice Oseman",
							book_image:
								"https://storage.googleapis.com/du-prd/books/images/9781338885101.jpg",
							book_image_width: 333,
							book_image_height: 500,
							book_review_link: "",
							contributor: "by Alice Oseman",
							contributor_note: "",
							created_date: "2023-07-19 22:10:20",
							description:
								"Nick and Charlie question whether their love is strong enough to survive being apart when Nick leaves for university.",
							first_chapter_link: "",
							price: "0.00",
							primary_isbn10: "1338885103",
							primary_isbn13: "9781338885101",
							book_uri:
								"nyt://book/bcf57ebd-38b2-59ea-bb70-bd09334e117e",
							publisher: "Scholastic",
							rank: 3,
							rank_last_week: 3,
							sunday_review_link: "",
							title: "NICK AND CHARLIE",
							updated_date: "2023-07-19 22:14:36",
							weeks_on_list: 28,
							buy_links: [
								{
									name: "Amazon",
									url: "https://www.amazon.com/dp/1338885103?tag=NYTBSREV-20",
								},
								{
									name: "Apple Books",
									url: "https://goto.applebooks.apple/9781338885101?at=10lIEQ",
								},
								{
									name: "Barnes and Noble",
									url: "https://www.anrdoezrs.net/click-7990613-11819508?url=https%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9781338885101",
								},
								{
									name: "Books-A-Million",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fp%252FNICK%252BAND%252BCHARLIE%252FAlice%252BOseman%252F9781338885101&url2=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fsearch%253Fquery%253DNICK%252BAND%252BCHARLIE%252BAlice%252BOseman",
								},
								{
									name: "Bookshop",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fbookshop.org%2Fa%2F3546%2F9781338885101&url2=https%3A%2F%2Fbookshop.org%2Fbooks%3Faffiliate%3D3546%26keywords%3DNICK%2BAND%2BCHARLIE",
								},
								{
									name: "IndieBound",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.indiebound.org%2Fbook%2F9781338885101%3Faff%3DNYT&url2=https%3A%2F%2Fwww.indiebound.org%2Fsearch%2Fbook%3Fkeys%3DNICK%2BAND%2BCHARLIE%2BAlice%2BOseman%26aff%3DNYT",
								},
							],
						},
						{
							age_group: "",
							amazon_product_url:
								"https://www.amazon.com/dp/0063240807?tag=NYTBSREV-20",
							article_chapter_link: "",
							author: "Adam Silvera",
							book_image:
								"https://storage.googleapis.com/du-prd/books/images/9780063240803.jpg",
							book_image_width: 331,
							book_image_height: 500,
							book_review_link: "",
							contributor: "by Adam Silvera",
							contributor_note: "",
							created_date: "2023-07-19 22:10:20",
							description:
								'In this prequel to "They Both Die at the End," Orion and Valentino attend the premiere of Death-Cast in Times Square.',
							first_chapter_link: "",
							price: "0.00",
							primary_isbn10: "0063240807",
							primary_isbn13: "9780063240803",
							book_uri:
								"nyt://book/0f81d9c3-b905-51f8-bba7-6f3c5610603c",
							publisher: "Quill Tree",
							rank: 4,
							rank_last_week: 4,
							sunday_review_link: "",
							title: "THE FIRST TO DIE AT THE END",
							updated_date: "2023-07-19 22:14:36",
							weeks_on_list: 41,
							buy_links: [
								{
									name: "Amazon",
									url: "https://www.amazon.com/dp/0063240807?tag=NYTBSREV-20",
								},
								{
									name: "Apple Books",
									url: "https://goto.applebooks.apple/9780063240803?at=10lIEQ",
								},
								{
									name: "Barnes and Noble",
									url: "https://www.anrdoezrs.net/click-7990613-11819508?url=https%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9780063240803",
								},
								{
									name: "Books-A-Million",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fp%252FTHE%252BFIRST%252BTO%252BDIE%252BAT%252BTHE%252BEND%252FAdam%252BSilvera%252F9780063240803&url2=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fsearch%253Fquery%253DTHE%252BFIRST%252BTO%252BDIE%252BAT%252BTHE%252BEND%252BAdam%252BSilvera",
								},
								{
									name: "Bookshop",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fbookshop.org%2Fa%2F3546%2F9780063240803&url2=https%3A%2F%2Fbookshop.org%2Fbooks%3Faffiliate%3D3546%26keywords%3DTHE%2BFIRST%2BTO%2BDIE%2BAT%2BTHE%2BEND",
								},
								{
									name: "IndieBound",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.indiebound.org%2Fbook%2F9780063240803%3Faff%3DNYT&url2=https%3A%2F%2Fwww.indiebound.org%2Fsearch%2Fbook%3Fkeys%3DTHE%2BFIRST%2BTO%2BDIE%2BAT%2BTHE%2BEND%2BAdam%2BSilvera%26aff%3DNYT",
								},
							],
						},
						{
							age_group: "",
							amazon_product_url:
								"https://www.amazon.com/dp/1250857430?tag=NYTBSREV-20",
							article_chapter_link: "",
							author: "Rebecca Ross",
							book_image:
								"https://storage.googleapis.com/du-prd/books/images/9781250857439.jpg",
							book_image_width: 329,
							book_image_height: 500,
							book_review_link: "",
							contributor: "by Rebecca Ross",
							contributor_note: "",
							created_date: "2023-07-19 22:10:20",
							description:
								"Two young rival journalists find love through a magical connection.",
							first_chapter_link: "",
							price: "0.00",
							primary_isbn10: "1250857430",
							primary_isbn13: "9781250857439",
							book_uri:
								"nyt://book/4e2ba9dd-e1e3-510d-9830-66259b3e98ec",
							publisher: "Wednesday",
							rank: 5,
							rank_last_week: 5,
							sunday_review_link: "",
							title: "DIVINE RIVALS",
							updated_date: "2023-07-19 22:14:36",
							weeks_on_list: 4,
							buy_links: [
								{
									name: "Amazon",
									url: "https://www.amazon.com/dp/1250857430?tag=NYTBSREV-20",
								},
								{
									name: "Apple Books",
									url: "https://goto.applebooks.apple/9781250857439?at=10lIEQ",
								},
								{
									name: "Barnes and Noble",
									url: "https://www.anrdoezrs.net/click-7990613-11819508?url=https%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9781250857439",
								},
								{
									name: "Books-A-Million",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fp%252FDIVINE%252BRIVALS%252FRebecca%252BRoss%252F9781250857439&url2=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fsearch%253Fquery%253DDIVINE%252BRIVALS%252BRebecca%252BRoss",
								},
								{
									name: "Bookshop",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fbookshop.org%2Fa%2F3546%2F9781250857439&url2=https%3A%2F%2Fbookshop.org%2Fbooks%3Faffiliate%3D3546%26keywords%3DDIVINE%2BRIVALS",
								},
								{
									name: "IndieBound",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.indiebound.org%2Fbook%2F9781250857439%3Faff%3DNYT&url2=https%3A%2F%2Fwww.indiebound.org%2Fsearch%2Fbook%3Fkeys%3DDIVINE%2BRIVALS%2BRebecca%2BRoss%26aff%3DNYT",
								},
							],
						},
					],
				},
				{
					list_id: 301,
					list_name: "Audio Fiction",
					list_name_encoded: "audio-fiction",
					display_name: "Audio Fiction",
					updated: "MONTHLY",
					list_image: null,
					list_image_width: null,
					list_image_height: null,
					books: [
						{
							age_group: "",
							amazon_product_url:
								"https://www.amazon.com/dp/038554734X?tag=NYTBSREV-20",
							article_chapter_link: "",
							author: "Bonnie Garmus",
							book_image:
								"https://storage.googleapis.com/du-prd/books/images/9780385547345.jpg",
							book_image_width: 328,
							book_image_height: 500,
							book_review_link: "",
							contributor: "by Bonnie Garmus",
							contributor_note: "",
							created_date: "2023-07-05 23:06:11",
							description:
								"A scientist and single mother living in California in the 1960s becomes a star on a TV cooking show. Read by Miranda Raison, Pandora Sykes and the author. 11 hours, 55 minutes unabridged.",
							first_chapter_link: "",
							price: "0.00",
							primary_isbn10: "0593507533",
							primary_isbn13: "9780593507537",
							book_uri:
								"nyt://book/a7229ef2-7522-5cb7-86c4-024aca7420e7",
							publisher: "Random House Audio",
							rank: 1,
							rank_last_week: 0,
							sunday_review_link: "",
							title: "LESSONS IN CHEMISTRY",
							updated_date: "2023-07-05 23:06:11",
							weeks_on_list: 0,
							buy_links: [
								{
									name: "Amazon",
									url: "https://www.amazon.com/dp/038554734X?tag=NYTBSREV-20",
								},
								{
									name: "Apple Books",
									url: "https://goto.applebooks.apple/9780593507537?at=10lIEQ",
								},
								{
									name: "Barnes and Noble",
									url: "https://www.anrdoezrs.net/click-7990613-11819508?url=https%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9780593507537",
								},
								{
									name: "Books-A-Million",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fp%252FLESSONS%252BIN%252BCHEMISTRY%252FBonnie%252BGarmus%252F9780593507537&url2=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fsearch%253Fquery%253DLESSONS%252BIN%252BCHEMISTRY%252BBonnie%252BGarmus",
								},
								{
									name: "Bookshop",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fbookshop.org%2Fa%2F3546%2F9780593507537&url2=https%3A%2F%2Fbookshop.org%2Fbooks%3Faffiliate%3D3546%26keywords%3DLESSONS%2BIN%2BCHEMISTRY",
								},
								{
									name: "IndieBound",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.indiebound.org%2Fbook%2F9780593507537%3Faff%3DNYT&url2=https%3A%2F%2Fwww.indiebound.org%2Fsearch%2Fbook%3Fkeys%3DLESSONS%2BIN%2BCHEMISTRY%2BBonnie%2BGarmus%26aff%3DNYT",
								},
							],
						},
						{
							age_group: "",
							amazon_product_url:
								"https://www.amazon.com/dp/1649374046?tag=NYTBSREV-20",
							article_chapter_link: "",
							author: "Rebecca Yarros",
							book_image:
								"https://storage.googleapis.com/du-prd/books/images/9781649374042.jpg",
							book_image_width: 309,
							book_image_height: 500,
							book_review_link: "",
							contributor: "by Rebecca Yarros",
							contributor_note: "",
							created_date: "2023-07-05 23:06:11",
							description:
								"Violet Sorrengail is urged by the commanding general, who also is her mother, to become a candidate for the elite dragon riders. Read by Rebecca Soler and Teddy Hamilton. 20 hours, 47 minutes unabridged.",
							first_chapter_link: "",
							price: "0.00",
							primary_isbn10: "1705085059",
							primary_isbn13: "9781705085059",
							book_uri:
								"nyt://book/fb6b3476-1860-5b9d-8df2-4d06d0e54d05",
							publisher: "Recorded Books",
							rank: 2,
							rank_last_week: 0,
							sunday_review_link: "",
							title: "FOURTH WING",
							updated_date: "2023-07-05 23:06:11",
							weeks_on_list: 0,
							buy_links: [
								{
									name: "Amazon",
									url: "https://www.amazon.com/dp/1649374046?tag=NYTBSREV-20",
								},
								{
									name: "Apple Books",
									url: "https://goto.applebooks.apple/9781705085059?at=10lIEQ",
								},
								{
									name: "Barnes and Noble",
									url: "https://www.anrdoezrs.net/click-7990613-11819508?url=https%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9781705085059",
								},
								{
									name: "Books-A-Million",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fp%252FFOURTH%252BWING%252FRebecca%252BYarros%252F9781705085059&url2=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fsearch%253Fquery%253DFOURTH%252BWING%252BRebecca%252BYarros",
								},
								{
									name: "Bookshop",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fbookshop.org%2Fa%2F3546%2F9781705085059&url2=https%3A%2F%2Fbookshop.org%2Fbooks%3Faffiliate%3D3546%26keywords%3DFOURTH%2BWING",
								},
								{
									name: "IndieBound",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.indiebound.org%2Fbook%2F9781705085059%3Faff%3DNYT&url2=https%3A%2F%2Fwww.indiebound.org%2Fsearch%2Fbook%3Fkeys%3DFOURTH%2BWING%2BRebecca%2BYarros%26aff%3DNYT",
								},
							],
						},
						{
							age_group: "",
							amazon_product_url:
								"https://www.amazon.com/dp/1250284112?tag=NYTBSREV-20",
							article_chapter_link: "",
							author: "Nora Roberts",
							book_image:
								"https://storage.googleapis.com/du-prd/books/images/9781250284112.jpg",
							book_image_width: 329,
							book_image_height: 500,
							book_review_link: "",
							contributor: "by Nora Roberts",
							contributor_note: "",
							created_date: "2023-07-05 23:06:11",
							description:
								"After her roommate is killed by a con artist, a former Army brat builds a new life at her mother's home in Vermont. Read by January LaVoy. 15 hours, 4 minutes unabridged.",
							first_chapter_link: "",
							price: "0.00",
							primary_isbn10: "",
							primary_isbn13: "9781250893338",
							book_uri:
								"nyt://book/7cd90059-ec8b-53ae-8a5d-6697b3c191c7",
							publisher: "Macmillan Audio",
							rank: 3,
							rank_last_week: 0,
							sunday_review_link: "",
							title: "IDENTITY",
							updated_date: "2023-07-05 23:06:11",
							weeks_on_list: 0,
							buy_links: [
								{
									name: "Amazon",
									url: "https://www.amazon.com/dp/1250284112?tag=NYTBSREV-20",
								},
								{
									name: "Apple Books",
									url: "https://goto.applebooks.apple/9781250893338?at=10lIEQ",
								},
								{
									name: "Barnes and Noble",
									url: "https://www.anrdoezrs.net/click-7990613-11819508?url=https%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9781250893338",
								},
								{
									name: "Books-A-Million",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fp%252FIDENTITY%252FNora%252BRoberts%252F9781250893338&url2=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fsearch%253Fquery%253DIDENTITY%252BNora%252BRoberts",
								},
								{
									name: "Bookshop",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fbookshop.org%2Fa%2F3546%2F9781250893338&url2=https%3A%2F%2Fbookshop.org%2Fbooks%3Faffiliate%3D3546%26keywords%3DIDENTITY",
								},
								{
									name: "IndieBound",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.indiebound.org%2Fbook%2F9781250893338%3Faff%3DNYT&url2=https%3A%2F%2Fwww.indiebound.org%2Fsearch%2Fbook%3Fkeys%3DIDENTITY%2BNora%2BRoberts%26aff%3DNYT",
								},
							],
						},
						{
							age_group: "",
							amazon_product_url:
								"https://www.amazon.com/dp/0802162177?tag=NYTBSREV-20",
							article_chapter_link: "",
							author: "Abraham Verghese",
							book_image:
								"https://storage.googleapis.com/du-prd/books/images/9780802162175.jpg",
							book_image_width: 320,
							book_image_height: 500,
							book_review_link: "",
							contributor: "by Abraham Verghese",
							contributor_note: "",
							created_date: "2023-07-05 23:06:11",
							description:
								"Three generations of a family living on South India’s Malabar Coast suffer the loss of a family member by drowning. Read by the author. 31 hours, 16 minutes unabridged.",
							first_chapter_link: "",
							price: "0.00",
							primary_isbn10: "1705070248",
							primary_isbn13: "9781705070246",
							book_uri:
								"nyt://book/5fffb595-5d46-51f5-a683-449ac9645e64",
							publisher: "Recorded Books",
							rank: 4,
							rank_last_week: 0,
							sunday_review_link: "",
							title: "THE COVENANT OF WATER",
							updated_date: "2023-07-05 23:06:11",
							weeks_on_list: 0,
							buy_links: [
								{
									name: "Amazon",
									url: "https://www.amazon.com/dp/0802162177?tag=NYTBSREV-20",
								},
								{
									name: "Apple Books",
									url: "https://goto.applebooks.apple/9781705070246?at=10lIEQ",
								},
								{
									name: "Barnes and Noble",
									url: "https://www.anrdoezrs.net/click-7990613-11819508?url=https%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9781705070246",
								},
								{
									name: "Books-A-Million",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fp%252FTHE%252BCOVENANT%252BOF%252BWATER%252FAbraham%252BVerghese%252F9781705070246&url2=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fsearch%253Fquery%253DTHE%252BCOVENANT%252BOF%252BWATER%252BAbraham%252BVerghese",
								},
								{
									name: "Bookshop",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fbookshop.org%2Fa%2F3546%2F9781705070246&url2=https%3A%2F%2Fbookshop.org%2Fbooks%3Faffiliate%3D3546%26keywords%3DTHE%2BCOVENANT%2BOF%2BWATER",
								},
								{
									name: "IndieBound",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.indiebound.org%2Fbook%2F9781705070246%3Faff%3DNYT&url2=https%3A%2F%2Fwww.indiebound.org%2Fsearch%2Fbook%3Fkeys%3DTHE%2BCOVENANT%2BOF%2BWATER%2BAbraham%2BVerghese%26aff%3DNYT",
								},
							],
						},
						{
							age_group: "",
							amazon_product_url:
								"https://www.amazon.com/dp/0063251922?tag=NYTBSREV-20",
							article_chapter_link: "",
							author: "Barbara Kingsolver",
							book_image:
								"https://storage.googleapis.com/du-prd/books/images/9780063251922.jpg",
							book_image_width: 329,
							book_image_height: 500,
							book_review_link: "",
							contributor: "by Barbara Kingsolver",
							contributor_note: "",
							created_date: "2023-07-05 23:06:11",
							description:
								"Winner of a 2023 Pulitzer Prize for fiction. A reimagining of Charles Dickens’s “David Copperfield” set in the mountains of southern Appalachia. Read by Charlie Thurston. 21 hours, 3 minutes unabridged.",
							first_chapter_link: "",
							price: "0.00",
							primary_isbn10: "",
							primary_isbn13: "9780063252004",
							book_uri:
								"nyt://book/967e65c2-3fb5-5bca-b194-231600bef6f5",
							publisher: "HarperAudio",
							rank: 5,
							rank_last_week: 0,
							sunday_review_link: "",
							title: "DEMON COPPERHEAD",
							updated_date: "2023-07-05 23:06:11",
							weeks_on_list: 0,
							buy_links: [
								{
									name: "Amazon",
									url: "https://www.amazon.com/dp/0063251922?tag=NYTBSREV-20",
								},
								{
									name: "Apple Books",
									url: "https://goto.applebooks.apple/9780063252004?at=10lIEQ",
								},
								{
									name: "Barnes and Noble",
									url: "https://www.anrdoezrs.net/click-7990613-11819508?url=https%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9780063252004",
								},
								{
									name: "Books-A-Million",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fp%252FDEMON%252BCOPPERHEAD%252FBarbara%252BKingsolver%252F9780063252004&url2=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fsearch%253Fquery%253DDEMON%252BCOPPERHEAD%252BBarbara%252BKingsolver",
								},
								{
									name: "Bookshop",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fbookshop.org%2Fa%2F3546%2F9780063252004&url2=https%3A%2F%2Fbookshop.org%2Fbooks%3Faffiliate%3D3546%26keywords%3DDEMON%2BCOPPERHEAD",
								},
								{
									name: "IndieBound",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.indiebound.org%2Fbook%2F9780063252004%3Faff%3DNYT&url2=https%3A%2F%2Fwww.indiebound.org%2Fsearch%2Fbook%3Fkeys%3DDEMON%2BCOPPERHEAD%2BBarbara%2BKingsolver%26aff%3DNYT",
								},
							],
						},
					],
				},
				{
					list_id: 302,
					list_name: "Audio Nonfiction",
					list_name_encoded: "audio-nonfiction",
					display_name: "Audio Nonfiction",
					updated: "MONTHLY",
					list_image: null,
					list_image_width: null,
					list_image_height: null,
					books: [
						{
							age_group: "",
							amazon_product_url:
								"https://www.amazon.com/dp/0593236599?tag=NYTBSREV-20",
							article_chapter_link: "",
							author: "Peter Attia with Bill Gifford",
							book_image:
								"https://storage.googleapis.com/du-prd/books/images/9780593236598.jpg",
							book_image_width: 385,
							book_image_height: 500,
							book_review_link: "",
							contributor: "by Peter Attia with Bill Gifford",
							contributor_note: "",
							created_date: "2023-07-05 23:06:04",
							description:
								"A look at recent scientific research on aging and longevity. Read by Peter Attia. 17 hours, 8 minutes unabridged.",
							first_chapter_link: "",
							price: "0.00",
							primary_isbn10: "0593664442",
							primary_isbn13: "9780593664445",
							book_uri:
								"nyt://book/e093216d-a3f3-5b3b-8bee-fea6808cabb8",
							publisher: "Random House Audio",
							rank: 1,
							rank_last_week: 0,
							sunday_review_link: "",
							title: "OUTLIVE",
							updated_date: "2023-07-05 23:06:04",
							weeks_on_list: 0,
							buy_links: [
								{
									name: "Amazon",
									url: "https://www.amazon.com/dp/0593236599?tag=NYTBSREV-20",
								},
								{
									name: "Apple Books",
									url: "https://goto.applebooks.apple/9780593664445?at=10lIEQ",
								},
								{
									name: "Barnes and Noble",
									url: "https://www.anrdoezrs.net/click-7990613-11819508?url=https%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9780593664445",
								},
								{
									name: "Books-A-Million",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fp%252FOUTLIVE%252FPeter%252BAttia%252Bwith%252BBill%252BGifford%252F9780593664445&url2=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fsearch%253Fquery%253DOUTLIVE%252BPeter%252BAttia%252Bwith%252BBill%252BGifford",
								},
								{
									name: "Bookshop",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fbookshop.org%2Fa%2F3546%2F9780593664445&url2=https%3A%2F%2Fbookshop.org%2Fbooks%3Faffiliate%3D3546%26keywords%3DOUTLIVE",
								},
								{
									name: "IndieBound",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.indiebound.org%2Fbook%2F9780593664445%3Faff%3DNYT&url2=https%3A%2F%2Fwww.indiebound.org%2Fsearch%2Fbook%3Fkeys%3DOUTLIVE%2BPeter%2BAttia%2Bwith%2BBill%2BGifford%26aff%3DNYT",
								},
							],
						},
						{
							age_group: "",
							amazon_product_url:
								"https://www.amazon.com/dp/1982185821?tag=NYTBSREV-20",
							article_chapter_link: "",
							author: "Jennette McCurdy",
							book_image:
								"https://storage.googleapis.com/du-prd/books/images/9781982185824.jpg",
							book_image_width: 329,
							book_image_height: 500,
							book_review_link: "",
							contributor: "by Jennette McCurdy",
							contributor_note: "",
							created_date: "2023-07-05 23:06:04",
							description:
								"The actress and filmmaker describes her eating disorders and difficult relationship with her mother. Read by the author. 6 hours, 25 minutes unabridged.",
							first_chapter_link: "",
							price: "0.00",
							primary_isbn10: "",
							primary_isbn13: "9781797147949",
							book_uri:
								"nyt://book/dade20d6-b303-510c-9687-48eab9308755",
							publisher: "Simon & Schuster Audio",
							rank: 2,
							rank_last_week: 0,
							sunday_review_link: "",
							title: "I'M GLAD MY MOM DIED",
							updated_date: "2023-07-05 23:06:04",
							weeks_on_list: 0,
							buy_links: [
								{
									name: "Amazon",
									url: "https://www.amazon.com/dp/1982185821?tag=NYTBSREV-20",
								},
								{
									name: "Apple Books",
									url: "https://goto.applebooks.apple/9781797147949?at=10lIEQ",
								},
								{
									name: "Barnes and Noble",
									url: "https://www.anrdoezrs.net/click-7990613-11819508?url=https%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9781797147949",
								},
								{
									name: "Books-A-Million",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fp%252FI%252527M%252BGLAD%252BMY%252BMOM%252BDIED%252FJennette%252BMcCurdy%252F9781797147949&url2=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fsearch%253Fquery%253DI%252527M%252BGLAD%252BMY%252BMOM%252BDIED%252BJennette%252BMcCurdy",
								},
								{
									name: "Bookshop",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fbookshop.org%2Fa%2F3546%2F9781797147949&url2=https%3A%2F%2Fbookshop.org%2Fbooks%3Faffiliate%3D3546%26keywords%3DI%2527M%2BGLAD%2BMY%2BMOM%2BDIED",
								},
								{
									name: "IndieBound",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.indiebound.org%2Fbook%2F9781797147949%3Faff%3DNYT&url2=https%3A%2F%2Fwww.indiebound.org%2Fsearch%2Fbook%3Fkeys%3DI%2527M%2BGLAD%2BMY%2BMOM%2BDIED%2BJennette%2BMcCurdy%26aff%3DNYT",
								},
							],
						},
						{
							age_group: "",
							amazon_product_url:
								"https://www.amazon.com/dp/1250878357?tag=NYTBSREV-20",
							article_chapter_link: "",
							author: "Elliot Page",
							book_image:
								"https://storage.googleapis.com/du-prd/books/images/9781250878359.jpg",
							book_image_width: 324,
							book_image_height: 500,
							book_review_link: "",
							contributor: "by Elliot Page",
							contributor_note: "",
							created_date: "2023-07-05 23:06:04",
							description:
								"The Oscar-nominated star details discovering himself as a trans person and navigating abuse in Hollywood. Read by the author. 8 hours, 23 minutes unabridged.",
							first_chapter_link: "",
							price: "0.00",
							primary_isbn10: "",
							primary_isbn13: "9781250897664",
							book_uri:
								"nyt://book/80848bc3-eb06-5a8f-a71e-9fe50bfba233",
							publisher: "Macmillan Audio",
							rank: 3,
							rank_last_week: 0,
							sunday_review_link: "",
							title: "PAGEBOY",
							updated_date: "2023-07-05 23:06:04",
							weeks_on_list: 0,
							buy_links: [
								{
									name: "Amazon",
									url: "https://www.amazon.com/dp/1250878357?tag=NYTBSREV-20",
								},
								{
									name: "Apple Books",
									url: "https://goto.applebooks.apple/9781250897664?at=10lIEQ",
								},
								{
									name: "Barnes and Noble",
									url: "https://www.anrdoezrs.net/click-7990613-11819508?url=https%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9781250897664",
								},
								{
									name: "Books-A-Million",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fp%252FPAGEBOY%252FElliot%252BPage%252F9781250897664&url2=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fsearch%253Fquery%253DPAGEBOY%252BElliot%252BPage",
								},
								{
									name: "Bookshop",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fbookshop.org%2Fa%2F3546%2F9781250897664&url2=https%3A%2F%2Fbookshop.org%2Fbooks%3Faffiliate%3D3546%26keywords%3DPAGEBOY",
								},
								{
									name: "IndieBound",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.indiebound.org%2Fbook%2F9781250897664%3Faff%3DNYT&url2=https%3A%2F%2Fwww.indiebound.org%2Fsearch%2Fbook%3Fkeys%3DPAGEBOY%2BElliot%2BPage%26aff%3DNYT",
								},
							],
						},
						{
							age_group: "",
							amazon_product_url:
								"https://www.amazon.com/dp/0593139135?tag=NYTBSREV-20",
							article_chapter_link: "",
							author: "Matthew McConaughey",
							book_image:
								"https://storage.googleapis.com/du-prd/books/images/9780593139134.jpg",
							book_image_width: 392,
							book_image_height: 500,
							book_review_link: "",
							contributor: "by Matthew McConaughey",
							contributor_note: "",
							created_date: "2023-07-05 23:06:04",
							description:
								"The Academy Award-winning actor shares snippets from the diaries he kept over 35 years. Read by the author. 6 hours, 42 minutes unabridged.",
							first_chapter_link: "",
							price: "0.00",
							primary_isbn10: "",
							primary_isbn13: "9780593294185",
							book_uri:
								"nyt://book/cdd69a90-455d-5194-8060-86c0df2ea509",
							publisher: "Random House Audio",
							rank: 4,
							rank_last_week: 0,
							sunday_review_link: "",
							title: "GREENLIGHTS",
							updated_date: "2023-07-05 23:06:04",
							weeks_on_list: 0,
							buy_links: [
								{
									name: "Amazon",
									url: "https://www.amazon.com/dp/0593139135?tag=NYTBSREV-20",
								},
								{
									name: "Apple Books",
									url: "https://goto.applebooks.apple/9780593294185?at=10lIEQ",
								},
								{
									name: "Barnes and Noble",
									url: "https://www.anrdoezrs.net/click-7990613-11819508?url=https%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9780593294185",
								},
								{
									name: "Books-A-Million",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fp%252FGREENLIGHTS%252FMatthew%252BMcConaughey%252F9780593294185&url2=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fsearch%253Fquery%253DGREENLIGHTS%252BMatthew%252BMcConaughey",
								},
								{
									name: "Bookshop",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fbookshop.org%2Fa%2F3546%2F9780593294185&url2=https%3A%2F%2Fbookshop.org%2Fbooks%3Faffiliate%3D3546%26keywords%3DGREENLIGHTS",
								},
								{
									name: "IndieBound",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.indiebound.org%2Fbook%2F9780593294185%3Faff%3DNYT&url2=https%3A%2F%2Fwww.indiebound.org%2Fsearch%2Fbook%3Fkeys%3DGREENLIGHTS%2BMatthew%2BMcConaughey%26aff%3DNYT",
								},
							],
						},
						{
							age_group: "",
							amazon_product_url:
								"https://www.amazon.com/dp/1510766804?tag=NYTBSREV-20",
							article_chapter_link: "",
							author: "Robert F. Kennedy Jr",
							book_image:
								"https://storage.googleapis.com/du-prd/books/images/9781510766808.jpg",
							book_image_width: 334,
							book_image_height: 500,
							book_review_link: "",
							contributor: "by Robert F. Kennedy Jr.",
							contributor_note: "",
							created_date: "2023-07-05 23:06:04",
							description:
								"The anti-vaccine advocate gives his take on the former chief medical advisor to the president. Read by Bruce Wagner. 27 hours, 20 minutes unabridged.",
							first_chapter_link: "",
							price: "0.00",
							primary_isbn10: "",
							primary_isbn13: "DBKACX0286289",
							book_uri:
								"nyt://book/50093459-fb0d-54bc-a8b3-0091663acaa0",
							publisher: "Skyhorse",
							rank: 5,
							rank_last_week: 0,
							sunday_review_link: "",
							title: "THE REAL ANTHONY FAUCI",
							updated_date: "2023-07-05 23:06:04",
							weeks_on_list: 0,
							buy_links: [
								{
									name: "Amazon",
									url: "https://www.amazon.com/dp/1510766804?tag=NYTBSREV-20",
								},
								{
									name: "Apple Books",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/buy?title=THE+REAL+ANTHONY+FAUCI&author=Robert+F.+Kennedy+Jr&isbn=DBKACX0286289",
								},
								{
									name: "Barnes and Noble",
									url: "https://www.anrdoezrs.net/click-7990613-11819508?url=https%3A%2F%2Fwww.barnesandnoble.com%2Fs%2FTHE+REAL+ANTHONY+FAUCI+Robert+F.+Kennedy+Jr",
								},
								{
									name: "Books-A-Million",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fp%252FTHE%252BREAL%252BANTHONY%252BFAUCI%252FRobert%252BF.%252BKennedy%252BJr%252FDBKACX0286289&url2=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fsearch%253Fquery%253DTHE%252BREAL%252BANTHONY%252BFAUCI%252BRobert%252BF.%252BKennedy%252BJr",
								},
								{
									name: "Bookshop",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fbookshop.org%2Fa%2F3546%2FDBKACX0286289&url2=https%3A%2F%2Fbookshop.org%2Fbooks%3Faffiliate%3D3546%26keywords%3DTHE%2BREAL%2BANTHONY%2BFAUCI",
								},
								{
									name: "IndieBound",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.indiebound.org%2Fbook%2FDBKACX0286289%3Faff%3DNYT&url2=https%3A%2F%2Fwww.indiebound.org%2Fsearch%2Fbook%3Fkeys%3DTHE%2BREAL%2BANTHONY%2BFAUCI%2BRobert%2BF.%2BKennedy%2BJr%26aff%3DNYT",
								},
							],
						},
					],
				},
				{
					list_id: 532,
					list_name: "Business Books",
					list_name_encoded: "business-books",
					display_name: "Business",
					updated: "MONTHLY",
					list_image: null,
					list_image_width: null,
					list_image_height: null,
					books: [
						{
							age_group: "",
							amazon_product_url:
								"https://www.amazon.com/Atomic-Habits-Proven-Build-Break/dp/0735211299?tag=NYTBSREV-20",
							article_chapter_link: "",
							author: "James Clear",
							book_image:
								"https://storage.googleapis.com/du-prd/books/images/9780735211292.jpg",
							book_image_width: 328,
							book_image_height: 495,
							book_review_link: "",
							contributor: "by James Clear",
							contributor_note: "",
							created_date: "2023-07-05 23:06:13",
							description: "",
							first_chapter_link: "",
							price: "0.00",
							primary_isbn10: "0735211299",
							primary_isbn13: "9780735211292",
							book_uri:
								"nyt://book/0398a355-c032-534e-a0af-647b06f0840d",
							publisher: "Avery",
							rank: 1,
							rank_last_week: 0,
							sunday_review_link: "",
							title: "ATOMIC HABITS",
							updated_date: "2023-07-05 23:06:13",
							weeks_on_list: 0,
							buy_links: [
								{
									name: "Amazon",
									url: "https://www.amazon.com/Atomic-Habits-Proven-Build-Break/dp/0735211299?tag=NYTBSREV-20",
								},
								{
									name: "Apple Books",
									url: "https://goto.applebooks.apple/9780735211292?at=10lIEQ",
								},
								{
									name: "Barnes and Noble",
									url: "https://www.anrdoezrs.net/click-7990613-11819508?url=https%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9780735211292",
								},
								{
									name: "Books-A-Million",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fp%252FATOMIC%252BHABITS%252FJames%252BClear%252F9780735211292&url2=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fsearch%253Fquery%253DATOMIC%252BHABITS%252BJames%252BClear",
								},
								{
									name: "Bookshop",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fbookshop.org%2Fa%2F3546%2F9780735211292&url2=https%3A%2F%2Fbookshop.org%2Fbooks%3Faffiliate%3D3546%26keywords%3DATOMIC%2BHABITS",
								},
								{
									name: "IndieBound",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.indiebound.org%2Fbook%2F9780735211292%3Faff%3DNYT&url2=https%3A%2F%2Fwww.indiebound.org%2Fsearch%2Fbook%3Fkeys%3DATOMIC%2BHABITS%2BJames%2BClear%26aff%3DNYT",
								},
							],
						},
						{
							age_group: "",
							amazon_product_url:
								"https://www.amazon.com/dp/0593652886?tag=NYTBSREV-20",
							article_chapter_link: "",
							author: "Rick Rubin with Neil Strauss",
							book_image:
								"https://storage.googleapis.com/du-prd/books/images/9780593652886.jpg",
							book_image_width: 341,
							book_image_height: 500,
							book_review_link: "",
							contributor: "by Rick Rubin with Neil Strauss",
							contributor_note: "",
							created_date: "2023-07-05 23:06:13",
							description: "",
							first_chapter_link: "",
							price: "0.00",
							primary_isbn10: "0593652886",
							primary_isbn13: "9780593652886",
							book_uri:
								"nyt://book/da28ca16-1ebd-55aa-aba3-fde278604a82",
							publisher: "Penguin Press",
							rank: 2,
							rank_last_week: 0,
							sunday_review_link: "",
							title: "THE CREATIVE ACT",
							updated_date: "2023-07-05 23:06:13",
							weeks_on_list: 0,
							buy_links: [
								{
									name: "Amazon",
									url: "https://www.amazon.com/dp/0593652886?tag=NYTBSREV-20",
								},
								{
									name: "Apple Books",
									url: "https://goto.applebooks.apple/9780593652886?at=10lIEQ",
								},
								{
									name: "Barnes and Noble",
									url: "https://www.anrdoezrs.net/click-7990613-11819508?url=https%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9780593652886",
								},
								{
									name: "Books-A-Million",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fp%252FTHE%252BCREATIVE%252BACT%252FRick%252BRubin%252Bwith%252BNeil%252BStrauss%252F9780593652886&url2=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fsearch%253Fquery%253DTHE%252BCREATIVE%252BACT%252BRick%252BRubin%252Bwith%252BNeil%252BStrauss",
								},
								{
									name: "Bookshop",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fbookshop.org%2Fa%2F3546%2F9780593652886&url2=https%3A%2F%2Fbookshop.org%2Fbooks%3Faffiliate%3D3546%26keywords%3DTHE%2BCREATIVE%2BACT",
								},
								{
									name: "IndieBound",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.indiebound.org%2Fbook%2F9780593652886%3Faff%3DNYT&url2=https%3A%2F%2Fwww.indiebound.org%2Fsearch%2Fbook%3Fkeys%3DTHE%2BCREATIVE%2BACT%2BRick%2BRubin%2Bwith%2BNeil%2BStrauss%26aff%3DNYT",
								},
							],
						},
						{
							age_group: "",
							amazon_product_url:
								"https://www.amazon.com/Will-Teach-You-Rich-Second-ebook/dp/B07GNXPP4P?tag=NYTBSREV-20",
							article_chapter_link: "",
							author: "Ramit Sethi",
							book_image:
								"https://storage.googleapis.com/du-prd/books/images/9781523507870.jpg",
							book_image_width: 329,
							book_image_height: 495,
							book_review_link: "",
							contributor: "by Ramit Sethi",
							contributor_note: "",
							created_date: "2023-07-05 23:06:13",
							description: "",
							first_chapter_link: "",
							price: "0.00",
							primary_isbn10: "1523505745",
							primary_isbn13: "9781523505746",
							book_uri:
								"nyt://book/b0fe54f6-2468-55d3-bedf-0338f98478e6",
							publisher: "Workman",
							rank: 3,
							rank_last_week: 0,
							sunday_review_link: "",
							title: "I WILL TEACH YOU TO BE RICH, SECOND EDITION",
							updated_date: "2023-07-05 23:06:13",
							weeks_on_list: 0,
							buy_links: [
								{
									name: "Amazon",
									url: "https://www.amazon.com/Will-Teach-You-Rich-Second-ebook/dp/B07GNXPP4P?tag=NYTBSREV-20",
								},
								{
									name: "Apple Books",
									url: "https://goto.applebooks.apple/9781523505746?at=10lIEQ",
								},
								{
									name: "Barnes and Noble",
									url: "https://www.anrdoezrs.net/click-7990613-11819508?url=https%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9781523505746",
								},
								{
									name: "Books-A-Million",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fp%252FI%252BWILL%252BTEACH%252BYOU%252BTO%252BBE%252BRICH%25252C%252BSECOND%252BEDITION%252FRamit%252BSethi%252F9781523505746&url2=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fsearch%253Fquery%253DI%252BWILL%252BTEACH%252BYOU%252BTO%252BBE%252BRICH%25252C%252BSECOND%252BEDITION%252BRamit%252BSethi",
								},
								{
									name: "Bookshop",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fbookshop.org%2Fa%2F3546%2F9781523505746&url2=https%3A%2F%2Fbookshop.org%2Fbooks%3Faffiliate%3D3546%26keywords%3DI%2BWILL%2BTEACH%2BYOU%2BTO%2BBE%2BRICH%252C%2BSECOND%2BEDITION",
								},
								{
									name: "IndieBound",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.indiebound.org%2Fbook%2F9781523505746%3Faff%3DNYT&url2=https%3A%2F%2Fwww.indiebound.org%2Fsearch%2Fbook%3Fkeys%3DI%2BWILL%2BTEACH%2BYOU%2BTO%2BBE%2BRICH%252C%2BSECOND%2BEDITION%2BRamit%2BSethi%26aff%3DNYT",
								},
							],
						},
						{
							age_group: "",
							amazon_product_url:
								"https://www.amazon.com/dp/1538707942?tag=NYTBSREV-20",
							article_chapter_link: "",
							author: "William H. McRaven",
							book_image:
								"https://storage.googleapis.com/du-prd/books/images/9781538707944.jpg",
							book_image_width: 351,
							book_image_height: 500,
							book_review_link: "",
							contributor: "by William H. McRaven",
							contributor_note: "",
							created_date: "2023-07-05 23:06:13",
							description: "",
							first_chapter_link: "",
							price: "0.00",
							primary_isbn10: "1538707942",
							primary_isbn13: "9781538707944",
							book_uri:
								"nyt://book/4d35910f-8a4f-54f7-a63c-27748e8b5399",
							publisher: "Grand Central",
							rank: 4,
							rank_last_week: 0,
							sunday_review_link: "",
							title: "THE WISDOM OF THE BULLFROG",
							updated_date: "2023-07-05 23:06:13",
							weeks_on_list: 0,
							buy_links: [
								{
									name: "Amazon",
									url: "https://www.amazon.com/dp/1538707942?tag=NYTBSREV-20",
								},
								{
									name: "Apple Books",
									url: "https://goto.applebooks.apple/9781538707944?at=10lIEQ",
								},
								{
									name: "Barnes and Noble",
									url: "https://www.anrdoezrs.net/click-7990613-11819508?url=https%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9781538707944",
								},
								{
									name: "Books-A-Million",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fp%252FTHE%252BWISDOM%252BOF%252BTHE%252BBULLFROG%252FWilliam%252BH.%252BMcRaven%252F9781538707944&url2=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fsearch%253Fquery%253DTHE%252BWISDOM%252BOF%252BTHE%252BBULLFROG%252BWilliam%252BH.%252BMcRaven",
								},
								{
									name: "Bookshop",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fbookshop.org%2Fa%2F3546%2F9781538707944&url2=https%3A%2F%2Fbookshop.org%2Fbooks%3Faffiliate%3D3546%26keywords%3DTHE%2BWISDOM%2BOF%2BTHE%2BBULLFROG",
								},
								{
									name: "IndieBound",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.indiebound.org%2Fbook%2F9781538707944%3Faff%3DNYT&url2=https%3A%2F%2Fwww.indiebound.org%2Fsearch%2Fbook%3Fkeys%3DTHE%2BWISDOM%2BOF%2BTHE%2BBULLFROG%2BWilliam%2BH.%2BMcRaven%26aff%3DNYT",
								},
							],
						},
						{
							age_group: "",
							amazon_product_url:
								"https://www.amazon.com/Dare-Lead-Brave-Conversations-Hearts/dp/0399592520?tag=NYTBSREV-20",
							article_chapter_link: "",
							author: "Brené Brown",
							book_image:
								"https://storage.googleapis.com/du-prd/books/images/9780399592522.jpg",
							book_image_width: 326,
							book_image_height: 495,
							book_review_link: "",
							contributor: "by Brené Brown",
							contributor_note: "",
							created_date: "2023-07-05 23:06:13",
							description: "",
							first_chapter_link: "",
							price: "0.00",
							primary_isbn10: "0399592520",
							primary_isbn13: "9780399592522",
							book_uri:
								"nyt://book/e9569270-fe3a-5fe4-a068-7eca3da622e7",
							publisher: "Random House",
							rank: 5,
							rank_last_week: 0,
							sunday_review_link: "",
							title: "DARE TO LEAD",
							updated_date: "2023-07-05 23:06:13",
							weeks_on_list: 0,
							buy_links: [
								{
									name: "Amazon",
									url: "https://www.amazon.com/Dare-Lead-Brave-Conversations-Hearts/dp/0399592520?tag=NYTBSREV-20",
								},
								{
									name: "Apple Books",
									url: "https://goto.applebooks.apple/9780399592522?at=10lIEQ",
								},
								{
									name: "Barnes and Noble",
									url: "https://www.anrdoezrs.net/click-7990613-11819508?url=https%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9780399592522",
								},
								{
									name: "Books-A-Million",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fp%252FDARE%252BTO%252BLEAD%252FBren%2525C3%2525A9%252BBrown%252F9780399592522&url2=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fsearch%253Fquery%253DDARE%252BTO%252BLEAD%252BBren%2525C3%2525A9%252BBrown",
								},
								{
									name: "Bookshop",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fbookshop.org%2Fa%2F3546%2F9780399592522&url2=https%3A%2F%2Fbookshop.org%2Fbooks%3Faffiliate%3D3546%26keywords%3DDARE%2BTO%2BLEAD",
								},
								{
									name: "IndieBound",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.indiebound.org%2Fbook%2F9780399592522%3Faff%3DNYT&url2=https%3A%2F%2Fwww.indiebound.org%2Fsearch%2Fbook%3Fkeys%3DDARE%2BTO%2BLEAD%2BBren%25C3%25A9%2BBrown%26aff%3DNYT",
								},
							],
						},
					],
				},
				{
					list_id: 719,
					list_name: "Graphic Books and Manga",
					list_name_encoded: "graphic-books-and-manga",
					display_name: "Graphic Books and Manga",
					updated: "MONTHLY",
					list_image: null,
					list_image_width: null,
					list_image_height: null,
					books: [
						{
							age_group: "",
							amazon_product_url:
								"https://www.amazon.com/dp/1338801910?tag=NYTBSREV-20",
							article_chapter_link: "",
							author: "Dav Pilkey",
							book_image:
								"https://storage.googleapis.com/du-prd/books/images/9781338801910.jpg",
							book_image_width: 331,
							book_image_height: 500,
							book_review_link: "",
							contributor: "by Dav Pilkey",
							contributor_note: "",
							created_date: "2023-07-05 23:06:06",
							description:
								"The 11th book in the Dog Man series. Piggy returns and the Supa Buddies are sabotaged.",
							first_chapter_link: "",
							price: "0.00",
							primary_isbn10: "1338801910",
							primary_isbn13: "9781338801910",
							book_uri:
								"nyt://book/38aa45cc-2c25-57e9-9150-beae0acea89e",
							publisher: "Scholastic",
							rank: 1,
							rank_last_week: 0,
							sunday_review_link: "",
							title: "TWENTY THOUSAND FLEAS UNDER THE SEA",
							updated_date: "2023-07-05 23:06:06",
							weeks_on_list: 0,
							buy_links: [
								{
									name: "Amazon",
									url: "https://www.amazon.com/dp/1338801910?tag=NYTBSREV-20",
								},
								{
									name: "Apple Books",
									url: "https://goto.applebooks.apple/9781338801910?at=10lIEQ",
								},
								{
									name: "Barnes and Noble",
									url: "https://www.anrdoezrs.net/click-7990613-11819508?url=https%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9781338801910",
								},
								{
									name: "Books-A-Million",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fp%252FTWENTY%252BTHOUSAND%252BFLEAS%252BUNDER%252BTHE%252BSEA%252FDav%252BPilkey%252F9781338801910&url2=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fsearch%253Fquery%253DTWENTY%252BTHOUSAND%252BFLEAS%252BUNDER%252BTHE%252BSEA%252BDav%252BPilkey",
								},
								{
									name: "Bookshop",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fbookshop.org%2Fa%2F3546%2F9781338801910&url2=https%3A%2F%2Fbookshop.org%2Fbooks%3Faffiliate%3D3546%26keywords%3DTWENTY%2BTHOUSAND%2BFLEAS%2BUNDER%2BTHE%2BSEA",
								},
								{
									name: "IndieBound",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.indiebound.org%2Fbook%2F9781338801910%3Faff%3DNYT&url2=https%3A%2F%2Fwww.indiebound.org%2Fsearch%2Fbook%3Fkeys%3DTWENTY%2BTHOUSAND%2BFLEAS%2BUNDER%2BTHE%2BSEA%2BDav%2BPilkey%26aff%3DNYT",
								},
							],
						},
						{
							age_group: "",
							amazon_product_url:
								"https://www.amazon.com/dp/0593599047?tag=NYTBSREV-20",
							article_chapter_link: "",
							author: "Rachel Smythe",
							book_image:
								"https://storage.googleapis.com/du-prd/books/images/9780593599044.jpg",
							book_image_width: 389,
							book_image_height: 500,
							book_review_link: "",
							contributor: "by Rachel Smythe",
							contributor_note: "",
							created_date: "2023-07-05 23:06:06",
							description:
								"As gossip about them swirls, Persephone and Hades choose to take a break to focus on their own issues.",
							first_chapter_link: "",
							price: "0.00",
							primary_isbn10: "0593599047",
							primary_isbn13: "9780593599044",
							book_uri:
								"nyt://book/30ca1735-594d-5fb6-a065-cb9306f2071a",
							publisher: "Inklore",
							rank: 2,
							rank_last_week: 0,
							sunday_review_link: "",
							title: "LORE OLYMPUS, VOL. 4",
							updated_date: "2023-07-05 23:06:06",
							weeks_on_list: 0,
							buy_links: [
								{
									name: "Amazon",
									url: "https://www.amazon.com/dp/0593599047?tag=NYTBSREV-20",
								},
								{
									name: "Apple Books",
									url: "https://goto.applebooks.apple/9780593599044?at=10lIEQ",
								},
								{
									name: "Barnes and Noble",
									url: "https://www.anrdoezrs.net/click-7990613-11819508?url=https%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9780593599044",
								},
								{
									name: "Books-A-Million",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fp%252FLORE%252BOLYMPUS%25252C%252BVOL.%252B4%252FRachel%252BSmythe%252F9780593599044&url2=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fsearch%253Fquery%253DLORE%252BOLYMPUS%25252C%252BVOL.%252B4%252BRachel%252BSmythe",
								},
								{
									name: "Bookshop",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fbookshop.org%2Fa%2F3546%2F9780593599044&url2=https%3A%2F%2Fbookshop.org%2Fbooks%3Faffiliate%3D3546%26keywords%3DLORE%2BOLYMPUS%252C%2BVOL.%2B4",
								},
								{
									name: "IndieBound",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.indiebound.org%2Fbook%2F9780593599044%3Faff%3DNYT&url2=https%3A%2F%2Fwww.indiebound.org%2Fsearch%2Fbook%3Fkeys%3DLORE%2BOLYMPUS%252C%2BVOL.%2B4%2BRachel%2BSmythe%26aff%3DNYT",
								},
							],
						},
						{
							age_group: "",
							amazon_product_url:
								"https://www.amazon.com/dp/0063260506?tag=NYTBSREV-20",
							article_chapter_link: "",
							author: "FGTeeV.",
							book_image:
								"https://storage.googleapis.com/du-prd/books/images/9780063260504.jpg",
							book_image_width: 387,
							book_image_height: 500,
							book_review_link: "",
							contributor:
								"by FGTeeV. Illustrated by Miguel Díaz Rivas",
							contributor_note:
								"Illustrated by Miguel Díaz Rivas",
							created_date: "2023-07-05 23:06:06",
							description:
								"The FGTeeV family encounter a T-Rex, a knight and angry space invaders.",
							first_chapter_link: "",
							price: "0.00",
							primary_isbn10: "0063260506",
							primary_isbn13: "9780063260504",
							book_uri:
								"nyt://book/72d9402f-c6f0-505b-be12-c74c7b8646f5",
							publisher: "HarperAlley",
							rank: 3,
							rank_last_week: 0,
							sunday_review_link: "",
							title: "FGTEEV: OUT OF TIME!",
							updated_date: "2023-07-05 23:06:06",
							weeks_on_list: 0,
							buy_links: [
								{
									name: "Amazon",
									url: "https://www.amazon.com/dp/0063260506?tag=NYTBSREV-20",
								},
								{
									name: "Apple Books",
									url: "https://goto.applebooks.apple/9780063260504?at=10lIEQ",
								},
								{
									name: "Barnes and Noble",
									url: "https://www.anrdoezrs.net/click-7990613-11819508?url=https%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9780063260504",
								},
								{
									name: "Books-A-Million",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fp%252FFGTEEV%25253A%252BOUT%252BOF%252BTIME%252521%252FFGTeeV.%252F9780063260504&url2=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fsearch%253Fquery%253DFGTEEV%25253A%252BOUT%252BOF%252BTIME%252521%252BFGTeeV.",
								},
								{
									name: "Bookshop",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fbookshop.org%2Fa%2F3546%2F9780063260504&url2=https%3A%2F%2Fbookshop.org%2Fbooks%3Faffiliate%3D3546%26keywords%3DFGTEEV%253A%2BOUT%2BOF%2BTIME%2521",
								},
								{
									name: "IndieBound",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.indiebound.org%2Fbook%2F9780063260504%3Faff%3DNYT&url2=https%3A%2F%2Fwww.indiebound.org%2Fsearch%2Fbook%3Fkeys%3DFGTEEV%253A%2BOUT%2BOF%2BTIME%2521%2BFGTeeV.%26aff%3DNYT",
								},
							],
						},
						{
							age_group: "",
							amazon_product_url:
								"https://www.amazon.com/dp/1338846620?tag=NYTBSREV-20",
							article_chapter_link: "",
							author: "Dav Pilkey",
							book_image:
								"https://storage.googleapis.com/du-prd/books/images/9781338846621.jpg",
							book_image_width: 338,
							book_image_height: 500,
							book_review_link: "",
							contributor: "by Dav Pilkey",
							contributor_note: "",
							created_date: "2023-07-05 23:06:06",
							description:
								"The fourth book in the Cat Kid Comic Club series. After doing their chores, baby frogs work together to create more mini-comics.",
							first_chapter_link: "",
							price: "0.00",
							primary_isbn10: "1338846620",
							primary_isbn13: "9781338846621",
							book_uri:
								"nyt://book/82b8b5ef-13af-5d86-980b-cd84e69bffe1",
							publisher: "Scholastic",
							rank: 4,
							rank_last_week: 0,
							sunday_review_link: "",
							title: "COLLABORATIONS",
							updated_date: "2023-07-05 23:06:06",
							weeks_on_list: 0,
							buy_links: [
								{
									name: "Amazon",
									url: "https://www.amazon.com/dp/1338846620?tag=NYTBSREV-20",
								},
								{
									name: "Apple Books",
									url: "https://goto.applebooks.apple/9781338846621?at=10lIEQ",
								},
								{
									name: "Barnes and Noble",
									url: "https://www.anrdoezrs.net/click-7990613-11819508?url=https%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9781338846621",
								},
								{
									name: "Books-A-Million",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fp%252FCOLLABORATIONS%252FDav%252BPilkey%252F9781338846621&url2=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fsearch%253Fquery%253DCOLLABORATIONS%252BDav%252BPilkey",
								},
								{
									name: "Bookshop",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fbookshop.org%2Fa%2F3546%2F9781338846621&url2=https%3A%2F%2Fbookshop.org%2Fbooks%3Faffiliate%3D3546%26keywords%3DCOLLABORATIONS",
								},
								{
									name: "IndieBound",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.indiebound.org%2Fbook%2F9781338846621%3Faff%3DNYT&url2=https%3A%2F%2Fwww.indiebound.org%2Fsearch%2Fbook%3Fkeys%3DCOLLABORATIONS%2BDav%2BPilkey%26aff%3DNYT",
								},
							],
						},
						{
							age_group: "",
							amazon_product_url:
								"https://www.amazon.com/dp/1338616102?tag=NYTBSREV-20",
							article_chapter_link: "",
							author: "Ann M. Martin.",
							book_image:
								"https://storage.googleapis.com/du-prd/books/images/9781338616101.jpg",
							book_image_width: 344,
							book_image_height: 500,
							book_review_link: "",
							contributor:
								"by Ann M. Martin. Illustrated by Cynthia Yuan Cheng",
							contributor_note:
								"Illustrated by Cynthia Yuan Cheng",
							created_date: "2023-07-05 23:06:06",
							description:
								"The 13th book in the Baby-sitters Club graphic novel series. Mary Anne receives a bad-luck charm in the mail with instructions to wear it.",
							first_chapter_link: "",
							price: "0.00",
							primary_isbn10: "1338616102",
							primary_isbn13: "9781338616101",
							book_uri:
								"nyt://book/3c8b48b0-ff27-5880-bed1-31f146d58c82",
							publisher: "Scholastic",
							rank: 5,
							rank_last_week: 0,
							sunday_review_link: "",
							title: "MARY ANNE'S BAD LUCK MYSTERY",
							updated_date: "2023-07-05 23:06:06",
							weeks_on_list: 0,
							buy_links: [
								{
									name: "Amazon",
									url: "https://www.amazon.com/dp/1338616102?tag=NYTBSREV-20",
								},
								{
									name: "Apple Books",
									url: "https://goto.applebooks.apple/9781338616101?at=10lIEQ",
								},
								{
									name: "Barnes and Noble",
									url: "https://www.anrdoezrs.net/click-7990613-11819508?url=https%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9781338616101",
								},
								{
									name: "Books-A-Million",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fp%252FMARY%252BANNE%252527S%252BBAD%252BLUCK%252BMYSTERY%252FAnn%252BM.%252BMartin.%252F9781338616101&url2=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fsearch%253Fquery%253DMARY%252BANNE%252527S%252BBAD%252BLUCK%252BMYSTERY%252BAnn%252BM.%252BMartin.",
								},
								{
									name: "Bookshop",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fbookshop.org%2Fa%2F3546%2F9781338616101&url2=https%3A%2F%2Fbookshop.org%2Fbooks%3Faffiliate%3D3546%26keywords%3DMARY%2BANNE%2527S%2BBAD%2BLUCK%2BMYSTERY",
								},
								{
									name: "IndieBound",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.indiebound.org%2Fbook%2F9781338616101%3Faff%3DNYT&url2=https%3A%2F%2Fwww.indiebound.org%2Fsearch%2Fbook%3Fkeys%3DMARY%2BANNE%2527S%2BBAD%2BLUCK%2BMYSTERY%2BAnn%2BM.%2BMartin.%26aff%3DNYT",
								},
							],
						},
					],
				},
				{
					list_id: 10018,
					list_name: "Mass Market Monthly",
					list_name_encoded: "mass-market-monthly",
					display_name: "Mass Market",
					updated: "MONTHLY",
					list_image: null,
					list_image_width: null,
					list_image_height: null,
					books: [
						{
							age_group: "",
							amazon_product_url:
								"https://www.amazon.com/dp/0385549326?tag=NYTBSREV-20",
							article_chapter_link: "",
							author: "John Grisham",
							book_image:
								"https://storage.googleapis.com/du-prd/books/images/9780385549325.jpg",
							book_image_width: 329,
							book_image_height: 500,
							book_review_link: "",
							contributor: "by John Grisham",
							contributor_note: "",
							created_date: "2023-07-05 23:06:16",
							description:
								"Three novellas: “Homecoming,” “Strawberry Moon” and “Sparring Partners.”",
							first_chapter_link: "",
							price: "0.00",
							primary_isbn10: "0593470907",
							primary_isbn13: "9780593470909",
							book_uri:
								"nyt://book/1d13b23f-da08-5fa6-992b-7108b3968bef",
							publisher: "Vintage",
							rank: 1,
							rank_last_week: 0,
							sunday_review_link: "",
							title: "SPARRING PARTNERS",
							updated_date: "2023-07-05 23:06:16",
							weeks_on_list: 0,
							buy_links: [
								{
									name: "Amazon",
									url: "https://www.amazon.com/dp/0385549326?tag=NYTBSREV-20",
								},
								{
									name: "Apple Books",
									url: "https://goto.applebooks.apple/9780593470909?at=10lIEQ",
								},
								{
									name: "Barnes and Noble",
									url: "https://www.anrdoezrs.net/click-7990613-11819508?url=https%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9780593470909",
								},
								{
									name: "Books-A-Million",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fp%252FSPARRING%252BPARTNERS%252FJohn%252BGrisham%252F9780593470909&url2=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fsearch%253Fquery%253DSPARRING%252BPARTNERS%252BJohn%252BGrisham",
								},
								{
									name: "Bookshop",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fbookshop.org%2Fa%2F3546%2F9780593470909&url2=https%3A%2F%2Fbookshop.org%2Fbooks%3Faffiliate%3D3546%26keywords%3DSPARRING%2BPARTNERS",
								},
								{
									name: "IndieBound",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.indiebound.org%2Fbook%2F9780593470909%3Faff%3DNYT&url2=https%3A%2F%2Fwww.indiebound.org%2Fsearch%2Fbook%3Fkeys%3DSPARRING%2BPARTNERS%2BJohn%2BGrisham%26aff%3DNYT",
								},
							],
						},
						{
							age_group: "",
							amazon_product_url:
								"https://www.amazon.com/dp/198482161X?tag=NYTBSREV-20",
							article_chapter_link: "",
							author: "Danielle Steel",
							book_image:
								"https://storage.googleapis.com/du-prd/books/images/9781984821614.jpg",
							book_image_width: 330,
							book_image_height: 500,
							book_review_link: "",
							contributor: "by Danielle Steel",
							contributor_note: "",
							created_date: "2023-07-05 23:06:16",
							description:
								"Parents of children who have gone missing on a dangerous peak in Montana form a search-and-rescue mission.",
							first_chapter_link: "",
							price: "0.00",
							primary_isbn10: "1984821636",
							primary_isbn13: "9781984821638",
							book_uri:
								"nyt://book/19171670-8823-526e-bb46-8cca2339962f",
							publisher: "Dell",
							rank: 2,
							rank_last_week: 0,
							sunday_review_link: "",
							title: "THE CHALLENGE",
							updated_date: "2023-07-05 23:06:16",
							weeks_on_list: 0,
							buy_links: [
								{
									name: "Amazon",
									url: "https://www.amazon.com/dp/198482161X?tag=NYTBSREV-20",
								},
								{
									name: "Apple Books",
									url: "https://goto.applebooks.apple/9781984821638?at=10lIEQ",
								},
								{
									name: "Barnes and Noble",
									url: "https://www.anrdoezrs.net/click-7990613-11819508?url=https%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9781984821638",
								},
								{
									name: "Books-A-Million",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fp%252FTHE%252BCHALLENGE%252FDanielle%252BSteel%252F9781984821638&url2=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fsearch%253Fquery%253DTHE%252BCHALLENGE%252BDanielle%252BSteel",
								},
								{
									name: "Bookshop",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fbookshop.org%2Fa%2F3546%2F9781984821638&url2=https%3A%2F%2Fbookshop.org%2Fbooks%3Faffiliate%3D3546%26keywords%3DTHE%2BCHALLENGE",
								},
								{
									name: "IndieBound",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.indiebound.org%2Fbook%2F9781984821638%3Faff%3DNYT&url2=https%3A%2F%2Fwww.indiebound.org%2Fsearch%2Fbook%3Fkeys%3DTHE%2BCHALLENGE%2BDanielle%2BSteel%26aff%3DNYT",
								},
							],
						},
						{
							age_group: "",
							amazon_product_url:
								"https://www.amazon.com/dp/1250220254?tag=NYTBSREV-20",
							article_chapter_link: "",
							author: "Liane Moriarty",
							book_image:
								"https://storage.googleapis.com/du-prd/books/images/9781250220257.jpg",
							book_image_width: 329,
							book_image_height: 500,
							book_review_link: "",
							contributor: "by Liane Moriarty",
							contributor_note: "",
							created_date: "2023-07-05 23:06:16",
							description:
								"The Delaney siblings suspect their father of causing the disappearance of their mother.",
							first_chapter_link: "",
							price: "0.00",
							primary_isbn10: "1250894220",
							primary_isbn13: "9781250894229",
							book_uri:
								"nyt://book/c2496368-e2ff-5420-a60f-e246ed19ddb5",
							publisher: "Holt",
							rank: 3,
							rank_last_week: 0,
							sunday_review_link: "",
							title: "APPLES NEVER FALL",
							updated_date: "2023-07-05 23:06:16",
							weeks_on_list: 0,
							buy_links: [
								{
									name: "Amazon",
									url: "https://www.amazon.com/dp/1250220254?tag=NYTBSREV-20",
								},
								{
									name: "Apple Books",
									url: "https://goto.applebooks.apple/9781250894229?at=10lIEQ",
								},
								{
									name: "Barnes and Noble",
									url: "https://www.anrdoezrs.net/click-7990613-11819508?url=https%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9781250894229",
								},
								{
									name: "Books-A-Million",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fp%252FAPPLES%252BNEVER%252BFALL%252FLiane%252BMoriarty%252F9781250894229&url2=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fsearch%253Fquery%253DAPPLES%252BNEVER%252BFALL%252BLiane%252BMoriarty",
								},
								{
									name: "Bookshop",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fbookshop.org%2Fa%2F3546%2F9781250894229&url2=https%3A%2F%2Fbookshop.org%2Fbooks%3Faffiliate%3D3546%26keywords%3DAPPLES%2BNEVER%2BFALL",
								},
								{
									name: "IndieBound",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.indiebound.org%2Fbook%2F9781250894229%3Faff%3DNYT&url2=https%3A%2F%2Fwww.indiebound.org%2Fsearch%2Fbook%3Fkeys%3DAPPLES%2BNEVER%2BFALL%2BLiane%2BMoriarty%26aff%3DNYT",
								},
							],
						},
						{
							age_group: "",
							amazon_product_url:
								"https://www.amazon.com/dp/1250890071?tag=NYTBSREV-20",
							article_chapter_link: "",
							author: "Nora Roberts",
							book_image:
								"https://storage.googleapis.com/du-prd/books/images/9781250890078.jpg",
							book_image_width: 339,
							book_image_height: 500,
							book_review_link: "",
							contributor: "by Nora Roberts",
							contributor_note: "",
							created_date: "2023-07-05 23:06:16",
							description:
								"Two thrillers: “The Art of Deception” and “Risky Business.”",
							first_chapter_link: "",
							price: "0.00",
							primary_isbn10: "1250890071",
							primary_isbn13: "9781250890078",
							book_uri:
								"nyt://book/a4906c3b-1e9c-5855-9abb-7ea8aff4b7cb",
							publisher: "St. Martin's",
							rank: 4,
							rank_last_week: 0,
							sunday_review_link: "",
							title: "DANGER ZONE",
							updated_date: "2023-07-05 23:06:16",
							weeks_on_list: 0,
							buy_links: [
								{
									name: "Amazon",
									url: "https://www.amazon.com/dp/1250890071?tag=NYTBSREV-20",
								},
								{
									name: "Apple Books",
									url: "https://goto.applebooks.apple/9781250890078?at=10lIEQ",
								},
								{
									name: "Barnes and Noble",
									url: "https://www.anrdoezrs.net/click-7990613-11819508?url=https%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9781250890078",
								},
								{
									name: "Books-A-Million",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fp%252FDANGER%252BZONE%252FNora%252BRoberts%252F9781250890078&url2=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fsearch%253Fquery%253DDANGER%252BZONE%252BNora%252BRoberts",
								},
								{
									name: "Bookshop",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fbookshop.org%2Fa%2F3546%2F9781250890078&url2=https%3A%2F%2Fbookshop.org%2Fbooks%3Faffiliate%3D3546%26keywords%3DDANGER%2BZONE",
								},
								{
									name: "IndieBound",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.indiebound.org%2Fbook%2F9781250890078%3Faff%3DNYT&url2=https%3A%2F%2Fwww.indiebound.org%2Fsearch%2Fbook%3Fkeys%3DDANGER%2BZONE%2BNora%2BRoberts%26aff%3DNYT",
								},
							],
						},
						{
							age_group: "",
							amazon_product_url:
								"http://www.amazon.com/Devious-Rick-Bentz-Reuben-Montoya/dp/1420102753?tag=NYTBSREV-20",
							article_chapter_link: "",
							author: "Lisa Jackson",
							book_image:
								"https://storage.googleapis.com/du-prd/books/images/9780758225658.jpg",
							book_image_width: 261,
							book_image_height: 400,
							book_review_link: "",
							contributor: "by Lisa Jackson",
							contributor_note: "",
							created_date: "2023-07-05 23:06:16",
							description:
								"Called to investigate a murder, the New Orleans detective Reuben Montoya discovers that the victim — found on a cathedral altar in a yellowed bridal gown — is an old high school friend.",
							first_chapter_link: "",
							price: "0.00",
							primary_isbn10: "1420155008",
							primary_isbn13: "9781420155006",
							book_uri:
								"nyt://book/ed212151-6693-5058-a421-a1ea23c8531c",
							publisher: "Zebra/Kensington",
							rank: 5,
							rank_last_week: 0,
							sunday_review_link: "",
							title: "DEVIOUS",
							updated_date: "2023-07-05 23:06:16",
							weeks_on_list: 0,
							buy_links: [
								{
									name: "Amazon",
									url: "http://www.amazon.com/Devious-Rick-Bentz-Reuben-Montoya/dp/1420102753?tag=NYTBSREV-20",
								},
								{
									name: "Apple Books",
									url: "https://goto.applebooks.apple/9781420155006?at=10lIEQ",
								},
								{
									name: "Barnes and Noble",
									url: "https://www.anrdoezrs.net/click-7990613-11819508?url=https%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9781420155006",
								},
								{
									name: "Books-A-Million",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fp%252FDEVIOUS%252FLisa%252BJackson%252F9781420155006&url2=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fsearch%253Fquery%253DDEVIOUS%252BLisa%252BJackson",
								},
								{
									name: "Bookshop",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fbookshop.org%2Fa%2F3546%2F9781420155006&url2=https%3A%2F%2Fbookshop.org%2Fbooks%3Faffiliate%3D3546%26keywords%3DDEVIOUS",
								},
								{
									name: "IndieBound",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.indiebound.org%2Fbook%2F9781420155006%3Faff%3DNYT&url2=https%3A%2F%2Fwww.indiebound.org%2Fsearch%2Fbook%3Fkeys%3DDEVIOUS%2BLisa%2BJackson%26aff%3DNYT",
								},
							],
						},
					],
				},
				{
					list_id: 10015,
					list_name: "Middle Grade Paperback Monthly",
					list_name_encoded: "middle-grade-paperback-monthly",
					display_name: "Middle Grade Paperback",
					updated: "MONTHLY",
					list_image: null,
					list_image_width: null,
					list_image_height: null,
					books: [
						{
							age_group: "",
							amazon_product_url:
								"http://www.amazon.com/Out-My-Mind-Sharon-Draper/dp/1416971718?tag=NYTBSREV-20",
							article_chapter_link: "",
							author: "Sharon M. Draper",
							book_image:
								"https://storage.googleapis.com/du-prd/books/images/9781416971719.jpg",
							book_image_width: 330,
							book_image_height: 491,
							book_review_link: "",
							contributor: "by Sharon M. Draper",
							contributor_note: "",
							created_date: "2023-07-05 23:06:01",
							description: "",
							first_chapter_link: "",
							price: "0.00",
							primary_isbn10: "1416971718",
							primary_isbn13: "9781416971719",
							book_uri:
								"nyt://book/82d77c01-610e-563e-8f14-073084cd4a58",
							publisher: "Atheneum",
							rank: 1,
							rank_last_week: 0,
							sunday_review_link: "",
							title: "OUT OF MY MIND",
							updated_date: "2023-07-05 23:06:01",
							weeks_on_list: 0,
							buy_links: [
								{
									name: "Amazon",
									url: "http://www.amazon.com/Out-My-Mind-Sharon-Draper/dp/1416971718?tag=NYTBSREV-20",
								},
								{
									name: "Apple Books",
									url: "https://goto.applebooks.apple/9781416971719?at=10lIEQ",
								},
								{
									name: "Barnes and Noble",
									url: "https://www.anrdoezrs.net/click-7990613-11819508?url=https%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9781416971719",
								},
								{
									name: "Books-A-Million",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fp%252FOUT%252BOF%252BMY%252BMIND%252FSharon%252BM.%252BDraper%252F9781416971719&url2=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fsearch%253Fquery%253DOUT%252BOF%252BMY%252BMIND%252BSharon%252BM.%252BDraper",
								},
								{
									name: "Bookshop",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fbookshop.org%2Fa%2F3546%2F9781416971719&url2=https%3A%2F%2Fbookshop.org%2Fbooks%3Faffiliate%3D3546%26keywords%3DOUT%2BOF%2BMY%2BMIND",
								},
								{
									name: "IndieBound",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.indiebound.org%2Fbook%2F9781416971719%3Faff%3DNYT&url2=https%3A%2F%2Fwww.indiebound.org%2Fsearch%2Fbook%3Fkeys%3DOUT%2BOF%2BMY%2BMIND%2BSharon%2BM.%2BDraper%26aff%3DNYT",
								},
							],
						},
						{
							age_group: "",
							amazon_product_url:
								"https://www.amazon.com/Restart-Gordon-Korman/dp/1338053809?tag=NYTBSREV-20",
							article_chapter_link: "",
							author: "Gordon Korman",
							book_image:
								"https://storage.googleapis.com/du-prd/books/images/9781338053807.jpg",
							book_image_width: 330,
							book_image_height: 480,
							book_review_link: "",
							contributor: "by Gordon Korman",
							contributor_note: "",
							created_date: "2023-07-05 23:06:01",
							description: "",
							first_chapter_link: "",
							price: "0.00",
							primary_isbn10: "1338053809",
							primary_isbn13: "9781338053807",
							book_uri:
								"nyt://book/33e0a0a4-8158-58ef-9e88-1c49b4167580",
							publisher: "Scholastic",
							rank: 2,
							rank_last_week: 0,
							sunday_review_link: "",
							title: "RESTART",
							updated_date: "2023-07-05 23:06:01",
							weeks_on_list: 0,
							buy_links: [
								{
									name: "Amazon",
									url: "https://www.amazon.com/Restart-Gordon-Korman/dp/1338053809?tag=NYTBSREV-20",
								},
								{
									name: "Apple Books",
									url: "https://goto.applebooks.apple/9781338053807?at=10lIEQ",
								},
								{
									name: "Barnes and Noble",
									url: "https://www.anrdoezrs.net/click-7990613-11819508?url=https%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9781338053807",
								},
								{
									name: "Books-A-Million",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fp%252FRESTART%252FGordon%252BKorman%252F9781338053807&url2=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fsearch%253Fquery%253DRESTART%252BGordon%252BKorman",
								},
								{
									name: "Bookshop",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fbookshop.org%2Fa%2F3546%2F9781338053807&url2=https%3A%2F%2Fbookshop.org%2Fbooks%3Faffiliate%3D3546%26keywords%3DRESTART",
								},
								{
									name: "IndieBound",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.indiebound.org%2Fbook%2F9781338053807%3Faff%3DNYT&url2=https%3A%2F%2Fwww.indiebound.org%2Fsearch%2Fbook%3Fkeys%3DRESTART%2BGordon%2BKorman%26aff%3DNYT",
								},
							],
						},
						{
							age_group: "",
							amazon_product_url:
								"http://www.amazon.com/The-Wild-Robot-Peter-Brown/dp/0316381993?tag=NYTBSREV-20",
							article_chapter_link: "",
							author: "Peter Brown",
							book_image:
								"https://storage.googleapis.com/du-prd/books/images/9780316381994.jpg",
							book_image_width: 330,
							book_image_height: 491,
							book_review_link:
								"https://www.nytimes.com/2016/04/10/books/review/the-wild-robot-by-peter-brown.html",
							contributor: "by Peter Brown",
							contributor_note: "",
							created_date: "2023-07-05 23:06:02",
							description: "",
							first_chapter_link: "",
							price: "0.00",
							primary_isbn10: "0316382000",
							primary_isbn13: "9780316382007",
							book_uri:
								"nyt://book/58650411-6a43-5082-89a6-9e2c62832d2f",
							publisher: "Little, Brown",
							rank: 3,
							rank_last_week: 0,
							sunday_review_link: "",
							title: "THE WILD ROBOT",
							updated_date: "2023-07-05 23:06:02",
							weeks_on_list: 0,
							buy_links: [
								{
									name: "Amazon",
									url: "http://www.amazon.com/The-Wild-Robot-Peter-Brown/dp/0316381993?tag=NYTBSREV-20",
								},
								{
									name: "Apple Books",
									url: "https://goto.applebooks.apple/9780316382007?at=10lIEQ",
								},
								{
									name: "Barnes and Noble",
									url: "https://www.anrdoezrs.net/click-7990613-11819508?url=https%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9780316382007",
								},
								{
									name: "Books-A-Million",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fp%252FTHE%252BWILD%252BROBOT%252FPeter%252BBrown%252F9780316382007&url2=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fsearch%253Fquery%253DTHE%252BWILD%252BROBOT%252BPeter%252BBrown",
								},
								{
									name: "Bookshop",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fbookshop.org%2Fa%2F3546%2F9780316382007&url2=https%3A%2F%2Fbookshop.org%2Fbooks%3Faffiliate%3D3546%26keywords%3DTHE%2BWILD%2BROBOT",
								},
								{
									name: "IndieBound",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.indiebound.org%2Fbook%2F9780316382007%3Faff%3DNYT&url2=https%3A%2F%2Fwww.indiebound.org%2Fsearch%2Fbook%3Fkeys%3DTHE%2BWILD%2BROBOT%2BPeter%2BBrown%26aff%3DNYT",
								},
							],
						},
						{
							age_group: "",
							amazon_product_url:
								"http://www.amazon.com/Long-Walk-Water-Based-Story/dp/0547577311?tag=NYTBSREV-20",
							article_chapter_link: "",
							author: "Linda Sue Park",
							book_image:
								"https://storage.googleapis.com/du-prd/books/images/9780547577319.jpg",
							book_image_width: 328,
							book_image_height: 495,
							book_review_link: "",
							contributor: "by Linda Sue Park",
							contributor_note: "",
							created_date: "2023-07-05 23:06:02",
							description: "",
							first_chapter_link: "",
							price: "0.00",
							primary_isbn10: "0547577311",
							primary_isbn13: "9780547577319",
							book_uri:
								"nyt://book/72728168-095d-521b-90d2-2380bdc74220",
							publisher: "Clarion",
							rank: 4,
							rank_last_week: 0,
							sunday_review_link: "",
							title: "A LONG WALK TO WATER",
							updated_date: "2023-07-05 23:06:02",
							weeks_on_list: 0,
							buy_links: [
								{
									name: "Amazon",
									url: "http://www.amazon.com/Long-Walk-Water-Based-Story/dp/0547577311?tag=NYTBSREV-20",
								},
								{
									name: "Apple Books",
									url: "https://goto.applebooks.apple/9780547577319?at=10lIEQ",
								},
								{
									name: "Barnes and Noble",
									url: "https://www.anrdoezrs.net/click-7990613-11819508?url=https%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9780547577319",
								},
								{
									name: "Books-A-Million",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fp%252FA%252BLONG%252BWALK%252BTO%252BWATER%252FLinda%252BSue%252BPark%252F9780547577319&url2=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fsearch%253Fquery%253DA%252BLONG%252BWALK%252BTO%252BWATER%252BLinda%252BSue%252BPark",
								},
								{
									name: "Bookshop",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fbookshop.org%2Fa%2F3546%2F9780547577319&url2=https%3A%2F%2Fbookshop.org%2Fbooks%3Faffiliate%3D3546%26keywords%3DA%2BLONG%2BWALK%2BTO%2BWATER",
								},
								{
									name: "IndieBound",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.indiebound.org%2Fbook%2F9780547577319%3Faff%3DNYT&url2=https%3A%2F%2Fwww.indiebound.org%2Fsearch%2Fbook%3Fkeys%3DA%2BLONG%2BWALK%2BTO%2BWATER%2BLinda%2BSue%2BPark%26aff%3DNYT",
								},
							],
						},
						{
							age_group: "",
							amazon_product_url:
								"https://www.amazon.com/Wish-Barbara-OConnor/dp/1250144051?tag=NYTBSREV-20",
							article_chapter_link: "",
							author: "Barbara O'Connor",
							book_image:
								"https://storage.googleapis.com/du-prd/books/images/9781250144058.jpg",
							book_image_width: 330,
							book_image_height: 485,
							book_review_link: "",
							contributor: "by Barbara O'Connor",
							contributor_note: "",
							created_date: "2023-07-05 23:06:02",
							description: "",
							first_chapter_link: "",
							price: "0.00",
							primary_isbn10: "1250144051",
							primary_isbn13: "9781250144058",
							book_uri:
								"nyt://book/46604242-8624-57d1-bdd4-424c21cde273",
							publisher: "Square Fish",
							rank: 5,
							rank_last_week: 0,
							sunday_review_link: "",
							title: "WISH",
							updated_date: "2023-07-05 23:06:02",
							weeks_on_list: 0,
							buy_links: [
								{
									name: "Amazon",
									url: "https://www.amazon.com/Wish-Barbara-OConnor/dp/1250144051?tag=NYTBSREV-20",
								},
								{
									name: "Apple Books",
									url: "https://goto.applebooks.apple/9781250144058?at=10lIEQ",
								},
								{
									name: "Barnes and Noble",
									url: "https://www.anrdoezrs.net/click-7990613-11819508?url=https%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9781250144058",
								},
								{
									name: "Books-A-Million",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fp%252FWISH%252FBarbara%252BO%252527Connor%252F9781250144058&url2=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fsearch%253Fquery%253DWISH%252BBarbara%252BO%252527Connor",
								},
								{
									name: "Bookshop",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fbookshop.org%2Fa%2F3546%2F9781250144058&url2=https%3A%2F%2Fbookshop.org%2Fbooks%3Faffiliate%3D3546%26keywords%3DWISH",
								},
								{
									name: "IndieBound",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.indiebound.org%2Fbook%2F9781250144058%3Faff%3DNYT&url2=https%3A%2F%2Fwww.indiebound.org%2Fsearch%2Fbook%3Fkeys%3DWISH%2BBarbara%2BO%2527Connor%26aff%3DNYT",
								},
							],
						},
					],
				},
				{
					list_id: 10016,
					list_name: "Young Adult Paperback Monthly",
					list_name_encoded: "young-adult-paperback-monthly",
					display_name: "Young Adult Paperback",
					updated: "MONTHLY",
					list_image: null,
					list_image_width: null,
					list_image_height: null,
					books: [
						{
							age_group: "",
							amazon_product_url:
								"http://www.amazon.com/If-He-Had-Been-Me-ebook/dp/B00APIVOIE?tag=NYTBSREV-20",
							article_chapter_link: "",
							author: "Laura Nowlin",
							book_image:
								"https://storage.googleapis.com/du-prd/books/images/9781402277832.jpg",
							book_image_width: 330,
							book_image_height: 495,
							book_review_link: "",
							contributor: "by Laura Nowlin",
							contributor_note: "",
							created_date: "2023-07-05 23:06:08",
							description: "",
							first_chapter_link: "",
							price: "0.00",
							primary_isbn10: "1728205484",
							primary_isbn13: "9781728205489",
							book_uri:
								"nyt://book/06b8daaf-e835-5d8a-b7fc-fc6ad002bef2",
							publisher: "Sourcebooks Fire",
							rank: 1,
							rank_last_week: 0,
							sunday_review_link: "",
							title: "IF HE HAD BEEN WITH ME",
							updated_date: "2023-07-05 23:06:08",
							weeks_on_list: 0,
							buy_links: [
								{
									name: "Amazon",
									url: "http://www.amazon.com/If-He-Had-Been-Me-ebook/dp/B00APIVOIE?tag=NYTBSREV-20",
								},
								{
									name: "Apple Books",
									url: "https://goto.applebooks.apple/9781728205489?at=10lIEQ",
								},
								{
									name: "Barnes and Noble",
									url: "https://www.anrdoezrs.net/click-7990613-11819508?url=https%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9781728205489",
								},
								{
									name: "Books-A-Million",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fp%252FIF%252BHE%252BHAD%252BBEEN%252BWITH%252BME%252FLaura%252BNowlin%252F9781728205489&url2=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fsearch%253Fquery%253DIF%252BHE%252BHAD%252BBEEN%252BWITH%252BME%252BLaura%252BNowlin",
								},
								{
									name: "Bookshop",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fbookshop.org%2Fa%2F3546%2F9781728205489&url2=https%3A%2F%2Fbookshop.org%2Fbooks%3Faffiliate%3D3546%26keywords%3DIF%2BHE%2BHAD%2BBEEN%2BWITH%2BME",
								},
								{
									name: "IndieBound",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.indiebound.org%2Fbook%2F9781728205489%3Faff%3DNYT&url2=https%3A%2F%2Fwww.indiebound.org%2Fsearch%2Fbook%3Fkeys%3DIF%2BHE%2BHAD%2BBEEN%2BWITH%2BME%2BLaura%2BNowlin%26aff%3DNYT",
								},
							],
						},
						{
							age_group: "",
							amazon_product_url:
								"https://www.amazon.com/dp/1728210291?tag=NYTBSREV-20",
							article_chapter_link: "",
							author: "K.L. Walther",
							book_image:
								"https://storage.googleapis.com/du-prd/books/images/9781728210292.jpg",
							book_image_width: 333,
							book_image_height: 500,
							book_review_link: "",
							contributor: "by K.L. Walther",
							contributor_note: "",
							created_date: "2023-07-05 23:06:09",
							description: "",
							first_chapter_link: "",
							price: "0.00",
							primary_isbn10: "1728210291",
							primary_isbn13: "9781728210292",
							book_uri:
								"nyt://book/14805c1f-c503-550e-912c-f90aa1507d04",
							publisher: "Sourcebooks Fire",
							rank: 2,
							rank_last_week: 0,
							sunday_review_link: "",
							title: "THE SUMMER OF BROKEN RULES",
							updated_date: "2023-07-05 23:06:09",
							weeks_on_list: 0,
							buy_links: [
								{
									name: "Amazon",
									url: "https://www.amazon.com/dp/1728210291?tag=NYTBSREV-20",
								},
								{
									name: "Apple Books",
									url: "https://goto.applebooks.apple/9781728210292?at=10lIEQ",
								},
								{
									name: "Barnes and Noble",
									url: "https://www.anrdoezrs.net/click-7990613-11819508?url=https%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9781728210292",
								},
								{
									name: "Books-A-Million",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fp%252FTHE%252BSUMMER%252BOF%252BBROKEN%252BRULES%252FK.L.%252BWalther%252F9781728210292&url2=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fsearch%253Fquery%253DTHE%252BSUMMER%252BOF%252BBROKEN%252BRULES%252BK.L.%252BWalther",
								},
								{
									name: "Bookshop",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fbookshop.org%2Fa%2F3546%2F9781728210292&url2=https%3A%2F%2Fbookshop.org%2Fbooks%3Faffiliate%3D3546%26keywords%3DTHE%2BSUMMER%2BOF%2BBROKEN%2BRULES",
								},
								{
									name: "IndieBound",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.indiebound.org%2Fbook%2F9781728210292%3Faff%3DNYT&url2=https%3A%2F%2Fwww.indiebound.org%2Fsearch%2Fbook%3Fkeys%3DTHE%2BSUMMER%2BOF%2BBROKEN%2BRULES%2BK.L.%2BWalther%26aff%3DNYT",
								},
							],
						},
						{
							age_group: "",
							amazon_product_url:
								"http://www.amazon.com/Girl-Pieces-Kathleen-Glasgow/dp/1101934719?tag=NYTBSREV-20",
							article_chapter_link: "",
							author: "Kathleen Glasgow",
							book_image:
								"https://storage.googleapis.com/du-prd/books/images/9781101934715.jpg",
							book_image_width: 328,
							book_image_height: 495,
							book_review_link: "",
							contributor: "by Kathleen Glasgow",
							contributor_note: "",
							created_date: "2023-07-05 23:06:09",
							description: "",
							first_chapter_link: "",
							price: "0.00",
							primary_isbn10: "1101934743",
							primary_isbn13: "9781101934746",
							book_uri:
								"nyt://book/4c61a5f1-f0f8-5184-978c-c7056c96a644",
							publisher: "Ember",
							rank: 3,
							rank_last_week: 0,
							sunday_review_link: "",
							title: "GIRL IN PIECES",
							updated_date: "2023-07-05 23:06:09",
							weeks_on_list: 0,
							buy_links: [
								{
									name: "Amazon",
									url: "http://www.amazon.com/Girl-Pieces-Kathleen-Glasgow/dp/1101934719?tag=NYTBSREV-20",
								},
								{
									name: "Apple Books",
									url: "https://goto.applebooks.apple/9781101934746?at=10lIEQ",
								},
								{
									name: "Barnes and Noble",
									url: "https://www.anrdoezrs.net/click-7990613-11819508?url=https%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9781101934746",
								},
								{
									name: "Books-A-Million",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fp%252FGIRL%252BIN%252BPIECES%252FKathleen%252BGlasgow%252F9781101934746&url2=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fsearch%253Fquery%253DGIRL%252BIN%252BPIECES%252BKathleen%252BGlasgow",
								},
								{
									name: "Bookshop",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fbookshop.org%2Fa%2F3546%2F9781101934746&url2=https%3A%2F%2Fbookshop.org%2Fbooks%3Faffiliate%3D3546%26keywords%3DGIRL%2BIN%2BPIECES",
								},
								{
									name: "IndieBound",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.indiebound.org%2Fbook%2F9781101934746%3Faff%3DNYT&url2=https%3A%2F%2Fwww.indiebound.org%2Fsearch%2Fbook%3Fkeys%3DGIRL%2BIN%2BPIECES%2BKathleen%2BGlasgow%26aff%3DNYT",
								},
							],
						},
						{
							age_group: "",
							amazon_product_url:
								"https://www.amazon.com/dp/1534467637?tag=NYTBSREV-20",
							article_chapter_link: "",
							author: "Lynn Painter",
							book_image:
								"https://storage.googleapis.com/du-prd/books/images/9781534467637.jpg",
							book_image_width: 333,
							book_image_height: 500,
							book_review_link: "",
							contributor: "by Lynn Painter",
							contributor_note: "",
							created_date: "2023-07-05 23:06:09",
							description: "",
							first_chapter_link: "",
							price: "0.00",
							primary_isbn10: "1534467637",
							primary_isbn13: "9781534467637",
							book_uri:
								"nyt://book/1bf449fc-40e6-556e-8e7a-f4dc8ea61c3a",
							publisher: "Simon & Schuster",
							rank: 4,
							rank_last_week: 0,
							sunday_review_link: "",
							title: "BETTER THAN THE MOVIES",
							updated_date: "2023-07-05 23:06:09",
							weeks_on_list: 0,
							buy_links: [
								{
									name: "Amazon",
									url: "https://www.amazon.com/dp/1534467637?tag=NYTBSREV-20",
								},
								{
									name: "Apple Books",
									url: "https://goto.applebooks.apple/9781534467637?at=10lIEQ",
								},
								{
									name: "Barnes and Noble",
									url: "https://www.anrdoezrs.net/click-7990613-11819508?url=https%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9781534467637",
								},
								{
									name: "Books-A-Million",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fp%252FBETTER%252BTHAN%252BTHE%252BMOVIES%252FLynn%252BPainter%252F9781534467637&url2=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fsearch%253Fquery%253DBETTER%252BTHAN%252BTHE%252BMOVIES%252BLynn%252BPainter",
								},
								{
									name: "Bookshop",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fbookshop.org%2Fa%2F3546%2F9781534467637&url2=https%3A%2F%2Fbookshop.org%2Fbooks%3Faffiliate%3D3546%26keywords%3DBETTER%2BTHAN%2BTHE%2BMOVIES",
								},
								{
									name: "IndieBound",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.indiebound.org%2Fbook%2F9781534467637%3Faff%3DNYT&url2=https%3A%2F%2Fwww.indiebound.org%2Fsearch%2Fbook%3Fkeys%3DBETTER%2BTHAN%2BTHE%2BMOVIES%2BLynn%2BPainter%26aff%3DNYT",
								},
							],
						},
						{
							age_group: "",
							amazon_product_url:
								"http://www.amazon.com/The-Way-I-Used-Be/dp/1481449354?tag=NYTBSREV-20",
							article_chapter_link: "",
							author: "Amber Smith",
							book_image:
								"https://storage.googleapis.com/du-prd/books/images/9781481449359.jpg",
							book_image_width: 324,
							book_image_height: 495,
							book_review_link: "",
							contributor: "by Amber Smith",
							contributor_note: "",
							created_date: "2023-07-05 23:06:09",
							description: "",
							first_chapter_link: "",
							price: "0.00",
							primary_isbn10: "1481449362",
							primary_isbn13: "9781481449366",
							book_uri:
								"nyt://book/bc9d9c11-51e8-5885-9239-fa84b126632d",
							publisher: "Margaret K. McElderry",
							rank: 5,
							rank_last_week: 0,
							sunday_review_link: "",
							title: "THE WAY I USED TO BE",
							updated_date: "2023-07-05 23:06:09",
							weeks_on_list: 0,
							buy_links: [
								{
									name: "Amazon",
									url: "http://www.amazon.com/The-Way-I-Used-Be/dp/1481449354?tag=NYTBSREV-20",
								},
								{
									name: "Apple Books",
									url: "https://goto.applebooks.apple/9781481449366?at=10lIEQ",
								},
								{
									name: "Barnes and Noble",
									url: "https://www.anrdoezrs.net/click-7990613-11819508?url=https%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9781481449366",
								},
								{
									name: "Books-A-Million",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fp%252FTHE%252BWAY%252BI%252BUSED%252BTO%252BBE%252FAmber%252BSmith%252F9781481449366&url2=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fsearch%253Fquery%253DTHE%252BWAY%252BI%252BUSED%252BTO%252BBE%252BAmber%252BSmith",
								},
								{
									name: "Bookshop",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fbookshop.org%2Fa%2F3546%2F9781481449366&url2=https%3A%2F%2Fbookshop.org%2Fbooks%3Faffiliate%3D3546%26keywords%3DTHE%2BWAY%2BI%2BUSED%2BTO%2BBE",
								},
								{
									name: "IndieBound",
									url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.indiebound.org%2Fbook%2F9781481449366%3Faff%3DNYT&url2=https%3A%2F%2Fwww.indiebound.org%2Fsearch%2Fbook%3Fkeys%3DTHE%2BWAY%2BI%2BUSED%2BTO%2BBE%2BAmber%2BSmith%26aff%3DNYT",
								},
							],
						},
					],
				},
			],
		},
	},
};

export default mock;

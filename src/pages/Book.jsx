import React, { useState, useEffect, useMemo } from "react";
import { useParams, useNavigate } from "react-router-dom";

import { useSelector, useDispatch } from "react-redux";
import { load } from "../store/listDetailsSlice";

import LoadingIndicator from "../components/LoadingIndicator";
import IconArrowLeft from "../components/IconArrowLeft";
import BookHeader from "../components/BookHeader";
import PurchaseLinks from "../components/PurchaseLinks";

const Book = () => {
	const { isbn } = useParams();
	const navigate = useNavigate();
	const dispatch = useDispatch();

	const listData = useSelector((state) => state.listDetails);
	const data = useMemo(
		() => listData.data?.filter((b) => b.primary_isbn13 === isbn),
		[isbn, listData?.data]
	);
	const loading = useMemo(() => !data, [data]);

	return (
		<div className="container mx-auto">
			<div className="flex flex-row items-center">
				<div className="cursor-pointer" onClick={() => navigate(-1)}>
					<IconArrowLeft />
				</div>
			</div>

			{loading ? (
				<LoadingIndicator />
			) : (
				<div className="mt-10">
					<BookHeader book={data[0]} />
					<PurchaseLinks links={data[0].buy_links} />
				</div>
			)}
		</div>
	);
};

export default Book;

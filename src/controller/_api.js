import axios from "axios";
import app_config from "./config";

const api = axios.create({
	baseURL: app_config.base_url,
});

/**
 * Request interceptor
 * Here we add the API_KEY to every request URL
 */
api.interceptors.request.use(
	(config) => {
		let query_token = "?";
		if (config.url.indexOf("?") > app_config.base_url.length) {
			query_token = "&";
		}
		config.url = config.url + `${query_token}api-key=${app_config.API_KEY}`;
		return config;
	},
	(error) => {
		return Promise.reject(error);
	}
);

export default api;

const mock = {
	data: {
		status: "OK",
		copyright:
			"Copyright (c) 2023 The New York Times Company.  All Rights Reserved.",
		num_results: 15,
		last_modified: "2023-07-19T22:14:33-04:00",
		results: {
			list_name: "Hardcover Fiction",
			list_name_encoded: "hardcover-fiction",
			bestsellers_date: "2023-07-15",
			published_date: "2023-07-30",
			published_date_description: "latest",
			next_published_date: "",
			previous_published_date: "2023-07-23",
			display_name: "Hardcover Fiction",
			normal_list_ends_at: 15,
			updated: "WEEKLY",
			books: [
				{
					rank: 1,
					rank_last_week: 1,
					weeks_on_list: 10,
					asterisk: 0,
					dagger: 0,
					primary_isbn10: "1649374046",
					primary_isbn13: "9781649374042",
					publisher: "Red Tower",
					description:
						"Violet Sorrengail is urged by the commanding general, who also is her mother, to become a candidate for the elite dragon riders.",
					price: "0.00",
					title: "FOURTH WING",
					author: "Rebecca Yarros",
					contributor: "by Rebecca Yarros",
					contributor_note: "",
					book_image:
						"https://storage.googleapis.com/du-prd/books/images/9781649374042.jpg",
					book_image_width: 309,
					book_image_height: 500,
					amazon_product_url:
						"https://www.amazon.com/dp/1649374046?tag=NYTBSREV-20",
					age_group: "",
					book_review_link: "",
					first_chapter_link: "",
					sunday_review_link: "",
					article_chapter_link: "",
					isbns: [
						{
							isbn10: "1649374046",
							isbn13: "9781649374042",
						},
						{
							isbn10: "1649374089",
							isbn13: "9781649374080",
						},
						{
							isbn10: "1705085059",
							isbn13: "9781705085059",
						},
						{
							isbn10: "1705085032",
							isbn13: "9781705085035",
						},
					],
					buy_links: [
						{
							name: "Amazon",
							url: "https://www.amazon.com/dp/1649374046?tag=NYTBSREV-20",
						},
						{
							name: "Apple Books",
							url: "https://goto.applebooks.apple/9781649374042?at=10lIEQ",
						},
						{
							name: "Barnes and Noble",
							url: "https://www.anrdoezrs.net/click-7990613-11819508?url=https%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9781649374042",
						},
						{
							name: "Books-A-Million",
							url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fp%252FFOURTH%252BWING%252FRebecca%252BYarros%252F9781649374042&url2=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fsearch%253Fquery%253DFOURTH%252BWING%252BRebecca%252BYarros",
						},
						{
							name: "Bookshop",
							url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fbookshop.org%2Fa%2F3546%2F9781649374042&url2=https%3A%2F%2Fbookshop.org%2Fbooks%3Faffiliate%3D3546%26keywords%3DFOURTH%2BWING",
						},
						{
							name: "IndieBound",
							url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.indiebound.org%2Fbook%2F9781649374042%3Faff%3DNYT&url2=https%3A%2F%2Fwww.indiebound.org%2Fsearch%2Fbook%3Fkeys%3DFOURTH%2BWING%2BRebecca%2BYarros%26aff%3DNYT",
						},
					],
					book_uri: "nyt://book/106c2cf0-7d20-51b1-bad4-91c3ebcd131a",
				},
				{
					rank: 2,
					rank_last_week: 2,
					weeks_on_list: 12,
					asterisk: 0,
					dagger: 0,
					primary_isbn10: "0593441273",
					primary_isbn13: "9780593441275",
					publisher: "Berkley",
					description:
						"A former couple pretend to be together for the sake of their friends during their annual getaway in Maine.",
					price: "0.00",
					title: "HAPPY PLACE",
					author: "Emily Henry",
					contributor: "by Emily Henry",
					contributor_note: "",
					book_image:
						"https://storage.googleapis.com/du-prd/books/images/9780593441275.jpg",
					book_image_width: 331,
					book_image_height: 500,
					amazon_product_url:
						"https://www.amazon.com/dp/0593441273?tag=NYTBSREV-20",
					age_group: "",
					book_review_link: "",
					first_chapter_link: "",
					sunday_review_link: "",
					article_chapter_link: "",
					isbns: [
						{
							isbn10: "0593441273",
							isbn13: "9780593441275",
						},
						{
							isbn10: "0593441206",
							isbn13: "9780593441206",
						},
						{
							isbn10: "0593675118",
							isbn13: "9780593675113",
						},
						{
							isbn10: "0593740149",
							isbn13: "9780593740149",
						},
					],
					buy_links: [
						{
							name: "Amazon",
							url: "https://www.amazon.com/dp/0593441273?tag=NYTBSREV-20",
						},
						{
							name: "Apple Books",
							url: "https://goto.applebooks.apple/9780593441275?at=10lIEQ",
						},
						{
							name: "Barnes and Noble",
							url: "https://www.anrdoezrs.net/click-7990613-11819508?url=https%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9780593441275",
						},
						{
							name: "Books-A-Million",
							url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fp%252FHAPPY%252BPLACE%252FEmily%252BHenry%252F9780593441275&url2=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fsearch%253Fquery%253DHAPPY%252BPLACE%252BEmily%252BHenry",
						},
						{
							name: "Bookshop",
							url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fbookshop.org%2Fa%2F3546%2F9780593441275&url2=https%3A%2F%2Fbookshop.org%2Fbooks%3Faffiliate%3D3546%26keywords%3DHAPPY%2BPLACE",
						},
						{
							name: "IndieBound",
							url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.indiebound.org%2Fbook%2F9780593441275%3Faff%3DNYT&url2=https%3A%2F%2Fwww.indiebound.org%2Fsearch%2Fbook%3Fkeys%3DHAPPY%2BPLACE%2BEmily%2BHenry%26aff%3DNYT",
						},
					],
					book_uri: "nyt://book/9ab3a59a-e572-5452-ae0c-eedeadb05831",
				},
				{
					rank: 3,
					rank_last_week: 3,
					weeks_on_list: 62,
					asterisk: 0,
					dagger: 0,
					primary_isbn10: "038554734X",
					primary_isbn13: "9780385547345",
					publisher: "Doubleday",
					description:
						"A scientist and single mother living in California in the 1960s becomes a star on a TV cooking show.",
					price: "0.00",
					title: "LESSONS IN CHEMISTRY",
					author: "Bonnie Garmus",
					contributor: "by Bonnie Garmus",
					contributor_note: "",
					book_image:
						"https://storage.googleapis.com/du-prd/books/images/9780385547345.jpg",
					book_image_width: 328,
					book_image_height: 500,
					amazon_product_url:
						"https://www.amazon.com/dp/038554734X?tag=NYTBSREV-20",
					age_group: "",
					book_review_link: "",
					first_chapter_link: "",
					sunday_review_link: "",
					article_chapter_link: "",
					isbns: [
						{
							isbn10: "038554734X",
							isbn13: "9780385547345",
						},
						{
							isbn10: "0593507533",
							isbn13: "9780593507537",
						},
						{
							isbn10: "0385547374",
							isbn13: "9780385547376",
						},
					],
					buy_links: [
						{
							name: "Amazon",
							url: "https://www.amazon.com/dp/038554734X?tag=NYTBSREV-20",
						},
						{
							name: "Apple Books",
							url: "https://goto.applebooks.apple/9780385547345?at=10lIEQ",
						},
						{
							name: "Barnes and Noble",
							url: "https://www.anrdoezrs.net/click-7990613-11819508?url=https%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9780385547345",
						},
						{
							name: "Books-A-Million",
							url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fp%252FLESSONS%252BIN%252BCHEMISTRY%252FBonnie%252BGarmus%252F9780385547345&url2=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fsearch%253Fquery%253DLESSONS%252BIN%252BCHEMISTRY%252BBonnie%252BGarmus",
						},
						{
							name: "Bookshop",
							url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fbookshop.org%2Fa%2F3546%2F9780385547345&url2=https%3A%2F%2Fbookshop.org%2Fbooks%3Faffiliate%3D3546%26keywords%3DLESSONS%2BIN%2BCHEMISTRY",
						},
						{
							name: "IndieBound",
							url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.indiebound.org%2Fbook%2F9780385547345%3Faff%3DNYT&url2=https%3A%2F%2Fwww.indiebound.org%2Fsearch%2Fbook%3Fkeys%3DLESSONS%2BIN%2BCHEMISTRY%2BBonnie%2BGarmus%26aff%3DNYT",
						},
					],
					book_uri: "nyt://book/717e0ead-9782-567d-af27-d9a6cfb1856f",
				},
				{
					rank: 4,
					rank_last_week: 0,
					weeks_on_list: 1,
					asterisk: 0,
					dagger: 0,
					primary_isbn10: "0316499579",
					primary_isbn13: "9780316499576",
					publisher: "Little, Brown",
					description:
						"A killer, who targets women in New York City, becomes obsessed with Michael Bennett’s daughter.",
					price: "0.00",
					title: "OBSESSED",
					author: "James Patterson and James O. Born",
					contributor: "by James Patterson and James O. Born",
					contributor_note: "",
					book_image:
						"https://storage.googleapis.com/du-prd/books/images/9780316499576.jpg",
					book_image_width: 322,
					book_image_height: 500,
					amazon_product_url:
						"https://www.amazon.com/dp/0316499579?tag=NYTBSREV-20",
					age_group: "",
					book_review_link: "",
					first_chapter_link: "",
					sunday_review_link: "",
					article_chapter_link: "",
					isbns: [
						{
							isbn10: "0316499579",
							isbn13: "9780316499576",
						},
						{
							isbn10: "0316499587",
							isbn13: "9780316499583",
						},
					],
					buy_links: [
						{
							name: "Amazon",
							url: "https://www.amazon.com/dp/0316499579?tag=NYTBSREV-20",
						},
						{
							name: "Apple Books",
							url: "https://goto.applebooks.apple/9780316499576?at=10lIEQ",
						},
						{
							name: "Barnes and Noble",
							url: "https://www.anrdoezrs.net/click-7990613-11819508?url=https%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9780316499576",
						},
						{
							name: "Books-A-Million",
							url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fp%252FOBSESSED%252FJames%252BPatterson%252Band%252BJames%252BO.%252BBorn%252F9780316499576&url2=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fsearch%253Fquery%253DOBSESSED%252BJames%252BPatterson%252Band%252BJames%252BO.%252BBorn",
						},
						{
							name: "Bookshop",
							url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fbookshop.org%2Fa%2F3546%2F9780316499576&url2=https%3A%2F%2Fbookshop.org%2Fbooks%3Faffiliate%3D3546%26keywords%3DOBSESSED",
						},
						{
							name: "IndieBound",
							url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.indiebound.org%2Fbook%2F9780316499576%3Faff%3DNYT&url2=https%3A%2F%2Fwww.indiebound.org%2Fsearch%2Fbook%3Fkeys%3DOBSESSED%2BJames%2BPatterson%2Band%2BJames%2BO.%2BBorn%26aff%3DNYT",
						},
					],
					book_uri: "nyt://book/f1421518-0d0e-5dc4-b96e-a71560ef0aff",
				},
				{
					rank: 5,
					rank_last_week: 4,
					weeks_on_list: 5,
					asterisk: 0,
					dagger: 0,
					primary_isbn10: "0316258776",
					primary_isbn13: "9780316258777",
					publisher: "Little, Brown",
					description:
						"After a tragedy, a popular food blogger brings friends from distinct times in her life to spend a weekend in Nantucket.",
					price: "0.00",
					title: "THE FIVE-STAR WEEKEND",
					author: "Elin Hilderbrand",
					contributor: "by Elin Hilderbrand",
					contributor_note: "",
					book_image:
						"https://storage.googleapis.com/du-prd/books/images/9780316258777.jpg",
					book_image_width: 323,
					book_image_height: 500,
					amazon_product_url:
						"https://www.amazon.com/dp/0316258776?tag=NYTBSREV-20",
					age_group: "",
					book_review_link: "",
					first_chapter_link: "",
					sunday_review_link: "",
					article_chapter_link: "",
					isbns: [
						{
							isbn10: "0316258776",
							isbn13: "9780316258777",
						},
						{
							isbn10: "0316259381",
							isbn13: "9780316259385",
						},
						{
							isbn10: "1668629496",
							isbn13: "9781668629499",
						},
					],
					buy_links: [
						{
							name: "Amazon",
							url: "https://www.amazon.com/dp/0316258776?tag=NYTBSREV-20",
						},
						{
							name: "Apple Books",
							url: "https://goto.applebooks.apple/9780316258777?at=10lIEQ",
						},
						{
							name: "Barnes and Noble",
							url: "https://www.anrdoezrs.net/click-7990613-11819508?url=https%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9780316258777",
						},
						{
							name: "Books-A-Million",
							url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fp%252FTHE%252BFIVE-STAR%252BWEEKEND%252FElin%252BHilderbrand%252F9780316258777&url2=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fsearch%253Fquery%253DTHE%252BFIVE-STAR%252BWEEKEND%252BElin%252BHilderbrand",
						},
						{
							name: "Bookshop",
							url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fbookshop.org%2Fa%2F3546%2F9780316258777&url2=https%3A%2F%2Fbookshop.org%2Fbooks%3Faffiliate%3D3546%26keywords%3DTHE%2BFIVE-STAR%2BWEEKEND",
						},
						{
							name: "IndieBound",
							url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.indiebound.org%2Fbook%2F9780316258777%3Faff%3DNYT&url2=https%3A%2F%2Fwww.indiebound.org%2Fsearch%2Fbook%3Fkeys%3DTHE%2BFIVE-STAR%2BWEEKEND%2BElin%2BHilderbrand%26aff%3DNYT",
						},
					],
					book_uri: "nyt://book/8c779b4e-ab10-5946-a522-e8dd1167b1db",
				},
				{
					rank: 6,
					rank_last_week: 5,
					weeks_on_list: 11,
					asterisk: 0,
					dagger: 0,
					primary_isbn10: "0802162177",
					primary_isbn13: "9780802162175",
					publisher: "Grove",
					description:
						"Three generations of a family living on South India’s Malabar Coast suffer the loss of a family member by drowning.",
					price: "0.00",
					title: "THE COVENANT OF WATER",
					author: "Abraham Verghese",
					contributor: "by Abraham Verghese",
					contributor_note: "",
					book_image:
						"https://storage.googleapis.com/du-prd/books/images/9780802162175.jpg",
					book_image_width: 320,
					book_image_height: 500,
					amazon_product_url:
						"https://www.amazon.com/dp/0802162177?tag=NYTBSREV-20",
					age_group: "",
					book_review_link: "",
					first_chapter_link: "",
					sunday_review_link: "",
					article_chapter_link: "",
					isbns: [
						{
							isbn10: "0802162177",
							isbn13: "9780802162175",
						},
						{
							isbn10: "1705070248",
							isbn13: "9781705070246",
						},
						{
							isbn10: "1705070221",
							isbn13: "9781705070222",
						},
					],
					buy_links: [
						{
							name: "Amazon",
							url: "https://www.amazon.com/dp/0802162177?tag=NYTBSREV-20",
						},
						{
							name: "Apple Books",
							url: "https://goto.applebooks.apple/9780802162175?at=10lIEQ",
						},
						{
							name: "Barnes and Noble",
							url: "https://www.anrdoezrs.net/click-7990613-11819508?url=https%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9780802162175",
						},
						{
							name: "Books-A-Million",
							url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fp%252FTHE%252BCOVENANT%252BOF%252BWATER%252FAbraham%252BVerghese%252F9780802162175&url2=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fsearch%253Fquery%253DTHE%252BCOVENANT%252BOF%252BWATER%252BAbraham%252BVerghese",
						},
						{
							name: "Bookshop",
							url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fbookshop.org%2Fa%2F3546%2F9780802162175&url2=https%3A%2F%2Fbookshop.org%2Fbooks%3Faffiliate%3D3546%26keywords%3DTHE%2BCOVENANT%2BOF%2BWATER",
						},
						{
							name: "IndieBound",
							url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.indiebound.org%2Fbook%2F9780802162175%3Faff%3DNYT&url2=https%3A%2F%2Fwww.indiebound.org%2Fsearch%2Fbook%3Fkeys%3DTHE%2BCOVENANT%2BOF%2BWATER%2BAbraham%2BVerghese%26aff%3DNYT",
						},
					],
					book_uri: "nyt://book/5ebf1d34-4c68-560a-8330-7e65bedaaa1a",
				},
				{
					rank: 7,
					rank_last_week: 6,
					weeks_on_list: 37,
					asterisk: 0,
					dagger: 0,
					primary_isbn10: "0063251922",
					primary_isbn13: "9780063251922",
					publisher: "Harper",
					description:
						"Winner of a 2023 Pulitzer Prize for fiction. A reimagining of Charles Dickens’s “David Copperfield” set in the mountains of southern Appalachia.",
					price: "0.00",
					title: "DEMON COPPERHEAD",
					author: "Barbara Kingsolver",
					contributor: "by Barbara Kingsolver",
					contributor_note: "",
					book_image:
						"https://storage.googleapis.com/du-prd/books/images/9780063251922.jpg",
					book_image_width: 329,
					book_image_height: 500,
					amazon_product_url:
						"https://www.amazon.com/dp/0063251922?tag=NYTBSREV-20",
					age_group: "",
					book_review_link: "",
					first_chapter_link: "",
					sunday_review_link: "",
					article_chapter_link: "",
					isbns: [
						{
							isbn10: "0063251922",
							isbn13: "9780063251922",
						},
						{
							isbn10: "006325199X",
							isbn13: "9780063251991",
						},
					],
					buy_links: [
						{
							name: "Amazon",
							url: "https://www.amazon.com/dp/0063251922?tag=NYTBSREV-20",
						},
						{
							name: "Apple Books",
							url: "https://goto.applebooks.apple/9780063251922?at=10lIEQ",
						},
						{
							name: "Barnes and Noble",
							url: "https://www.anrdoezrs.net/click-7990613-11819508?url=https%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9780063251922",
						},
						{
							name: "Books-A-Million",
							url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fp%252FDEMON%252BCOPPERHEAD%252FBarbara%252BKingsolver%252F9780063251922&url2=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fsearch%253Fquery%253DDEMON%252BCOPPERHEAD%252BBarbara%252BKingsolver",
						},
						{
							name: "Bookshop",
							url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fbookshop.org%2Fa%2F3546%2F9780063251922&url2=https%3A%2F%2Fbookshop.org%2Fbooks%3Faffiliate%3D3546%26keywords%3DDEMON%2BCOPPERHEAD",
						},
						{
							name: "IndieBound",
							url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.indiebound.org%2Fbook%2F9780063251922%3Faff%3DNYT&url2=https%3A%2F%2Fwww.indiebound.org%2Fsearch%2Fbook%3Fkeys%3DDEMON%2BCOPPERHEAD%2BBarbara%2BKingsolver%26aff%3DNYT",
						},
					],
					book_uri: "nyt://book/dd6e4f9d-d082-552d-b888-7276a25b5aca",
				},
				{
					rank: 8,
					rank_last_week: 12,
					weeks_on_list: 41,
					asterisk: 0,
					dagger: 0,
					primary_isbn10: "0593321200",
					primary_isbn13: "9780593321201",
					publisher: "Knopf",
					description:
						"Two friends find their partnership challenged in the world of video game design.",
					price: "0.00",
					title: "TOMORROW, AND TOMORROW, AND TOMORROW",
					author: "Gabrielle Zevin",
					contributor: "by Gabrielle Zevin",
					contributor_note: "",
					book_image:
						"https://storage.googleapis.com/du-prd/books/images/9780593321201.jpg",
					book_image_width: 329,
					book_image_height: 500,
					amazon_product_url:
						"https://www.amazon.com/dp/0593321200?tag=NYTBSREV-20",
					age_group: "",
					book_review_link: "",
					first_chapter_link: "",
					sunday_review_link: "",
					article_chapter_link: "",
					isbns: [
						{
							isbn10: "0593321200",
							isbn13: "9780593321201",
						},
						{
							isbn10: "0593321219",
							isbn13: "9780593321218",
						},
						{
							isbn10: "0593591631",
							isbn13: "9780593591635",
						},
					],
					buy_links: [
						{
							name: "Amazon",
							url: "https://www.amazon.com/dp/0593321200?tag=NYTBSREV-20",
						},
						{
							name: "Apple Books",
							url: "https://goto.applebooks.apple/9780593321201?at=10lIEQ",
						},
						{
							name: "Barnes and Noble",
							url: "https://www.anrdoezrs.net/click-7990613-11819508?url=https%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9780593321201",
						},
						{
							name: "Books-A-Million",
							url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fp%252FTOMORROW%25252C%252BAND%252BTOMORROW%25252C%252BAND%252BTOMORROW%252FGabrielle%252BZevin%252F9780593321201&url2=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fsearch%253Fquery%253DTOMORROW%25252C%252BAND%252BTOMORROW%25252C%252BAND%252BTOMORROW%252BGabrielle%252BZevin",
						},
						{
							name: "Bookshop",
							url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fbookshop.org%2Fa%2F3546%2F9780593321201&url2=https%3A%2F%2Fbookshop.org%2Fbooks%3Faffiliate%3D3546%26keywords%3DTOMORROW%252C%2BAND%2BTOMORROW%252C%2BAND%2BTOMORROW",
						},
						{
							name: "IndieBound",
							url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.indiebound.org%2Fbook%2F9780593321201%3Faff%3DNYT&url2=https%3A%2F%2Fwww.indiebound.org%2Fsearch%2Fbook%3Fkeys%3DTOMORROW%252C%2BAND%2BTOMORROW%252C%2BAND%2BTOMORROW%2BGabrielle%2BZevin%26aff%3DNYT",
						},
					],
					book_uri: "nyt://book/bc07e3e3-e917-50bb-9439-70118335f40c",
				},
				{
					rank: 9,
					rank_last_week: 7,
					weeks_on_list: 4,
					asterisk: 0,
					dagger: 0,
					primary_isbn10: "0593183223",
					primary_isbn13: "9780593183229",
					publisher: "Dutton",
					description:
						"In 1983, a mute woman confined to a wheelchair types out her side of the story about a family massacre to her home-health aide.",
					price: "0.00",
					title: "THE ONLY ONE LEFT",
					author: "Riley Sager",
					contributor: "by Riley Sager",
					contributor_note: "",
					book_image:
						"https://storage.googleapis.com/du-prd/books/images/9780593183229.jpg",
					book_image_width: 331,
					book_image_height: 500,
					amazon_product_url:
						"https://www.amazon.com/dp/0593183223?tag=NYTBSREV-20",
					age_group: "",
					book_review_link: "",
					first_chapter_link: "",
					sunday_review_link: "",
					article_chapter_link: "",
					isbns: [
						{
							isbn10: "0593183223",
							isbn13: "9780593183229",
						},
						{
							isbn10: "0593183231",
							isbn13: "9780593183236",
						},
					],
					buy_links: [
						{
							name: "Amazon",
							url: "https://www.amazon.com/dp/0593183223?tag=NYTBSREV-20",
						},
						{
							name: "Apple Books",
							url: "https://goto.applebooks.apple/9780593183229?at=10lIEQ",
						},
						{
							name: "Barnes and Noble",
							url: "https://www.anrdoezrs.net/click-7990613-11819508?url=https%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9780593183229",
						},
						{
							name: "Books-A-Million",
							url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fp%252FTHE%252BONLY%252BONE%252BLEFT%252FRiley%252BSager%252F9780593183229&url2=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fsearch%253Fquery%253DTHE%252BONLY%252BONE%252BLEFT%252BRiley%252BSager",
						},
						{
							name: "Bookshop",
							url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fbookshop.org%2Fa%2F3546%2F9780593183229&url2=https%3A%2F%2Fbookshop.org%2Fbooks%3Faffiliate%3D3546%26keywords%3DTHE%2BONLY%2BONE%2BLEFT",
						},
						{
							name: "IndieBound",
							url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.indiebound.org%2Fbook%2F9780593183229%3Faff%3DNYT&url2=https%3A%2F%2Fwww.indiebound.org%2Fsearch%2Fbook%3Fkeys%3DTHE%2BONLY%2BONE%2BLEFT%2BRiley%2BSager%26aff%3DNYT",
						},
					],
					book_uri: "nyt://book/dca58bef-c5c8-54a5-aa7a-53d2a4c185ca",
				},
				{
					rank: 10,
					rank_last_week: 9,
					weeks_on_list: 7,
					asterisk: 0,
					dagger: 0,
					primary_isbn10: "0063250837",
					primary_isbn13: "9780063250833",
					publisher: "Morrow",
					description:
						"June Hayward, a struggling writer, must conceal the fact that she stole Athena Liu’s just-finished masterpiece after Liu’s sudden death.",
					price: "0.00",
					title: "YELLOWFACE",
					author: "R.F. Kuang",
					contributor: "by R.F. Kuang",
					contributor_note: "",
					book_image:
						"https://storage.googleapis.com/du-prd/books/images/9780063250833.jpg",
					book_image_width: 332,
					book_image_height: 500,
					amazon_product_url:
						"https://www.amazon.com/dp/0063250837?tag=NYTBSREV-20",
					age_group: "",
					book_review_link: "",
					first_chapter_link: "",
					sunday_review_link: "",
					article_chapter_link: "",
					isbns: [
						{
							isbn10: "0063250837",
							isbn13: "9780063250833",
						},
						{
							isbn10: "0063250845",
							isbn13: "9780063250840",
						},
					],
					buy_links: [
						{
							name: "Amazon",
							url: "https://www.amazon.com/dp/0063250837?tag=NYTBSREV-20",
						},
						{
							name: "Apple Books",
							url: "https://goto.applebooks.apple/9780063250833?at=10lIEQ",
						},
						{
							name: "Barnes and Noble",
							url: "https://www.anrdoezrs.net/click-7990613-11819508?url=https%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9780063250833",
						},
						{
							name: "Books-A-Million",
							url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fp%252FYELLOWFACE%252FR.F.%252BKuang%252F9780063250833&url2=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fsearch%253Fquery%253DYELLOWFACE%252BR.F.%252BKuang",
						},
						{
							name: "Bookshop",
							url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fbookshop.org%2Fa%2F3546%2F9780063250833&url2=https%3A%2F%2Fbookshop.org%2Fbooks%3Faffiliate%3D3546%26keywords%3DYELLOWFACE",
						},
						{
							name: "IndieBound",
							url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.indiebound.org%2Fbook%2F9780063250833%3Faff%3DNYT&url2=https%3A%2F%2Fwww.indiebound.org%2Fsearch%2Fbook%3Fkeys%3DYELLOWFACE%2BR.F.%2BKuang%26aff%3DNYT",
						},
					],
					book_uri: "nyt://book/373be832-eae0-51c7-bb40-a411601b36ef",
				},
				{
					rank: 11,
					rank_last_week: 8,
					weeks_on_list: 18,
					asterisk: 0,
					dagger: 0,
					primary_isbn10: "0593243730",
					primary_isbn13: "9780593243732",
					publisher: "Dial",
					description:
						"In a homage to Louisa May Alcott’s “Little Women,” a young man’s dark past resurfaces as he gets to know the family of his college sweetheart.",
					price: "0.00",
					title: "HELLO BEAUTIFUL",
					author: "Ann Napolitano",
					contributor: "by Ann Napolitano",
					contributor_note: "",
					book_image:
						"https://storage.googleapis.com/du-prd/books/images/9780593243732.jpg",
					book_image_width: 330,
					book_image_height: 500,
					amazon_product_url:
						"https://www.amazon.com/dp/0593243730?tag=NYTBSREV-20",
					age_group: "",
					book_review_link: "",
					first_chapter_link: "",
					sunday_review_link: "",
					article_chapter_link: "",
					isbns: [
						{
							isbn10: "0593243730",
							isbn13: "9780593243732",
						},
						{
							isbn10: "0593243749",
							isbn13: "9780593243749",
						},
						{
							isbn10: "0593664043",
							isbn13: "9780593664049",
						},
					],
					buy_links: [
						{
							name: "Amazon",
							url: "https://www.amazon.com/dp/0593243730?tag=NYTBSREV-20",
						},
						{
							name: "Apple Books",
							url: "https://goto.applebooks.apple/9780593243732?at=10lIEQ",
						},
						{
							name: "Barnes and Noble",
							url: "https://www.anrdoezrs.net/click-7990613-11819508?url=https%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9780593243732",
						},
						{
							name: "Books-A-Million",
							url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fp%252FHELLO%252BBEAUTIFUL%252FAnn%252BNapolitano%252F9780593243732&url2=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fsearch%253Fquery%253DHELLO%252BBEAUTIFUL%252BAnn%252BNapolitano",
						},
						{
							name: "Bookshop",
							url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fbookshop.org%2Fa%2F3546%2F9780593243732&url2=https%3A%2F%2Fbookshop.org%2Fbooks%3Faffiliate%3D3546%26keywords%3DHELLO%2BBEAUTIFUL",
						},
						{
							name: "IndieBound",
							url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.indiebound.org%2Fbook%2F9780593243732%3Faff%3DNYT&url2=https%3A%2F%2Fwww.indiebound.org%2Fsearch%2Fbook%3Fkeys%3DHELLO%2BBEAUTIFUL%2BAnn%2BNapolitano%26aff%3DNYT",
						},
					],
					book_uri: "nyt://book/3b1a624d-7296-5b11-9c41-a473f433c18d",
				},
				{
					rank: 12,
					rank_last_week: 0,
					weeks_on_list: 1,
					asterisk: 0,
					dagger: 0,
					primary_isbn10: "059360055X",
					primary_isbn13: "9780593600559",
					publisher: "Ballantine",
					description:
						"A nursing student and her widowed landlady navigate life changes and potential love interests.",
					price: "0.00",
					title: "MUST LOVE FLOWERS",
					author: "Debbie Macomber",
					contributor: "by Debbie Macomber",
					contributor_note: "",
					book_image:
						"https://storage.googleapis.com/du-prd/books/images/9780593600559.jpg",
					book_image_width: 329,
					book_image_height: 500,
					amazon_product_url:
						"https://www.amazon.com/dp/059360055X?tag=NYTBSREV-20",
					age_group: "",
					book_review_link: "",
					first_chapter_link: "",
					sunday_review_link: "",
					article_chapter_link: "",
					isbns: [
						{
							isbn10: "059360055X",
							isbn13: "9780593600559",
						},
					],
					buy_links: [
						{
							name: "Amazon",
							url: "https://www.amazon.com/dp/059360055X?tag=NYTBSREV-20",
						},
						{
							name: "Apple Books",
							url: "https://goto.applebooks.apple/9780593600559?at=10lIEQ",
						},
						{
							name: "Barnes and Noble",
							url: "https://www.anrdoezrs.net/click-7990613-11819508?url=https%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9780593600559",
						},
						{
							name: "Books-A-Million",
							url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fp%252FMUST%252BLOVE%252BFLOWERS%252FDebbie%252BMacomber%252F9780593600559&url2=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fsearch%253Fquery%253DMUST%252BLOVE%252BFLOWERS%252BDebbie%252BMacomber",
						},
						{
							name: "Bookshop",
							url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fbookshop.org%2Fa%2F3546%2F9780593600559&url2=https%3A%2F%2Fbookshop.org%2Fbooks%3Faffiliate%3D3546%26keywords%3DMUST%2BLOVE%2BFLOWERS",
						},
						{
							name: "IndieBound",
							url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.indiebound.org%2Fbook%2F9780593600559%3Faff%3DNYT&url2=https%3A%2F%2Fwww.indiebound.org%2Fsearch%2Fbook%3Fkeys%3DMUST%2BLOVE%2BFLOWERS%2BDebbie%2BMacomber%26aff%3DNYT",
						},
					],
					book_uri: "nyt://book/af054ceb-419a-5249-bee8-9c27a926cf71",
				},
				{
					rank: 13,
					rank_last_week: 0,
					weeks_on_list: 1,
					asterisk: 0,
					dagger: 0,
					primary_isbn10: "1668010828",
					primary_isbn13: "9781668010822",
					publisher: "Gallery",
					description:
						"Four women join together to save a summer camp for girls.",
					price: "0.00",
					title: "THE SUMMER OF SONGBIRDS",
					author: "Kristy Woodson Harvey",
					contributor: "by Kristy Woodson Harvey",
					contributor_note: "",
					book_image:
						"https://storage.googleapis.com/du-prd/books/images/9781668010822.jpg",
					book_image_width: 331,
					book_image_height: 500,
					amazon_product_url:
						"https://www.amazon.com/dp/1668010828?tag=NYTBSREV-20",
					age_group: "",
					book_review_link: "",
					first_chapter_link: "",
					sunday_review_link: "",
					article_chapter_link: "",
					isbns: [
						{
							isbn10: "1668010828",
							isbn13: "9781668010822",
						},
					],
					buy_links: [
						{
							name: "Amazon",
							url: "https://www.amazon.com/dp/1668010828?tag=NYTBSREV-20",
						},
						{
							name: "Apple Books",
							url: "https://goto.applebooks.apple/9781668010822?at=10lIEQ",
						},
						{
							name: "Barnes and Noble",
							url: "https://www.anrdoezrs.net/click-7990613-11819508?url=https%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9781668010822",
						},
						{
							name: "Books-A-Million",
							url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fp%252FTHE%252BSUMMER%252BOF%252BSONGBIRDS%252FKristy%252BWoodson%252BHarvey%252F9781668010822&url2=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fsearch%253Fquery%253DTHE%252BSUMMER%252BOF%252BSONGBIRDS%252BKristy%252BWoodson%252BHarvey",
						},
						{
							name: "Bookshop",
							url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fbookshop.org%2Fa%2F3546%2F9781668010822&url2=https%3A%2F%2Fbookshop.org%2Fbooks%3Faffiliate%3D3546%26keywords%3DTHE%2BSUMMER%2BOF%2BSONGBIRDS",
						},
						{
							name: "IndieBound",
							url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.indiebound.org%2Fbook%2F9781668010822%3Faff%3DNYT&url2=https%3A%2F%2Fwww.indiebound.org%2Fsearch%2Fbook%3Fkeys%3DTHE%2BSUMMER%2BOF%2BSONGBIRDS%2BKristy%2BWoodson%2BHarvey%26aff%3DNYT",
						},
					],
					book_uri: "nyt://book/fa10ae5c-dd59-52c6-8903-14767dd061f7",
				},
				{
					rank: 14,
					rank_last_week: 15,
					weeks_on_list: 21,
					asterisk: 0,
					dagger: 0,
					primary_isbn10: "0063204150",
					primary_isbn13: "9780063204157",
					publisher: "Ecco",
					description:
						"A widow working the night shift at the Sowell Bay Aquarium is aided in solving a mystery by a giant Pacific octopus living there.",
					price: "0.00",
					title: "REMARKABLY BRIGHT CREATURES",
					author: "Shel Van Pelt",
					contributor: "by Shelby Van Pelt",
					contributor_note: "",
					book_image:
						"https://storage.googleapis.com/du-prd/books/images/9780063204157.jpg",
					book_image_width: 329,
					book_image_height: 500,
					amazon_product_url:
						"https://www.amazon.com/dp/0063204150?tag=NYTBSREV-20",
					age_group: "",
					book_review_link: "",
					first_chapter_link: "",
					sunday_review_link: "",
					article_chapter_link: "",
					isbns: [
						{
							isbn10: "0063204150",
							isbn13: "9780063204157",
						},
						{
							isbn10: "0063204185",
							isbn13: "9780063204188",
						},
						{
							isbn10: "0063204169",
							isbn13: "9780063204164",
						},
					],
					buy_links: [
						{
							name: "Amazon",
							url: "https://www.amazon.com/dp/0063204150?tag=NYTBSREV-20",
						},
						{
							name: "Apple Books",
							url: "https://goto.applebooks.apple/9780063204157?at=10lIEQ",
						},
						{
							name: "Barnes and Noble",
							url: "https://www.anrdoezrs.net/click-7990613-11819508?url=https%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9780063204157",
						},
						{
							name: "Books-A-Million",
							url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fp%252FREMARKABLY%252BBRIGHT%252BCREATURES%252FShel%252BVan%252BPelt%252F9780063204157&url2=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fsearch%253Fquery%253DREMARKABLY%252BBRIGHT%252BCREATURES%252BShel%252BVan%252BPelt",
						},
						{
							name: "Bookshop",
							url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fbookshop.org%2Fa%2F3546%2F9780063204157&url2=https%3A%2F%2Fbookshop.org%2Fbooks%3Faffiliate%3D3546%26keywords%3DREMARKABLY%2BBRIGHT%2BCREATURES",
						},
						{
							name: "IndieBound",
							url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.indiebound.org%2Fbook%2F9780063204157%3Faff%3DNYT&url2=https%3A%2F%2Fwww.indiebound.org%2Fsearch%2Fbook%3Fkeys%3DREMARKABLY%2BBRIGHT%2BCREATURES%2BShel%2BVan%2BPelt%26aff%3DNYT",
						},
					],
					book_uri: "nyt://book/206a5e83-12f4-5ea9-8e63-1da5470a6ef5",
				},
				{
					rank: 15,
					rank_last_week: 0,
					weeks_on_list: 1,
					asterisk: 0,
					dagger: 0,
					primary_isbn10: "1250283787",
					primary_isbn13: "9781250283788",
					publisher: "St. Martin's",
					description:
						"A portrait painter, who is diagnosed with a condition known as face blindness, falls for two different men.",
					price: "0.00",
					title: "HELLO STRANGER",
					author: "Katherine Center",
					contributor: "by Katherine Center",
					contributor_note: "",
					book_image:
						"https://storage.googleapis.com/du-prd/books/images/9781250283788.jpg",
					book_image_width: 329,
					book_image_height: 500,
					amazon_product_url:
						"https://www.amazon.com/dp/1250283787?tag=NYTBSREV-20",
					age_group: "",
					book_review_link: "",
					first_chapter_link: "",
					sunday_review_link: "",
					article_chapter_link: "",
					isbns: [
						{
							isbn10: "1250283787",
							isbn13: "9781250283788",
						},
					],
					buy_links: [
						{
							name: "Amazon",
							url: "https://www.amazon.com/dp/1250283787?tag=NYTBSREV-20",
						},
						{
							name: "Apple Books",
							url: "https://goto.applebooks.apple/9781250283788?at=10lIEQ",
						},
						{
							name: "Barnes and Noble",
							url: "https://www.anrdoezrs.net/click-7990613-11819508?url=https%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9781250283788",
						},
						{
							name: "Books-A-Million",
							url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fp%252FHELLO%252BSTRANGER%252FKatherine%252BCenter%252F9781250283788&url2=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fsearch%253Fquery%253DHELLO%252BSTRANGER%252BKatherine%252BCenter",
						},
						{
							name: "Bookshop",
							url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fbookshop.org%2Fa%2F3546%2F9781250283788&url2=https%3A%2F%2Fbookshop.org%2Fbooks%3Faffiliate%3D3546%26keywords%3DHELLO%2BSTRANGER",
						},
						{
							name: "IndieBound",
							url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.indiebound.org%2Fbook%2F9781250283788%3Faff%3DNYT&url2=https%3A%2F%2Fwww.indiebound.org%2Fsearch%2Fbook%3Fkeys%3DHELLO%2BSTRANGER%2BKatherine%2BCenter%26aff%3DNYT",
						},
					],
					book_uri: "nyt://book/06e1a2d6-1d48-5218-8f16-7e2ef8fc93da",
				},
			],
			corrections: [],
		},
	},
};

export default mock;

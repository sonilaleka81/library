import { useCallback } from "react";
import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";

import IconBookSolid from "./IconBookSolid";
import IconBookShelf from "./IconBookShelf";

const MySwal = withReactContent(Swal);

const Item = (props) => {
	const askDate = useCallback(() => {
		MySwal.fire({
			title: <p>Choose Date Period for the book list</p>,
			html: `<div className="mt-5 md:mt-0 self-end justify-self-end w-full">
					<input
						type="date"
						id="date"
						className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5"
						placeholder="Date"
					/>
				</div>`,
			showCancelButton: true,
			confirmButtonText: "Open",
		}).then(() => {
			const date = document.getElementById("date").value;
			props.onOpen(date);
		});
	}, [props]);

	return (
		<div
			// to={`/list/${props.list_name_encoded}/current`}
			onClick={() => askDate()}
			className="cursor-pointer w-full h-full transition duration-500 hover:scale-95 hover:opacity-75 "
		>
			{props.list_image ? (
				<img
					src={props.list_image}
					className="object-cover w-full h-full"
					alt={props?.display_name}
				/>
			) : (
				<div
					className={`w-full aspect-square opacity-100 bg-slate-500 rounded`}
				>
					<div className="h-full w-full flex justify-center items-center">
						<IconBookShelf />
					</div>
				</div>
			)}
			<div className="flex flex-col my-4 mx-1">
				<div className="flex flex-row">
					<div className="text-xs flex-1">
						Updated: {props?.updated}
					</div>
					<div className="items-center flex flex-row">
						<IconBookSolid />
						<div className="ml-1 text-xs">{props.books.length}</div>
					</div>
				</div>

				<div className="font-bold">{props?.display_name}</div>
			</div>
		</div>
	);
};

export default Item;

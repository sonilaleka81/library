const IconBookShelf = () => {
	return (
		<svg
			xmlns="http://www.w3.org/2000/svg"
			className="h-10 w-10"
			fill="#ffffff"
			viewBox="0 0 24 24"
		>
			<path d="M9 3V18H12V3H9M12 5L16 18L19 17L15 4L12 5M5 5V18H8V5H5M3 19V21H21V19H3Z" />
		</svg>
	);
};

export default IconBookShelf;

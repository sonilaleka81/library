import React, { useState, useEffect, useMemo } from "react";
import { useParams, useNavigate, Link } from "react-router-dom";

import { useSelector, useDispatch } from "react-redux";
import { load } from "../store/listDetailsSlice";

import LoadingIndicator from "../components/LoadingIndicator";
import IconArrowLeft from "../components/IconArrowLeft";

import { getBestSellerListDetails } from "../controller";

const ListDetails = () => {
	const { listName, publishDate } = useParams();
	const navigate = useNavigate();
	const dispatch = useDispatch();

	const listData = useSelector((state) => state.listDetails);
	const data = useMemo(() => listData.data, [listData]);
	const details = useMemo(() => listData.details, [listData]);

	const [loading, setLoading] = useState(true);

	useEffect(() => {
		const loadData = async () => {
			setLoading(true);
			console.log("Loading from store");
			if (listData.data.length === 0) {
				console.log("Store empty, calling API");
				const { books, ...otherDetails } = (
					await getBestSellerListDetails(listName, publishDate)
				).data.results;
				dispatch(load({ data: books, details: otherDetails }));
			} else {
				setLoading(false);
			}
		};
		loadData();
	}, [listData, dispatch, listName, publishDate]);

	return (
		<div className="container mx-auto">
			<div className="flex flex-row items-center">
				<div className="cursor-pointer" onClick={() => navigate(-1)}>
					<IconArrowLeft />
				</div>
				<h1 className="ml-3 font-bold text-3xl">
					{details?.display_name || "List Details"}
				</h1>
			</div>

			{loading ? (
				<LoadingIndicator />
			) : (
				<div className="mt-10">
					{data?.length < 0 ? (
						<p>No Results</p>
					) : (
						<div>
							<div className="mt-5">
								<div className="relative overflow-x-auto">
									<table className="w-full text-sm text-left text-gray-500 table-auto">
										<thead className="text-xs text-gray-700 uppercase bg-gray-50  ">
											<tr>
												<th
													scope="col"
													className="px-6 py-3"
												>
													#
												</th>
												<th
													scope="col"
													className="px-6 py-3"
												>
													Title
												</th>
												<th
													scope="col"
													className="px-6 py-3"
												>
													Author
												</th>
												<th
													scope="col"
													className="px-6 py-3"
												>
													Description
												</th>
												<th
													scope="col"
													className="px-6 py-3"
												>
													Publisher
												</th>
												<th
													scope="col"
													className="px-6 py-3"
												>
													Rank
												</th>
											</tr>
										</thead>
										<tbody>
											{data.map((book, index) => (
												<tr
													key={`book-item-${index}`}
													className="bg-white border-b  "
												>
													<td className="px-6 py-4">
														{book.book_image ? (
															<img
																src={
																	book.book_image
																}
																className="rounded object-cover h-12 w-16"
																alt={book.title}
															/>
														) : (
															<div className="rounded bg-slate-400 h-12 w-16"></div>
														)}
													</td>
													<th
														scope="row"
														className="px-6 py-4 font-medium text-gray-900 whitespace-nowrap "
													>
														<Link
															to={`/book/${book.primary_isbn13}`}
														>
															{book.title}
														</Link>
													</th>
													<td className="px-6 py-4">
														{book.author}
													</td>
													<td className="px-6 py-4">
														{book.description}
													</td>
													<td className="px-6 py-4">
														{book.publisher}
													</td>
													<td className="px-6 py-4">
														{book.rank}
													</td>
												</tr>
											))}
										</tbody>
									</table>
								</div>
							</div>
						</div>
					)}
				</div>
			)}
		</div>
	);
};

export default ListDetails;

import { configureStore } from "@reduxjs/toolkit";
import listDetailsReducer from "./listDetailsSlice";

export const store = configureStore({
	reducer: {
		listDetails: listDetailsReducer,
	},
});

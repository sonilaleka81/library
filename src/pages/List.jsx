import React, { useState, useEffect } from "react";

import { useNavigate } from "react-router-dom";

import LoadingIndicator from "../components/LoadingIndicator";
import ResultDetails from "../components/ResultDetails";
import BestsellerItem from "../components/BSListItem";

import { getBestsellerOverview } from "../controller";

const List = () => {
	const navigate = useNavigate();

	const [loading, setLoading] = useState(true);
	const [data, setData] = useState([]);
	const [details, setDetails] = useState({});
	const [filteredList, setFilteredList] = useState([]);

	const [filter, setFilter] = useState("");

	const load = async () => {
		setLoading(true);
		const { lists, ...otherDetails } = (await getBestsellerOverview()).data
			.results;
		setData(lists);
		setDetails(otherDetails);
		setLoading(false);
	};

	useEffect(() => {
		load();
	}, []);

	useEffect(() => {
		if (filter && filter.length > 0) {
			setFilteredList(
				data.filter(
					(l) =>
						l.list_name
							.toLowerCase()
							.indexOf(filter.toLowerCase()) >= 0
				)
			);
		} else {
			setFilteredList(data);
		}
	}, [data, filter]);

	return (
		<div className="container mx-auto">
			<h1 className="text-3xl font-bold">NY Times Bestsellers</h1>
			<h2 className="text-md">Developed by: Sonila Leka</h2>

			{loading ? (
				<LoadingIndicator />
			) : (
				<div className="mt-10">
					{data?.length < 0 ? (
						<p>No Results</p>
					) : (
						<div>
							<div className="grid grid-cols-1 md:grid-cols-2">
								<ResultDetails details={details} />
								<div className="mt-5 md:mt-0 self-end justify-self-end w-full">
									<input
										type="text"
										id="search"
										className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5"
										placeholder="Search"
										onChange={(e) =>
											setFilter(e.target.value)
										}
									/>
								</div>
							</div>
							<div className="mt-5 grid gap-5 grid-cols-2 sm:grid-cols-3 md:grid-cols-5 mb-10">
								{filteredList.length > 0 ? (
									filteredList.map((l, index) => (
										<BestsellerItem
											key={`bestseller-item-${index}`}
											{...l}
											onOpen={(date) => {
												navigate(
													`/list/${
														l.list_name_encoded
													}/${date || "current"}`
												);
											}}
										/>
									))
								) : (
									<div className="text-center">No Data</div>
								)}
							</div>
						</div>
					)}
				</div>
			)}
		</div>
	);
};

export default List;

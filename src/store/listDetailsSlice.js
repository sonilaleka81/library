import { createSlice } from "@reduxjs/toolkit";

const initialState = {
	data: [],
	details: {},
};

export const listDetailsSlice = createSlice({
	name: "listDetails",
	initialState,
	reducers: {
		load: (state, action) => {
			console.log("In dispatch action", action);
			state.data = action.payload.data;
			state.details = action.payload.details;
		},
	},
});

// Action creators are generated for each case reducer function
export const { load } = listDetailsSlice.actions;

export default listDetailsSlice.reducer;

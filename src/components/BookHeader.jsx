const BookHeader = ({ book }) => {
	return (
		<div className="grid grid-cols-1 md:grid-cols-4">
			<div className="col-span-1 mx-5 md:mx-0">
				<img
					src={book.book_image}
					className="w-full h-auto"
					alt={book.title}
				/>
			</div>
			<div className="col-span-1 md:col-span-3 mx-2 md:mx-10 my-4">
				<h1 className="text-3xl font-bold">{book.title}</h1>
				<h2 className="text-xl">{book.author}</h2>
				<p className="mt-10">{book.description}</p>
				<div className="font-bold text-xs mt-10">ISBN</div>
				<div className="p-2">
					{book.isbns.map((isbn, index) => (
						<div key={`isbn-${index}`}>{isbn.isbn13}</div>
					))}
				</div>
				<div className="font-bold text-xs mt-4">Publisher</div>
				<div className="p-2">{book.publisher}</div>
			</div>
		</div>
	);
};

export default BookHeader;

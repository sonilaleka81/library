const mock = {
	data: {
		rank: 1,
		rank_last_week: 1,
		weeks_on_list: 10,
		asterisk: 0,
		dagger: 0,
		primary_isbn10: "1649374046",
		primary_isbn13: "9781649374042",
		publisher: "Red Tower",
		description:
			"Violet Sorrengail is urged by the commanding general, who also is her mother, to become a candidate for the elite dragon riders.",
		price: "0.00",
		title: "FOURTH WING",
		author: "Rebecca Yarros",
		contributor: "by Rebecca Yarros",
		contributor_note: "",
		book_image:
			"https://storage.googleapis.com/du-prd/books/images/9781649374042.jpg",
		book_image_width: 309,
		book_image_height: 500,
		amazon_product_url:
			"https://www.amazon.com/dp/1649374046?tag=NYTBSREV-20",
		age_group: "",
		book_review_link: "",
		first_chapter_link: "",
		sunday_review_link: "",
		article_chapter_link: "",
		isbns: [
			{
				isbn10: "1649374046",
				isbn13: "9781649374042",
			},
			{
				isbn10: "1649374089",
				isbn13: "9781649374080",
			},
			{
				isbn10: "1705085059",
				isbn13: "9781705085059",
			},
			{
				isbn10: "1705085032",
				isbn13: "9781705085035",
			},
		],
		buy_links: [
			{
				name: "Amazon",
				url: "https://www.amazon.com/dp/1649374046?tag=NYTBSREV-20",
			},
			{
				name: "Apple Books",
				url: "https://goto.applebooks.apple/9781649374042?at=10lIEQ",
			},
			{
				name: "Barnes and Noble",
				url: "https://www.anrdoezrs.net/click-7990613-11819508?url=https%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9781649374042",
			},
			{
				name: "Books-A-Million",
				url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fp%252FFOURTH%252BWING%252FRebecca%252BYarros%252F9781649374042&url2=https%3A%2F%2Fwww.anrdoezrs.net%2Fclick-7990613-35140%3Furl%3Dhttps%253A%252F%252Fwww.booksamillion.com%252Fsearch%253Fquery%253DFOURTH%252BWING%252BRebecca%252BYarros",
			},
			{
				name: "Bookshop",
				url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fbookshop.org%2Fa%2F3546%2F9781649374042&url2=https%3A%2F%2Fbookshop.org%2Fbooks%3Faffiliate%3D3546%26keywords%3DFOURTH%2BWING",
			},
			{
				name: "IndieBound",
				url: "https://du-gae-books-dot-nyt-du-prd.appspot.com/redirect?url1=https%3A%2F%2Fwww.indiebound.org%2Fbook%2F9781649374042%3Faff%3DNYT&url2=https%3A%2F%2Fwww.indiebound.org%2Fsearch%2Fbook%3Fkeys%3DFOURTH%2BWING%2BRebecca%2BYarros%26aff%3DNYT",
			},
		],
		book_uri: "nyt://book/106c2cf0-7d20-51b1-bad4-91c3ebcd131a",
	},
};

export default mock;
